#pragma once

#include <string>
#include <map>
#include <vector>

class Commandlet;
class CommandletRegistration;

typedef Commandlet* (RegistrationFunc)(const std::string& name);
typedef std::map<std::string, CommandletRegistration*> CommandletRegistrationMap;
typedef std::map<std::string, Commandlet*> CommandletsGeneratedMap;

//----------------------------------------------------------------------------------
struct CommandletArgs
{
	std::string command;
	std::vector<std::string> args;
};

//----------------------------------------------------------------------------------
class CommandletRegistration
{
public:
	CommandletRegistration(const std::string& name, RegistrationFunc* registrationFunc)
		:m_name(name)
		, m_registrationFunc(registrationFunc)
	{
		if (!s_commandletRegistration)
		{
			s_commandletRegistration = new CommandletRegistrationMap();
		}
		(*s_commandletRegistration)[name] = this;
	};

	static CommandletRegistrationMap* getCommandletRegistrations()
	{
		return s_commandletRegistration;
	};

	std::string getName()
	{
		return m_name;
	};

	Commandlet* createCommandlet();

protected:
	std::string m_name;
	RegistrationFunc* m_registrationFunc;
	static CommandletRegistrationMap* s_commandletRegistration;
};


//----------------------------------------------------------------------------------
class Commandlet
{
public:
	//Commandlet(void);
	Commandlet(const std::string& name);
	virtual ~Commandlet(void);

	//Return > 0 to exit program
	virtual int runCommandlet(CommandletArgs& args);
	static int createCommandlets();

	std::string m_name;

	static CommandletsGeneratedMap s_generatedCommandlets;
};