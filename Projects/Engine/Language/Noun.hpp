#pragma once
#include "Engine/2D/GameEntity2D.hpp"
#include <map>
#include <string>
#include <vector>
#include "Verb.hpp"
#include "Engine/Core/AnyDataTypeMap.hpp"
#include "Trigger.hpp"

class Noun : public GameEntity2D
{
public:
	Noun(Noun* toCopy);
	Noun(std::vector<Component*>& components, AnyDataTypeMap& memberAttributes);
	virtual ~Noun();

	virtual void update(float delta);
	virtual void onEvent();

	void registerVerb(const std::string& conditionToTriggerVerb, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes); 
	static int sGenerateNounsFromFiles(std::vector<std::string>& files);
	static void sUpdateNouns(float delta);
	void addComponent(Component* component);
	void finishInitalizationFromFactory();

	AnyDataTypeMap m_attributes;	

	static std::map<int, Noun*> s_mapOfNouns;
	static std::map<int, Noun*> s_mapOfNounFactories;

private:
	std::map<int, Trigger*> m_conditionAsIntToTriggersMap;

	std::vector<TiXmlElement*> componentsToGenerateFromXML;
};