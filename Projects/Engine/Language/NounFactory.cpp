//#include "NounFactory.hpp"
//
//std::vector<NounFactory*> NounFactory::s_registeredNounFactories;
//
////----------------------------------------------------------------------------------
//NounFactory::NounFactory(const std::string& name, std::vector<Component*>& components, AnyDataTypeMap& memberAttributes)
//	: m_name(name)
//	, m_memberAttributes(memberAttributes)
//{
//	m_components = components;
//}
//
////----------------------------------------------------------------------------------
//NounFactory::~NounFactory(void)
//{
//}
//
////----------------------------------------------------------------------------------
//int NounFactory::sGenerateNPCFactoriesFromFileList(std::vector<std::string>& files)
//{
//	/*
//	for each define file
//		name -> 
//		for each component
//
//		//Stop only handle components for now
//	*/
//
//	for (size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
//	{
//		TiXmlDocument doc;
//
//		if (!doc.LoadFile(files[fileIndex].c_str()))
//		{
//			return 0;
//		}
//
//		TiXmlElement* root = doc.FirstChildElement();
//
//		if (root == NULL)
//		{
//			doc.Clear();
//			return 0;
//		}
//
//		TiXmlElement* NPC = root->FirstChildElement("NPC");
//
//		std::string name = NPC->Attribute("name");
//		std::string family = NPC->Attribute("family");
//		std::string region = NPC->Attribute("region");
//		std::string factions = NPC->Attribute("factions");
//		std::string glyph = NPC->Attribute("glyph");
//		std::string r = NPC->Attribute("r");
//		std::string g = NPC->Attribute("g");
//		std::string b = NPC->Attribute("b");
//		std::string a = NPC->Attribute("a");
//
//		std::string los = NPC->Attribute("lineOfSight");
//		int lineOfSight = stoi(los);
//
//		RGBAColors newColor((unsigned char)std::stof(r), (unsigned char)std::stof(g),
//			(unsigned char)std::stof(b), (unsigned char)std::stof(a));
//
//		IntVec2 baseAttack(
//			stoi(NPC->Attribute("minAttack")),
//			stoi(NPC->Attribute("maxAttack"))
//			);
//
//		int ac = stoi(NPC->Attribute("ac"));
//		IntVec2 hitChance(
//			stoi(NPC->Attribute("minHitChance")),
//			stoi(NPC->Attribute("maxHitChance"))
//			);
//
//		int bonusChanceToHit = stoi(NPC->Attribute("bonusChanceToHit"));
//		int health = stoi(NPC->Attribute("health"));
//
//		unsigned char movement = stoi(NPC->Attribute("move"));
//
//		std::vector<BaseAIBehavior*> personality;
//		for (TiXmlElement* AI = NPC->FirstChildElement(); AI != NULL; AI = AI->NextSiblingElement())
//		{
//			std::string nameOfBehavior = AI->Attribute("name");
//			BaseAIBehavior* newBehavior = (*AIBehaviorRegistration::s_behaviorRegistrationMap)[nameOfBehavior]->createBehavior();
//
//			for (TiXmlAttribute* attrib = AI->FirstAttribute(); attrib != NULL; attrib = attrib->Next()) {
//				newBehavior->addAttributeByName(attrib->Name(), attrib->Value());
//			}
//
//			personality.push_back(newBehavior);
//		}
//
//		NPCFactory* newFactory = new NPCFactory(name, region, factions, family, glyph[0], newColor, lineOfSight,
//			ac, baseAttack, hitChance, bonusChanceToHit, health, movement);
//
//		newFactory->m_personality = personality;
//		s_registeredFactories.push_back(newFactory);
//
//	}
//	return 0;
//}
//
////----------------------------------------------------------------------------------
//Noun* NounFactory::createNoun(AnyDataTypeMap& addTheseExtraAttributes)
//{
//	Noun* toReturn = new Noun(m_components, m_memberAttributes);
//	addTheseExtraAttributes.fillTargetMapWithDataFromSelf(toReturn->m_attributes);
//	return toReturn;
//}
//
//
////----------------------------------------------------------------------------------
//int NounFactory::getNounFactoryIndexForName(const std::string& name)
//{
//	for (int factoryIndex = 0; factoryIndex < (int)s_registeredNounFactories.size(); ++factoryIndex)
//	{
//		if (name == s_registeredNounFactories[factoryIndex]->m_name)
//		{
//			return factoryIndex;
//		}
//	}
//	return -1;
//}
