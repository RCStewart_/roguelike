#include "Verb.hpp"

VerbRegistrationMap* VerbRegistration::s_verbRegistration = nullptr;
VerbMap Verb::s_generatedVerbs;

//----------------------------------------------------------------------------------
Verb* VerbRegistration::createVerb()
{
	return (*s_verbRegistration)[m_name]->m_registrationFunc(m_name);
}

//----------------------------------------------------------------------------------
Verb::Verb()
{

}

//----------------------------------------------------------------------------------
void Verb::setAttributes(AnyDataTypeMap& attributesToSet)
{
	attributesToSet.fillTargetMapWithDataFromSelf(m_attributes);
}

//----------------------------------------------------------------------------------
int Verb::createVerbs()
{
	int numOfRegistrations = 0;
	VerbRegistrationMap* registrations = VerbRegistration::getVerbRegistrations();
	if (registrations)
	{
		for (VerbRegistrationMap::iterator registerIterator = registrations->begin(); registerIterator != registrations->end(); registerIterator++)
		{
			++numOfRegistrations;
			VerbRegistration* registration = registerIterator->second;
			Verb* verbGenerator = registration->createVerb();
			std::string name = registration->getName();
			s_generatedVerbs[name] = verbGenerator;
		}
	}
	return numOfRegistrations;
}

//----------------------------------------------------------------------------------
void Verb::fillAnyDataMapWithAttributesFromXMLElement(TiXmlElement* verb, AnyDataTypeMap& toFill)
{

}