#pragma once
#include <vector>
#include "Verb.hpp"
#include "Engine/2D/GameEntity2D.hpp"
#include "Engine/Core/AnyDataTypeMap.hpp"

class Trigger
{
public:

	Trigger(const std::string& nameOfTrigger);
	void addVerb(Verb* verbToAdd, AnyDataTypeMap& verbAttributes);
	void activateTrigger(GameEntity2D* caster, std::vector<GameEntity2D*>& everyActor);

	int m_id;
	std::string m_name;

private:
	std::vector<Verb*> m_verbsToCallOnTrigger;
};