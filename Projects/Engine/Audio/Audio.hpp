#ifndef INCLUDED_AUDIO
#define INCLUDED_AUDIO
#pragma once

//---------------------------------------------------------------------------
#pragma comment( lib, "fmodex_vc" ) // Link in the fmodex_vc.lib static library

//---------------------------------------------------------------------------
//#include "../External/fmod/fmod.hpp"
#include "fmod.hpp"
#include <string>
#include <vector>
#include <map>


//---------------------------------------------------------------------------
typedef unsigned int SoundID;
const unsigned int MISSING_SOUND_ID = 0xffffffff;


/////////////////////////////////////////////////////////////////////////////
class AudioSystem
{
public:
	AudioSystem();
	virtual ~AudioSystem();
	SoundID CreateOrGetSound( const std::string& soundFileName );
	void PlaySound( SoundID soundID, float volumeLevel);
	void Update(); // Must be called at regular intervals (e.g. every frame)

	void PlayMusic( SoundID soundID, float volumeLevel);
	void setPauseSounds(bool isPaused);
	std::vector< FMOD::Channel* > m_usedChannels;
	FMOD::Channel* m_musicChannel;
	SoundID m_musicID;

protected:
	void InitializeFMOD();
	void ValidateResult( FMOD_RESULT result );

protected:
	FMOD::System*						m_fmodSystem;
	std::map< std::string, SoundID >	m_registeredSoundIDs;
	std::vector< FMOD::Sound* >			m_registeredSounds;
};


//---------------------------------------------------------------------------
void InitializeAudio();


#endif // INCLUDED_AUDIO
