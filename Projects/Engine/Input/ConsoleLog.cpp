#include "Engine\Input\ConsoleLog.hpp"
#include "Engine\Font\FontGenerator.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Core\GameCommon.hpp"
#include <iostream>
#include <sstream>
#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Math\Time.hpp"
#include <cmath>
#include "Engine\Math\Vec2.hpp"

FontGenerator* ConsoleLog::s_font;
vector<ConsoleLine> ConsoleLog::s_log;
map<string, RegisteredCommand*> ConsoleLog::s_registeredCommands;

string ConsoleLog::s_activeInput;
int ConsoleLog::s_indexOfActive;
float ConsoleLog::s_cellHeight;
bool ConsoleLog::s_isLogActive;
//bool ConsoleLog::s_isTextShadowOn;

//----------------------------------------------------------------------------------
ConsoleLog::ConsoleLog(void)
{

}


//----------------------------------------------------------------------------------
ConsoleLog::ConsoleLog( FontGenerator* font, float cellHeight )
{
	s_font = font;
	s_cellHeight = cellHeight;
	//initializeConsole();
}


//----------------------------------------------------------------------------------
ConsoleLog::~ConsoleLog(void)
{
}

//----------------------------------------------------------------------------------
void ConsoleLog::getInputs()
{

	if(!s_isLogActive)
	{
		return;
	}

	if(WindowWrapperGetIsInputActiveAndConsume(WINDOW_WRAPPER_INPUT_LEFT_MOUSE_CLICK))
	{
		Vec2 pos = WindowWrapperGetMousePosition();
		setMouseCursor( pos );
	}

	for(int i = 0; i < g_numOfLetters; ++i)
	{
		if(i == WINDOW_WRAPPER_INPUT_ENTER)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_ENTER ))
			{
				if(s_activeInput.length() > 0)
				{
					executeCommand(s_activeInput);
					s_activeInput = "";
					s_indexOfActive = -1;
				}
				else
				{
					s_isLogActive = !s_isLogActive;
					WindowWrapperSetCursorVisibility(false);
					return;
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_ESCAPE)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_ESCAPE ))
			{
				if(s_activeInput.length() > 0)
				{
					s_activeInput = "";
					s_indexOfActive = -1;
				}
				else
				{
					s_isLogActive = !s_isLogActive;
					WindowWrapperSetCursorVisibility(false);
					return;
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_BACKSPACE)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_BACKSPACE ))
			{
				if(s_indexOfActive >= 0)
				{
					s_activeInput.erase (s_activeInput.begin() + s_indexOfActive);
					--s_indexOfActive;
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_DELETE)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_DELETE ))
			{
				if(s_indexOfActive < (int) s_activeInput.length() - 1 || (s_indexOfActive == -1 && s_activeInput.length() > 0))
				{
					if(s_indexOfActive == -1)
					{
						s_activeInput.erase(s_activeInput.begin());//0 + s_indexOfActive + 1);
					}
					else
					{
						s_activeInput.erase(s_activeInput.begin() + s_indexOfActive + 1);
					}
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_LEFT)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_LEFT ))
			{
				if(s_indexOfActive >= 0)
				{
					--s_indexOfActive;
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_RIGHT)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_RIGHT ))
			{
				if(s_indexOfActive < (int) s_activeInput.length() - 1 || (s_indexOfActive == -1 && (int) s_activeInput.length() > 0))
				{
					++s_indexOfActive;
				}
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_HOME)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_HOME ))
			{
				s_indexOfActive = -1;
			}
		}
		else if(i == WINDOW_WRAPPER_INPUT_END)
		{
			if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_END ))
			{
				s_indexOfActive = s_activeInput.length() - 1;
			}
		}
		else if(i == '`' || i == '~')
		{
			if(WindowWrapperGetIsInputActiveAndConsume( (unsigned char) i))
			{
				s_isLogActive = !s_isLogActive;
				WindowWrapperSetCursorVisibility(false);
				return;
			}
		}
		else
		{
			if(WindowWrapperGetIsInputActiveAndConsume( (unsigned char) i))
			{
				//s_activeInput += (char) i;
				string temp = "";
				temp += (char) i;
				s_activeInput.insert(s_indexOfActive + 1, temp);
				++s_indexOfActive;
			}
		}
	}
}


//----------------------------------------------------------------------------------
void ConsoleLog::setMouseCursor( Vec2& newPos )
{
	int newIndex = s_font->findClosestTextIndex( s_activeInput, (GENERATED_FONTS) 0, s_cellHeight, newPos );
	s_indexOfActive = newIndex;
}

//----------------------------------------------------------------------------------
void ConsoleLog::renderLog()
{
	if(s_isLogActive)
	{
		vector<Vertex3D_PCT> vertexes;

		for(int i = 0; i < 4; ++i)
		{
			vertexes.push_back(Vertex3D_PCT());
			vertexes[i].m_color = RGBAColors(0, 0, 0, 200);
		}
		vertexes[0].m_position = Vec3( 0.0f, g_maxScreenY, 0.0f );
		vertexes[1].m_position = Vec3( 0.0f, 0.0f, 0.0f );
		vertexes[2].m_position = Vec3( g_maxScreenX, 0.0f, 0.0f );
		vertexes[3].m_position = Vec3( g_maxScreenX, g_maxScreenY, 0.0f );

		Vec3 translation = Vec3(0.f, 0.f, 0.f);
		GLWrapperEnableGLWrapperOrtho();
		GLWrapperVertexArray( vertexes, translation, GL_WRAPPER_QUADS, 0 );
		drawBlinkingCursor();
		GLWrapperDisableGLWrapperOrtho();

		Vec2 activePosition = Vec2(0.f, 0.f);
		s_font->drawText( s_activeInput, (GENERATED_FONTS) 0, s_cellHeight, activePosition, RGBAColors(255, 255, 255, 255) );
		for(int i = s_log.size() - 1; i >= 0; --i)
		{
			Vec2 position = Vec2(0.0f, s_cellHeight * (s_log.size() - i));
			s_font->drawText( s_log[i].m_text, (GENERATED_FONTS) 0, s_cellHeight, position, s_log[i].m_textColor );
		}
	}
}

//----------------------------------------------------------------------------------
void ConsoleLog::addStringToLog( const string& toAdd, RGBAColors& color)
{
	ConsoleLine newLine = ConsoleLine();
	newLine.m_text = toAdd;
	newLine.m_textColor = color;
	int maxLogSize = 60;
	s_log.push_back(newLine);
	if((int) s_log.size() > maxLogSize)
	{
		vector<ConsoleLine> newLog;
		for(int i = (int) (maxLogSize * .5); i < (int) s_log.size(); ++i)
		{
			newLog.push_back(s_log[i]);
		}
		s_log.clear();
		s_log = newLog;
	}
}

//----------------------------------------------------------------------------------
void ConsoleLog::registerCommand( const string& name, CommandFunc* f, const string& description)
{
	RegisteredCommand* newCommand = new RegisteredCommand();
	newCommand->m_f = f;
	newCommand->m_description = description;
	newCommand->m_name = name;

	s_registeredCommands[name] = newCommand;
}

//----------------------------------------------------------------------------------
void ConsoleLog::help(const ConsoleCommandArgs&)
{
	map<string, RegisteredCommand*>::iterator iter = s_registeredCommands.begin();
	string toPrint = "List of Commands:";
	RGBAColors textColor = RGBAColors(255, 100, 125, 255);
	addStringToLog( toPrint, textColor );
	for(; iter != s_registeredCommands.end(); iter++) {
		toPrint = "  ";
		toPrint += iter->second->m_name;
		toPrint += ":     ";
		toPrint += iter->second->m_description;
		
		addStringToLog( toPrint, textColor );
	}
}

//----------------------------------------------------------------------------------
void ConsoleLog::clear(const ConsoleCommandArgs&)
{
	vector<ConsoleLine> newLog;
	s_log = newLog;
}

//----------------------------------------------------------------------------------
void ConsoleLog::quit(const ConsoleCommandArgs&)
{
	exit(0);
}

//----------------------------------------------------------------------------------
void ConsoleLog::test()
{
	string name = "help";
	ConsoleCommandArgs args;
	args.m_rawArgs = "Testing to see if function pointer is working.";
	s_registeredCommands[name]->m_f(args);
}

//----------------------------------------------------------------------------------
void ConsoleLog::initializeConsole( FontGenerator* font, float cellHeight )
{
	s_font = font;
	s_cellHeight = cellHeight;
	s_isLogActive = false;
	s_indexOfActive = -1;
	//s_isTextShadowOn = true;

	string command = "help";
	string description = "Shows each command and their description.";
	CommandFunc* f = help;
	registerCommand( command, f, description);

	command = "clear";
	description = "Clears the log";
	f = clear;
	registerCommand( command, f, description);

	command = "quit";
	description = "Exit the program";
	f = quit;
	registerCommand( command, f, description);

	//command = "toggleTextShadow";
	//description = "Toggles the shadow on the text on and off";
	//f = toggleTextShadow;
	//registerCommand( command, f, description);
}

//----------------------------------------------------------------------------------
void ConsoleLog::executeCommand( const string& toExecute )
{
	string name;
	istringstream sStream(toExecute);
	string word;
	sStream >> name;

	ConsoleCommandArgs args;

	args.m_rawArgs = toExecute;
	
	vector<string> parsedArgs;
	parsedArgs.push_back(name);

	string oneWordOfArg;
	while(sStream >> oneWordOfArg) {
		parsedArgs.push_back(oneWordOfArg);
	}
	args.m_args = parsedArgs;
	
	if(s_registeredCommands.find( name ) != s_registeredCommands.end())
	{
		s_registeredCommands[name]->m_f(args);
	}
	else
	{
		string errorMessage = "ERROR: Command '";
		errorMessage += name;
		errorMessage += "' is not valid";
		RGBAColors errorText = RGBAColors(255, 0, 0, 255);
		addStringToLog( errorMessage, errorText );
	}
}

//----------------------------------------------------------------------------------
void ConsoleLog::drawBlinkingCursor( )
{
	double theTime = GetCurrentTimeSeconds();
	double result = fmod(theTime, .5);
	if(result > 0.2)
	{
		float x_axis = s_font->calcTextWidth( s_activeInput, (GENERATED_FONTS) 0, s_cellHeight, s_indexOfActive + 1 );
		vector<Vertex3D_PCT> cursor;
		cursor.push_back(Vertex3D_PCT());
		cursor.push_back(Vertex3D_PCT());

		cursor[0].m_color = RGBAColors(255, 255, 255, 255);
		cursor[1].m_color = RGBAColors(255, 255, 255, 255);

		cursor[0].m_position = Vec3(0.f, 0.f, 0.f);
		cursor[1].m_position = Vec3(0.f, s_cellHeight, 0.f);

		Vec3 translation = Vec3(x_axis, 0.f, 0.f);
		GLWrapperVertexArray( cursor, translation, GL_WRAPPER_DRAW_LINES, 0 );
	}
}