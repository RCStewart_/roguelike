#pragma once

#include "Engine\Input\tinystr.h"
#include "Engine\Input\tinyxml.h"
#include <string>
#include "Engine\Math\IntVec2.hpp"
#include <map>
#include <vector>
#include "Engine\Math\Vec2.hpp"
#include "Engine\Core\RGBAColors.hpp"

class Texture;

enum GENERATED_FONTS
{
	GENERATED_FONTS_BIT,
	GENERATED_FONTS_MAX_NUM
};

struct GlyphMetaData
{
	Vec2 m_minTexCoords;
	Vec2 m_maxTexCoords;
	//ttf stands for true type font
	float m_ttfA; //border before glyph on the x axis
	float m_ttfB; //width of glyph 
	float m_ttfC; //border after glyph on the x axis

	Texture* texture;
};

using namespace std;
class FontGenerator
{
public:
	FontGenerator(void);
	~FontGenerator(void);

	int loadFontInformationFromXML(string& xmlFileName, string& imageFileName);
	void initialize();
	void drawText( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, Vec2& startingLocation, RGBAColors colorOfText = RGBAColors(255, 255, 255, 255));
	float calcTextWidth( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, int index = -1 );
	int findClosestTextIndex( string& toDraw, GENERATED_FONTS fontToUse, float cellHeight, Vec2& mousePos );

	int m_widthInPixels;
	int m_heightInPixels;
	vector<map<int, GlyphMetaData*>> m_Fonts;
	vector<Texture*> m_fontTextures;
};

