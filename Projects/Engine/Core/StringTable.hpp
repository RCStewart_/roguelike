#pragma once
#include <string>

int StringID( const std::string& stringID );
const char* StringValue( int stringID );