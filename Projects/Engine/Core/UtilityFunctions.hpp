#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <sstream>

class Vec2;
class IntVec2;

class UtilityFunctions
{
public:
	UtilityFunctions(void);
	~UtilityFunctions(void);

	static void clampFloat(float& valueToClamp, const float& minValue, const float& maxValue, float radiusOfValueToClamp = 0.f);
	static void clampDouble(double& valueToClamp, const double& minValue, const double& maxValue);
	static float computeShortestAngularDisplacement(float from, float to);
	static bool detectCircleCollision(const float& circleOneRadius, const Vec2& circleOnePosition, const float& circleTwoRadius, const Vec2& circleTwoPosition);

	static float CalcDistSquaredBetweenPoints(const Vec2& a, const Vec2& b);
	static int CalcDistSquaredBetweenPoints(const IntVec2& a, const IntVec2& b);

	static float degreesToRadians(const float& degrees);
	static float radiansToDegrees(const float& degrees);

	//static unsigned long getFileLength(const std::ifstream& file);
	static unsigned int getFileLength(const std::string filePath);
	static int loadFileToNewBuffer( const std::string filePath, unsigned char* buffer );

	template< typename T>
	static void deleteVectorOfPointers( std::vector<T*>& toDelete );

	template< typename T>
	static void clearVectorWithSwap( std::vector<T>& toClear );

	static bool detectCircleCollision(float circleOneRadius, Vec2 &circleOnePosition, float circleTwoRadius, Vec2 &circleTwoPosition);

	static std::vector<std::string> &Split(const std::string &s, char delim, std::vector<std::string> &elems);
	static std::vector<std::string> Split(const std::string &s, char delim);
};

//----------------------------------------------------------------------------------
template< typename T>
void UtilityFunctions::deleteVectorOfPointers( std::vector<T*>& toDelete )
{
	for( int i = 0; i < (int) toDelete.size(); ++i )
	{
		delete toDelete[ i ];
	}
	clearVectorWithSwap( toDelete );
}

//----------------------------------------------------------------------------------
template< typename T >
void UtilityFunctions::clearVectorWithSwap( std::vector<T>& toClear )
{
	toClear.clear();
	std::vector<T>(toClear).swap(toClear);
}
