//---------------------------------------------------------------------------
#include "Texture.hpp"
#include <fstream>
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Files/FileSystem.hpp"

#define STATIC // Do-nothing indicator that method/member is static in class definition


//---------------------------------------------------------------------------
STATIC std::map< std::string, Texture* >	Texture::s_textureRegistry;


//---------------------------------------------------------------------------
Texture::Texture( const std::string& imageFilePath, DefaultTexture typeToDefaultTo  )
	: m_openglTextureID( 0 )
	, m_sizeX(0), m_sizeY(0)
{
	UNUSED(typeToDefaultTo);
	GLWrapperGenerateTexture(m_openglTextureID, m_sizeX, m_sizeY, imageFilePath);
}

//----------------------------------------------------------------------------------
Texture::Texture( unsigned char* data)
	: m_openglTextureID( 0 )
	, m_sizeX(1), m_sizeY(1)
{
	GLWrapperGenerateTexture(m_openglTextureID, m_sizeX, m_sizeY, data);
}


//----------------------------------------------------------------------------------
STATIC void Texture::createDefaultTextures()
{
	unsigned char specGlossEmData[4] = {127, 127, 0, 255};
	Texture::s_textureRegistry["emmisive"] = new Texture(specGlossEmData);
	Texture::s_textureRegistry["gloss"] = new Texture(specGlossEmData);
	Texture::s_textureRegistry["specular"] = new Texture(specGlossEmData);

	unsigned char normal[4] = {127, 127, 255, 255};
	Texture::s_textureRegistry["normal"] = new Texture(normal);
	unsigned char diffuse[4] = {255, 255, 255, 255};
	Texture::s_textureRegistry["diffuse"] = new Texture(diffuse);
}

//---------------------------------------------------------------------------
// Returns a pointer to the already-loaded texture of a given image file,
//	or nullptr if no such texture/image has been loaded.
//
STATIC Texture* Texture::GetTextureByName( const std::string& imageFilePath, DefaultTexture typeToDefaultTo )
{
	UNUSED(typeToDefaultTo);
	Texture* loadedTexture = nullptr;
	bool isTextureLoaded = false;

	isTextureLoaded = (s_textureRegistry.find(imageFilePath) != Texture::s_textureRegistry.end());

	if( isTextureLoaded )
	{
		loadedTexture = s_textureRegistry[imageFilePath];
	}

	return loadedTexture;
}


//---------------------------------------------------------------------------
// Finds the named Texture among the registry of those already loaded; if
//	found, returns that Texture*.  If not, attempts to load that texture,
//	and returns a Texture* just created (or nullptr if unable to load file).
//
STATIC Texture* Texture::CreateOrGetTexture( const std::string& imageFilePath, DefaultTexture typeToDefaultTo )
{
	Texture* loadedTexture = nullptr;
	loadedTexture = GetTextureByName(imageFilePath);

	if( loadedTexture == nullptr )
	{
		if (!g_theFileSystem)
		{
			g_theFileSystem = new FileSystem(nullptr);
		}
		if(/*std::ifstream( imageFilePath )*/ g_theFileSystem->doesFileExistOnDisc(imageFilePath))
		{
			Texture::s_textureRegistry[imageFilePath] = new Texture(imageFilePath, typeToDefaultTo);
		}
		else
		{
			Texture::s_textureRegistry[imageFilePath] = Texture::getDefaultTexture( typeToDefaultTo );
		}
		loadedTexture = s_textureRegistry[imageFilePath];
	}

	return loadedTexture;
}

//----------------------------------------------------------------------------------
STATIC Texture* Texture::getDefaultTexture( DefaultTexture typeToDefaultTo )
{
	if( typeToDefaultTo == DefaultTextureDiffuse)
	{
		return Texture::s_textureRegistry["diffuse"];
	}
	if( typeToDefaultTo == DefaultTextureNormal)
	{
		return Texture::s_textureRegistry["normal"];
	}
	if( typeToDefaultTo == DefaultTextureSpecGlossEmissive)
	{
		return Texture::s_textureRegistry["specular"];
	}
	return Texture::s_textureRegistry["diffuse"];
}
