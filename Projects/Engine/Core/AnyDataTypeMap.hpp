#pragma once
#include <string>
#include "AnyDataType.hpp"
#include <map>

//----------------------------------------------------------------------------------
class AnyDataTypeMap
{
public:
	AnyDataTypeMap(){};
	~AnyDataTypeMap(){};

	Base* get(const std::string& key);
	
	template<class T>
	T getRawData(const std::string& key)
	{
		return static_cast<AnyDataType<T>*>(get(key))->m_data;
	};

	template<class T>
	void set(const std::string& key, const T& data)
	{
		AnyDataType<T>* dataToAdd = new AnyDataType<T>(data, key);
		updateValueAtKey(key, dataToAdd);
	};


	void  deleteAtKey(const std::string& key);
	void  fillTargetMapWithDataFromSelf(AnyDataTypeMap& toFill);

private:
	std::map<int, Base*> m_stringAsIntToDataMap;

	void  setDeepCopy(const std::string& key, Base* data);
	void  updateValueAtKey(const std::string& key, Base* data);
};
