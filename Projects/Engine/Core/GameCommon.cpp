#include "GameCommon.hpp"

float g_numOfTileUnitsX = 1600.f;
float g_numOfTileUnitsY = 900.f;

float g_maxNumOfTileUnitsXOnScreen = 1600.f;
float g_maxNumOfTileUnitsYOnScreen = 900.f;
//const float g_numOfTileUnitsZ = 128.f;

const float g_maxScreenX = 1600.f;
const float g_maxScreenY = 900.f;
const float g_windowDisplacement = 50.f;

const float g_PI = 3.14159265359f;
const float g_charToFloat = 1.f /255.f;

int g_numOfLetters = 200;

std::vector<int> g_materialIDIndexes;
//const float g_musicVolume = 1.f;
//const float g_soundVolume = .3f;

std::vector<Light> g_lights;

Vec3 g_cameraPos = Vec3(0.f, 0.f, 0.f);

const float g_zFar = 1000.f;

//TODO: create a commandlet to change password
std::string g_dataPassword = "password";

GAME_COMMON_LOAD_TYPE g_currentLoadPreference = LOAD_FROM_DATA_ONLY;

//Renderer* g_theRenderer = nullptr;

float g_actualDeltaTime = 1.0f / 30.0f;
float g_goalDeltaTime = 1.0f / 30.0f;
