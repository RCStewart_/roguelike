#include "Profiler.hpp"
#include "Engine/Time/UtilitiesTime.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Engine/Core/EngineCommon.hpp"

//#include "Engine/Core/UtilityFunctions.hpp"
//#include "Engine/Time/Alarm.hpp"

//TODO ADD A GLOBAL FONT RENDERER TO FONT CLASS

Profiler* g_rootProfiler = nullptr;
std::map<int, Profiler*> Profiler::s_nameToProfilerMap;
FontGenerator* Profiler::s_font = nullptr;
bool Profiler::s_renderProfiler = false;

//----------------------------------------------------------------------------------
Profiler::Profiler(const std::string &parentName, const std::string &name, const std::string classification)
	: m_iteration(0)
	, m_totalCount(0)
	, m_startCount(0)
	, m_lastAccumulatedCount(0)
{
	m_classification = StringID(classification);
	m_name = StringID(name);

	int parentNameID = -1;

	if (parentName != "nullptr")
	{
		parentNameID = StringID(parentName);
	}

	addSelfToParentAndCreateParentIfNotExist(parentNameID);
}


//----------------------------------------------------------------------------------
Profiler::~Profiler()
{
	for (int profilerIndex = 0; profilerIndex < (int)m_children.size(); ++profilerIndex)
	{
		delete m_children[profilerIndex];
	}
	//m_parent->removeClockFromChildren(m_index);
}

//----------------------------------------------------------------------------------
void Profiler::update()
{
	std::map<int, Profiler*>::iterator it = s_nameToProfilerMap.begin();
	for (; it != s_nameToProfilerMap.end(); ++it)
	{
		++it->second->m_iteration;
		it->second->m_lastAccumulatedCount = it->second->m_accumulatedCount;
		it->second->m_accumulatedCount = 0;
	}
}

//----------------------------------------------------------------------------------
void Profiler::render(Vec2& positionToRenderAt)
{
	g_rootProfiler->render( positionToRenderAt, g_rootProfiler->getTotalTimeAsSeconds(), 0.0 );
}

//----------------------------------------------------------------------------------
void Profiler::render(Vec2& positionToRenderAt, double totalTime, double xDisplacement)
{
	float xDis = 2.f;
	float yDis = 2.f;

	double long totalTimeString = getTotalTimeAsSeconds();
	double long averageTime = getAverageTimeAsSeconds();
	double long percentOfWhole = getTotalTimeAsSeconds() / totalTime;

	if (totalTime == 0.0)
	{
		percentOfWhole = 0.0;
	}

	string toWrite = GetMessageLiteralf("Name: %s | Ave: %.20lf | Time: %.20lf | Percent: %.20lf \n", std::string(StringValue(m_name)).c_str(), averageTime, totalTimeString, percentOfWhole);


	Profiler::s_font->drawText(toWrite, GENERATED_FONTS_BIT, 2.f, Vec2(positionToRenderAt.x + ((float)xDisplacement), positionToRenderAt.y), RGBAColors(255, 255, 255, 255), false);

	positionToRenderAt.y -= yDis;

	xDisplacement += xDis;

	for (int i = 0; i < (int) m_children.size(); ++i)
	{
		m_children[i]->render(positionToRenderAt, totalTime, xDisplacement);
	}

}

//----------------------------------------------------------------------------------
void Profiler::addSelfToParentAndCreateParentIfNotExist(int parentID)
{
	if (parentID == -1)
	{
		return;
	}

	//Check if parent exists
	std::map<int, Profiler*>::iterator it = s_nameToProfilerMap.find(parentID);

	//Generate Parent
	if (it == s_nameToProfilerMap.end())
	{
		//Parent is assumed to be of the same classification
		Profiler* parent = new Profiler("root", StringValue(parentID), StringValue(m_classification));
		s_nameToProfilerMap[parentID] = parent;
		m_parent = parent;
	}
	else
	{
		m_parent = s_nameToProfilerMap[parentID];
	}

	//Add Self to Parent
	if (m_parent)
	{
		m_parent->m_children.push_back(this);
	}
}

//----------------------------------------------------------------------------------
void Profiler::start(const std::string &name, const std::string &parentName, const std::string &category/*, bool isRootOfCatagory*/)
{
	//Check for profiler
	std::map<int, Profiler*>::iterator it = s_nameToProfilerMap.find(StringID(name));
	if (it == s_nameToProfilerMap.end())
	{
		//Parent is assumed to be of the same classification
		Profiler* newProfiler = new Profiler(parentName, name, category);
		s_nameToProfilerMap[StringID(name)] = newProfiler;
	}

	//Call start() on profiler
	s_nameToProfilerMap[StringID(name)]->start();
}

//----------------------------------------------------------------------------------
void Profiler::finish(const std::string &name)
{
	s_nameToProfilerMap[StringID(name)]->finish();
}

//----------------------------------------------------------------------------------
void Profiler::start()
{
	//reset start time
	LARGE_INTEGER startCount;
	QueryPerformanceCounter(&startCount);
	s_nameToProfilerMap[m_name]->m_startCount = (int) startCount.QuadPart;
}

//----------------------------------------------------------------------------------
void Profiler::finish()
{
	//accumulate 
	LARGE_INTEGER finishCount;
	QueryPerformanceCounter(&finishCount);
	int delta = ((int) finishCount.QuadPart) - m_startCount;
	m_accumulatedCount += delta;
	m_totalCount += delta;
}

//----------------------------------------------------------------------------------
void Profiler::initializeProfilerSystem()
{
	std::map<int, Profiler*>::iterator it = s_nameToProfilerMap.find(StringID("root"));
	if (it == s_nameToProfilerMap.end())
	{
		//Parent is assumed to be of the same classification
		Profiler* newProfiler = new Profiler("nullptr", "root", "root");
		s_nameToProfilerMap[StringID("root")] = newProfiler;
	}
	g_rootProfiler = s_nameToProfilerMap[StringID("root")];
}

//----------------------------------------------------------------------------------
double Profiler::getTotalTimeAsSeconds()
{
	double toReturn = 0.0;

	if (m_lastAccumulatedCount != 0.0)
	{
		toReturn = ((double) GetSecondsPerClock() * (double) m_lastAccumulatedCount);
	}
	return toReturn;
}

//----------------------------------------------------------------------------------
double Profiler::getAverageTimeAsSeconds()
{
	double toReturn = 0.0;
	if (m_totalCount != 0.0)
	{
		toReturn = ((double)(GetSecondsPerClock() * (double)m_totalCount) / (double) m_iteration);
	}
	return toReturn;
}