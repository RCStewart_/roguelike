#pragma once
#include "Windows.h"
#define WIN32_LEAN_AND_MEAN

#include <vector>
#include <string>
#include <map>
#include "Engine/Math/Vec2.hpp"
#include "Engine/Font/FontGenerator.hpp"

class Alarm;

class Profiler
{
public:
	~Profiler();

	static void update();
	static void render(Vec2& positionToRenderAt);

	void advanceTime(double deltaTime);

	double getTotalTimeAsSeconds();
	double getAverageTimeAsSeconds();

	static void start(const std::string &name, const std::string &parentName, const std::string &category/*, bool isRootOfCatagory*/);
	static void finish(const std::string &name);

	static void initializeProfilerSystem();

	static std::map<int, Profiler*> s_nameToProfilerMap;

	int						m_name;
	int						m_classification;
	int						m_iteration;

	int						m_totalCount;
	int						m_startCount;
	int						m_lastAccumulatedCount;
	int						m_accumulatedCount;

	Profiler*				m_parent;
	std::vector<Profiler*>	m_children;
	static FontGenerator* s_font;
	static bool s_renderProfiler;
private:

	//Profiler(const std::string name);
	Profiler(const std::string &parentName, const std::string &name, const std::string classification);

	//void setParent(int parentNameIndex);
	void addSelfToParentAndCreateParentIfNotExist(int parentID);

	void start();
	void finish();

	void render(Vec2& positionToRenderAt, double totalTime, double xDisplacement);
};

extern Profiler* g_rootProfiler;