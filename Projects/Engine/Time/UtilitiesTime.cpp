#include "Engine\Time\UtilitiesTime.hpp"


//----------------------------------------------------------------------------------
double GetAbsoluteTimeSeconds()
{
	static LARGE_INTEGER startTime;
	static double secondsPerCount = InitializeTimeSystem(startTime);
	LARGE_INTEGER currentCount;
	QueryPerformanceCounter(&currentCount);
	LONGLONG deltaCounts = currentCount.QuadPart - startTime.QuadPart;

	double currentSeconds = static_cast<double>(deltaCounts) * secondsPerCount;
	return currentSeconds;

}

//----------------------------------------------------------------------------------
double InitializeTimeSystem( LARGE_INTEGER &timeToInitialize )
{
	LARGE_INTEGER countsPerSecond;
	QueryPerformanceFrequency(&countsPerSecond);
	QueryPerformanceCounter(&timeToInitialize);
	return (1.0 / static_cast<double>(countsPerSecond.QuadPart));
}

//----------------------------------------------------------------------------------
double GetSecondsPerClock()
{
	static LARGE_INTEGER nullTime;
	static double secondsPerCount = InitializeTimeSystem(nullTime);
	return secondsPerCount;
}