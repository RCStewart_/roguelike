#include "EulerAngles.hpp"

EulerAngles::EulerAngles(void)
	: rollDegreesAboutX( 0.0f )
	, pitchDegreesAboutY( 0.0f )
	, yawDegreesAboutZ( 0.0f )
{
}


EulerAngles::EulerAngles(const float& newRollDegreesAboutX, const float& newPitchDegreesAboutY, const float& newYawDegreesAboutZ)
	: rollDegreesAboutX( newRollDegreesAboutX )
	, pitchDegreesAboutY( newPitchDegreesAboutY )
	, yawDegreesAboutZ( newYawDegreesAboutZ )
{
}


EulerAngles::~EulerAngles(void)
{
}

