#include "Vec3.hpp"
#include <cmath>

const double PI = 3.14159265;


//---------------------------------------------------------------------------
Vec3::Vec3(void)
	: x( 0 )
	, y( 0 )
	, z( 0 )
{

}


//---------------------------------------------------------------------------
Vec3::Vec3(const float& newX, const float& newY, const float& newZ)
	: x( newX )
	, y( newY )
	, z( newZ )
{
}


//---------------------------------------------------------------------------
float Vec3::getMagnetude()
{
	float magnetude;
	float pyth_x = x * x;
	float pyth_y = y * y;
	float pyth_z = z * z;
	float lenSquared = pyth_x + pyth_y + pyth_z;

	if(lenSquared > 0.f)
	{
		magnetude = sqrt(pyth_x + pyth_y + pyth_z);
	}
	else
	{
		return 0.f;
	}

	return magnetude;
}


//---------------------------------------------------------------------------
float Vec3::degreesToRadians(const float& degrees)
{
	float convertRatio = (float) (PI/180.f);
	float toReturn = degrees * convertRatio;
	return toReturn;
}


//---------------------------------------------------------------------------
float Vec3::radiansToDegrees(const float& radians)
{
	float convertRatio = (float) (180.f/PI);
	float toReturn = radians * convertRatio;
	return toReturn;
}


//---------------------------------------------------------------------------
void Vec3::scalarMultiplication(const float& scalar)
{
	x *= scalar;
	y *= scalar;
	z *= scalar;
}


//---------------------------------------------------------------------------
float Vec3::normalize()
{
	float magnetude = getMagnetude();
	if(magnetude > 0.f)
	{
		float invLen = 1.f / magnetude;
		x *= invLen;
		y *= invLen;
		z *= invLen;
	}
	else
	{
		return 0.f;
	}
	return magnetude;
}


//---------------------------------------------------------------------------
Vec3 Vec3::addVectors(const Vec3& one, const Vec3& two)
{
	Vec3 sum;
	sum.x = one.x + two.x;
	sum.y = one.y + two.y;
	sum.z = one.z + two.z;
	return sum;
}


//---------------------------------------------------------------------------
inline void Vec3::setLength(float newLength)
{
	normalize();
	x *= newLength;
	y *= newLength;
	z *= newLength;
}


//---------------------------------------------------------------------------
Vec3::~Vec3(void)
{

}


//----------------------------------------------------------------------------------
bool Vec3::operator==(const Vec3& other) const
{
	return (x == other.x && y == other.y && z == other.z);
}

//----------------------------------------------------------------------------------
Vec3 Vec3::operator-(const Vec3& other) const
{
	Vec3 toReturn = Vec3(0.f, 0.f, 0.f);
	toReturn.x = this->x - other.x;
	toReturn.y = this->y - other.y;
	toReturn.z = this->z - other.z;
	return toReturn;
}

//----------------------------------------------------------------------------------
Vec3 Vec3::operator*(const Vec3& other) const
{
	Vec3 toReturn = Vec3(0.f, 0.f, 0.f);
	toReturn.x = this->y * other.z - this->z * other.y;
	toReturn.y = this->z * other.x - this->x * other.z;
	toReturn.z = this->x * other.y - this->y * other.x;

	return toReturn;
}

//----------------------------------------------------------------------------------------------
float Vec3::dotProduct(const Vec3& one, const Vec3& two)
{
	return (one.x * two.x + one.y * two.y + one.z * two.z);
}


//----------------------------------------------------------------------------------
float Vec3::distanceSquaredBetweenTwoPoints(const Vec3& one, const Vec3& two)
{
	float xDist = one.x - two.x;
	float yDist = one.y - two.y;
	float zDist = one.z - two.z;
	return (xDist * xDist) + (yDist * yDist) + (zDist * zDist);
}