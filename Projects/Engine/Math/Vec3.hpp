#pragma once
class Vec3
{
public:
	Vec3(void);
	Vec3(const float& newX, const float& newY, const float& newZ);

	//Vec3 rotateToDegrees(float angle);
	float getMagnetude();
	//float getBaseOrientationDegrees();
	float degreesToRadians(const float& degrees);
	float radiansToDegrees(const float& radians);
	void scalarMultiplication(const float& scalar);
	float normalize();
	void setLength(float newLength);

	static Vec3 addVectors(const Vec3& one, const Vec3& two);
	static float dotProduct(const Vec3& one, const Vec3& two);
	static float distanceSquaredBetweenTwoPoints(const Vec3& one, const Vec3& two);

	bool operator==(const Vec3& other) const;
	Vec3 operator-(const Vec3& other) const;
	Vec3 operator*(const Vec3& other) const;
	~Vec3(void);

	float x;
	float y;
	float z;
};

