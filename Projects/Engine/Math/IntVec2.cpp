#include "IntVec2.hpp"
#include <cstdlib>
#include <cmath>

//---------------------------------------------------------------------------
IntVec2::IntVec2(void)
	: x( 0 )
	, y( 0 )
{
}


//---------------------------------------------------------------------------
IntVec2::~IntVec2(void)
{
}


//---------------------------------------------------------------------------
IntVec2::IntVec2(int newX, int newY)
	: x( newX )
	, y( newY )
{
}

//----------------------------------------------------------------------------------------------
bool IntVec2::operator==(const IntVec2& other) const
{
	return (x == other.x && y == other.y);
}

//----------------------------------------------------------------------------------------------
bool IntVec2::operator>=(const IntVec2& other) const
{
	if( y == other.y && x == other.x)
	{
		return true;
	}
	if( y > other.y)
	{
		return true;
	}
	if( y == other.y)
	{
		return x >= other.x;
	}

	return false;
}

//----------------------------------------------------------------------------------------------
bool IntVec2::operator<(const IntVec2& other) const
{
	if( y < other.y)
	{
		return true;
	}

	if(y == other.y)
	{
		return x < other.x;
	}

	return false;
}

//----------------------------------------------------------------------------------
int IntVec2::getManhattenDistance( const IntVec2& lhs, const IntVec2& rhs )
{
	return abs(lhs.x - rhs.x) + abs(lhs.y - rhs.y);
}

//----------------------------------------------------------------------------------
float IntVec2::getDistance( const IntVec2& lhs, const IntVec2& rhs)
{
	return (float) sqrt( (float) ((lhs.x - rhs.x)*(lhs.x - rhs.x) + (lhs.y - rhs.y)*(lhs.y - rhs.y)));
}
