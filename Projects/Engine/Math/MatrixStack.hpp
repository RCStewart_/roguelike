#pragma once
#include <vector>
#include "Matrix_4x4.hpp"

class MatrixStack
{
public:
	MatrixStack(void);
	~MatrixStack(void);

	void initiateStack();
	void clearStack();
	int stackSize();
	void stackPush();
	void stackPop();
	void loadMatrixOntoStack( Matrix_4x4& toAdd );
	Matrix_4x4 getBackOfMatrix();

	//void push(void)
	//{
	//	matrix_type tmp = stack.back(); //required in case the stack's storage gets reallocated
	//	stack.push_back(tmp);
	//}

	//bool pop(void)
	//{
	//	if (size() > 1)
	//	{
	//		stack.pop_back();
	//		return true;
	//	}
	//	else
	//	{
	//		return false;
	//	}
	//}

	//void load(const matrix_type& matrix)
	//{
	//	stack.back() = matrix;
	//}

	//void loadIdentity(void)
	//{
	//	load(matrix_type::identity());
	//}

	//void loadTransposed(const matrix_type& matrix)
	//{
	//	load(transpose(matrix));
	//}

	//void mult(const matrix_type& matrix)
	//{
	//	load(stack.back() * matrix);
	//}

	//void multTransposed(const matrix_type& matrix)
	//{
	//	load(stack.back() * transpose(matrix));
	//}

	//const matrix_type& get(void) const
	//{
	//	return stack.back();
	//}
};

