#pragma once

#include <string>
#include <vector>
#include "Engine\Files\FileParser.hpp"

class Vec3;
class Vec2;
class MeshVertexData;

class ObjAndC23Converter
{
public:
	ObjAndC23Converter();
	~ObjAndC23Converter();

	void writeToFile( const std::string& filePath, const std::string& fileName, MeshVertexData& dataToWrite, unsigned char subType, unsigned char version );
	void readFromC23File( const std::string& filePath, const std::string& fileName, MeshVertexData& outDataToFill );
	void fillMeshVertexDataFromC23File( MeshVertexData& outDataToFill, FileData* dataToParse );
	std::vector<unsigned char> m_outputBuffer;
	std::string objName;

	FileParser m_fileParser;

	unsigned char m_versionNumber;
	unsigned char m_subtypeNumber;
};
