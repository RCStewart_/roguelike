#include "FileParser.hpp"
#include "Engine/Core/GameCommon.hpp"

//----------------------------------------------------------------------------------
FileParser::FileParser()
{

}

//----------------------------------------------------------------------------------
FileParser::~FileParser()
{

}

//----------------------------------------------------------------------------------
void FileParser::writeFloatToBuffer	(std::vector<unsigned char>& outPutVector, const float& toWrite)
{
	float tempFloat = toWrite;
	unsigned char* charToWrite = reinterpret_cast< unsigned char* >(&tempFloat);
	for(int indexOfByteInFloat = 0; indexOfByteInFloat < 4; ++indexOfByteInFloat)
	{
		writeUCharToBuffer(outPutVector, charToWrite[indexOfByteInFloat]);
	}
}

//----------------------------------------------------------------------------------
void FileParser::writeUIntToBuffer	(std::vector<unsigned char>& outPutVector, const unsigned int& toWrite)
{
	unsigned int tempInt = toWrite;
	unsigned char* charToWrite = reinterpret_cast< unsigned char* >(&tempInt);
	for(int indexOfByteInFloat = 0; indexOfByteInFloat < 4; ++indexOfByteInFloat)
	{
		writeUCharToBuffer(outPutVector, charToWrite[indexOfByteInFloat]);
	}
}

//----------------------------------------------------------------------------------
void FileParser::writeIntToBuffer	(std::vector<unsigned char>& outPutVector, const int& toWrite)
{
	int tempInt = toWrite;
	unsigned char* charToWrite = reinterpret_cast< unsigned char* >(&tempInt);
	for(int indexOfByteInFloat = 0; indexOfByteInFloat < 4; ++indexOfByteInFloat)
	{
		writeUCharToBuffer(outPutVector, charToWrite[indexOfByteInFloat]);
	}
}

//----------------------------------------------------------------------------------
void FileParser::writeUCharToBuffer	(std::vector<unsigned char>& outPutVector, const unsigned char& toWrite)
{
	outPutVector.push_back(toWrite);
}

//----------------------------------------------------------------------------------
void FileParser::writeVec3ToBuffer	(std::vector<unsigned char>& outPutVector, const Vec3& toWrite)
{
	writeFloatToBuffer(outPutVector, toWrite.x);
	writeFloatToBuffer(outPutVector, toWrite.y);
	writeFloatToBuffer(outPutVector, toWrite.z);
}

//----------------------------------------------------------------------------------
void FileParser::writeVec2ToBuffer	(std::vector<unsigned char>& outPutVector, const Vec2& toWrite)
{
	writeFloatToBuffer(outPutVector, toWrite.x);
	writeFloatToBuffer(outPutVector, toWrite.y);
}

//----------------------------------------------------------------------------------
void FileParser::writeRGBAToBuffer	(std::vector<unsigned char>& outPutVector, const RGBAColors& toWrite)
{
	writeUCharToBuffer(outPutVector, toWrite.m_red);
	writeUCharToBuffer(outPutVector, toWrite.m_green);
	writeUCharToBuffer(outPutVector, toWrite.m_blue);
	writeUCharToBuffer(outPutVector, toWrite.m_alpha);
}

//----------------------------------------------------------------------------------
FileData* FileParser::generateFileDataFromFile( const std::string& filePath, const std::string& fileName )
{
	UNUSED(fileName);
	FileData* dataToReturn = new FileData();

	FILE* f = nullptr;
	fopen_s( &f, filePath.c_str(), "rb");

	fseek(f, 0, SEEK_END);
	dataToReturn->size = ftell(f);
	rewind(f);

	dataToReturn->startOfBuffer = new char[dataToReturn->size];
	fread(dataToReturn->startOfBuffer , sizeof(char), (size_t) dataToReturn->size, f);

	dataToReturn->buffer = dataToReturn->startOfBuffer;
	fclose(f);

	return dataToReturn;
}

//----------------------------------------------------------------------------------
void FileParser::readFloatFromBuffer( FileData* dataToRead, float& dataOut)
{
	const int floatSize = sizeof( float );

	//Handle endianess
	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------

	dataOut = *((float*) dataToRead->buffer);
	dataToRead->buffer += sizeof( floatSize );
	dataToRead->currentIndex += floatSize;
}

//----------------------------------------------------------------------------------
void FileParser::readIntFromBuffer	( FileData* dataToRead, int& dataOut )
{
	const int intSize = sizeof( int );

	//Handle endianess
	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------

	dataOut = *((int*) dataToRead->buffer);
	dataToRead->buffer += sizeof( intSize );
	dataToRead->currentIndex += intSize;
}

//----------------------------------------------------------------------------------
void FileParser::readUCharFromBuffer( FileData* dataToRead, unsigned char& dataOut)
{
	const int charSize = sizeof( unsigned char);

	//Handle endianess
	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------

	dataOut = *((unsigned char*) dataToRead->buffer);
	dataToRead->buffer += sizeof(unsigned char);
	dataToRead->currentIndex += charSize;
}

//----------------------------------------------------------------------------------
void FileParser::readStringFromBuffer( FileData* dataToRead, std::string& dataOut )
{
	std::string nextString(dataToRead->buffer);
	dataOut = nextString; 
	dataToRead->buffer += dataOut.length() + 1;
	dataToRead->currentIndex += dataOut.length() + 1;
}

//----------------------------------------------------------------------------------
void FileParser::readUIntFromBuffer	( FileData* dataToRead, unsigned int& dataOut)
{
	const unsigned int intSize = sizeof( unsigned int );

	dataOut = *((unsigned int*) dataToRead->buffer);
	dataToRead->buffer += sizeof( intSize );

	dataToRead->currentIndex += intSize;
}

//----------------------------------------------------------------------------------
void FileParser::readVec3FromBuffer	( FileData* dataToRead, Vec3& dataOut)
{
	readFloatFromBuffer(dataToRead, dataOut.x);
	readFloatFromBuffer(dataToRead, dataOut.y);
	readFloatFromBuffer(dataToRead, dataOut.z);
}

//----------------------------------------------------------------------------------
void FileParser::readVec2FromBuffer	( FileData* dataToRead, Vec2& dataOut)
{
	readFloatFromBuffer(dataToRead, dataOut.x);
	readFloatFromBuffer(dataToRead, dataOut.y);
}

//----------------------------------------------------------------------------------
void FileParser::readRGBAFromBuffer	( FileData* dataToRead, RGBAColors& dataOut)
{
	readUCharFromBuffer(dataToRead, dataOut.m_red);
	readUCharFromBuffer(dataToRead, dataOut.m_green);
	readUCharFromBuffer(dataToRead, dataOut.m_blue);
	readUCharFromBuffer(dataToRead, dataOut.m_alpha);
}
