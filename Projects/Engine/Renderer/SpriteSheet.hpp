#pragma once
#include "Engine\Core\Texture.hpp"
#include <string>
#include "Engine\Math\IntVec2.hpp"

using namespace std;
class SpriteSheet
{
public:
	SpriteSheet(void);
	~SpriteSheet(void);
	SpriteSheet(string& fileName, int spritesWide, int spritesHigh);

	Vec2 getMinTextureCoordsForTile(IntVec2& XYCoords);
	Vec2 getMaxTextureCoordsForTile(IntVec2& XYCoords);

	IntVec2 getTileCoordsForIndex(int index);

	static void sLoadAllSpriteSheets(const std::string& filePath);

	string m_imageFileName;
	int m_spritesWide;
	int m_spritesHigh;

	float m_spriteWidth;
	float m_spriteHeight;

	Texture* m_texture;
};

