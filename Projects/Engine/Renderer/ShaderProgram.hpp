#pragma once
#include "Engine\Math\Vec3.hpp"
#include "Engine\Core\RGBAColors.hpp"

struct ShaderUniforms
{
	ShaderUniforms()
	{
		timeUniformLocation				= -1;	
		diffuseMapUniformLocation		= -1;	
		normalMapUniformLocation		= -1;	
		specularMapUniformLocation		= -1;
		emissiveMapUniformLocation		= -1;
		outUniformLocation				= -1;
		projectionMatrixLocation		= -1;
		objectToWorldLocation			= -1;

		fogStartDistanceLocation		= -1;
		fogEndDistanceLocation			= -1;
		fogColorAndIntensityLocation	= -1;
		cameraPositionLocation			= -1;
	}

	int timeUniformLocation;	
	int diffuseMapUniformLocation;	
	int normalMapUniformLocation;	
	int specularMapUniformLocation;
	int emissiveMapUniformLocation;
	int outUniformLocation;
	int projectionMatrixLocation;

	int objectToWorldLocation;

	//----------------------------------------------------------------------------------
	//Fog Variables
	int fogStartDistanceLocation;
	int fogEndDistanceLocation;
	int fogColorAndIntensityLocation;

	int cameraPositionLocation;
};

class ShaderProgram
{
public:
	ShaderProgram(void);
	~ShaderProgram(void);

	ShaderUniforms m_uniforms;

	int m_programID;

	int m_vertexID;
	int m_colorID;
	int m_textureID;
	int m_textCoordsID;

	int m_lightPosID;

	int m_normalID;
	int m_biTangentID;
	int m_tangentID;

	int m_lightWorldPositions		;
	int m_forwardDirections 		;
	int m_ColorOfLights				;
	int m_innerDots					;
	int m_outerDots					;
	int m_ambienceOfLights			;
	int m_innerLightRadius			;
	int m_outerLightRadius			;
	int m_intensityOutsidePenumbra	;

	int m_perlinMapID;
	int m_perlinTexCoordsID;

	//Framebuffer Object variables
	int m_framebufferShaderColorTextureUniformLocation;
	int m_framebufferShaderDepthTextureUniformLocation;
};

