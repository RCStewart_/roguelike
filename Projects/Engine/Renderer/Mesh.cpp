#include "Mesh.hpp"
#include <sstream>
#include <cctype>
#include <cstdio>
#include <vector>
#include "Engine/Renderer/MeshVertexData.hpp"
#include "Engine/Renderer/GLWrapper.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Math/Matrix_4x4.hpp"
#include "Engine/Files/ObjAndC23Converter.hpp"

#include <algorithm>
#include <set>
#include <iterator>
#include <unordered_map>

#include "Engine/Core/Texture.hpp"

//----------------------------------------------------------------------------------
//Provided by Squirrel Eiserloh
//-----------------------------------------------------------------------------------------------
// Returns TRUE if the Bitangent is "right handed" with respect to the Normal and Tangent;
// i.e. if [Tangent,Bitangent,Normal] is a right-handed basis.  Typically this bool is
// converted to a +/- 1.0 and passed into a vertex shader as the Tangent's "w" coordinate.
// The Bitangent is then computed within the vertex shader using cross product and "w" to
// disambiguate between which of the two possible perpendiculars to use for the Bitangent.
//
bool ComputeSurfaceTangentsAtVertex(
	Vec3& surfaceTangentAtVertex_out,
	Vec3& surfaceBitangentAtVertex_out,
	const Vec3& normalAtThisVertex,
	const Vec3& positionOfThisVertex,
	const Vec2& texCoordsOfThisVertex,
	const Vec3& positionOfPreviousAdjacentVertex,
	const Vec2& texCoordsOfPreviousAdjacentVertex,
	const Vec3& positionOfNextAdjacentVertex,
	const Vec2& texCoordsOfNextAdjacentVertex )
{
	Vec3 vecToPrevious	= positionOfPreviousAdjacentVertex - positionOfThisVertex;
	Vec3 vecToNext		= positionOfNextAdjacentVertex - positionOfThisVertex;

	Vec2 texToPrevious	= texCoordsOfPreviousAdjacentVertex - texCoordsOfThisVertex;
	Vec2 texToNext		= texCoordsOfNextAdjacentVertex - texCoordsOfThisVertex;

	float determinant = ((texToPrevious.x * texToNext.y) - (texToNext.x * texToPrevious.y));

	Vec3 uDirectionInWorldSpace(	
		(texToNext.y * vecToPrevious.x - texToPrevious.y * vecToNext.x),
		(texToNext.y * vecToPrevious.y - texToPrevious.y * vecToNext.y),
		(texToNext.y * vecToPrevious.z - texToPrevious.y * vecToNext.z)
		);

	Vec3 vDirectionInWorldSpace(
		(texToPrevious.x * vecToNext.x - texToNext.x * vecToPrevious.x),
		(texToPrevious.x * vecToNext.y - texToNext.x * vecToPrevious.y),
		(texToPrevious.x * vecToNext.z - texToNext.x * vecToPrevious.z)
		);

	float invDeterminant = 1.0f / determinant;
	uDirectionInWorldSpace.scalarMultiplication(invDeterminant);
	vDirectionInWorldSpace.scalarMultiplication(invDeterminant);

	surfaceTangentAtVertex_out = uDirectionInWorldSpace;
	surfaceBitangentAtVertex_out = /*-*/vDirectionInWorldSpace; // NOTE: You should remove this minus sign if your V texture coordinates are using the opposite convention of mine!

	//surfaceTangentAtVertex_out = vDirectionInWorldSpace;
	//surfaceBitangentAtVertex_out = uDirectionInWorldSpace; 

	// NOTE: You don't need the following code, unless you intend to reconstitute the Bitangent vector inside the vertex shader (and not pass it in as a vertex attribute)
	Vec3 naturalBitangentFromCross = normalAtThisVertex * surfaceTangentAtVertex_out;
	float computedBitangentDotNaturalBitangent = Vec3::dotProduct( surfaceBitangentAtVertex_out, naturalBitangentFromCross );
	return( computedBitangentDotNaturalBitangent >= 0.f );
}


//----------------------------------------------------------------------------------
Mesh::Mesh(void)
	:m_emissiveID(nullptr)
	,m_specularID(nullptr)
	,m_normalID(nullptr)
	,m_diffuseID(nullptr)
	,m_iboID(0)
	,m_vboID(0)
{
}

//----------------------------------------------------------------------------------
Mesh::~Mesh(void)
{
}


//----------------------------------------------------------------------------------------------
bool Mesh::loadMeshFromOBJFileAndCook( std::string& fileName, std::string& name, std::string& baseDataFolder, const std::string& directoryToWriteTo )
{
	bool isLoaded = false;

	std::string directoryName = baseDataFolder;
	std::string writeToDirectoryName = directoryToWriteTo;
	writeToDirectoryName += name;
	writeToDirectoryName += ".C23";
	directoryName += fileName;

	FILE* f = nullptr;
	fopen_s( &f, directoryName.c_str(), "rb");

	if(f == nullptr)
	{
		return isLoaded;
	}
	isLoaded = true;

	fseek(f, 0, SEEK_END);
	size_t fileSize = ftell(f);
	rewind(f);

	unsigned char* buffer = new unsigned char[fileSize];

	fread(buffer , sizeof(unsigned char), (size_t) fileSize, f);
	fclose(f);

	std::string temp;

	gatherVertexesFromOBJBufferAndCook(buffer, fileSize, writeToDirectoryName, name);
	delete[] buffer;
	return isLoaded;
}

//----------------------------------------------------------------------------------------------
bool Mesh::loadMeshFromC23File( std::string& filePath, std::string& name )
{
	bool isLoaded = false;

	std::string directoryName = filePath;
	std::string readFromDirectoryName = filePath;
	readFromDirectoryName += name;
	readFromDirectoryName += ".C23";

	MeshVertexData dataToFill = MeshVertexData();
	ObjAndC23Converter converter;

	dataToFill.clear();

	converter.readFromC23File( readFromDirectoryName, name, dataToFill );

	GLWrapperGenerateVBO( m_vboID, dataToFill.m_vertexes );
	GLWrapperGenerateIBO( m_iboID, dataToFill.m_indexes );
	m_numOfVertexes = dataToFill.m_vertexes.size();
	m_numIndexes = dataToFill.m_indexes.size();

	loadTextures( directoryName, name );
	return isLoaded;
}

//----------------------------------------------------------------------------------
void Mesh::loadTextures( std::string& filePath, std::string& fileName)
{

	std::string diffuse = fileName + "_Diffuse.png";
	std::string specular = fileName + "_SpecGlossEmit.png";
	std::string normal = fileName + "_Normal.png";
	std::string emmissive = fileName + "_SpecGlossEmit.png";

	diffuse		= filePath + diffuse;
	specular	= filePath + specular;
	normal		= filePath + normal;
	emmissive	= filePath + emmissive;

	m_emissiveID =	Texture::CreateOrGetTexture( emmissive, DefaultTextureSpecGlossEmissive );
	m_specularID =	Texture::CreateOrGetTexture( specular, DefaultTextureSpecGlossEmissive );
	m_normalID =	Texture::CreateOrGetTexture( normal, DefaultTextureNormal );
	m_diffuseID =	Texture::CreateOrGetTexture( diffuse, DefaultTextureDiffuse );
}

//----------------------------------------------------------------------------------
void Mesh::gatherVertexesFromOBJBufferAndCook(unsigned char* buffer, size_t sizeOfBuffer, std::string fileName, std::string filePath)
{
	Matrix_4x4 changeOfBasis;

	MeshVertexData vertexData;
	std::string fromFile;
	bool hasNormals = false;
	for(int i = 0; i < (int) sizeOfBuffer; ++i)
	{
		if(buffer[i] == 'v' && (((buffer[i - 1] == '\r') || (buffer[i - 1] == '\n')) && (isspace(buffer[i + 1]))))
		{
			++i;
			seekToNonWhiteSpace(buffer, i , vertexData);
			get3VertexNumbersForOBJ( buffer, i, vertexData );
		}
		if(buffer[i] == 'v' && (((buffer[i - 1] == '\r') || (buffer[i - 1] == '\n')) && (buffer[i + 1] == 'n')))
		{
			hasNormals = true;
			++i;
			++i;
			seekToNonWhiteSpace(buffer, i , vertexData);
			get3NormalNumbersForOBJ( buffer, i, vertexData );
		}
		if(buffer[i] == 'v' && (((buffer[i - 1] == '\r') || (buffer[i - 1] == '\n')) && (buffer[i + 1] == 't')))
		{
			++i;
			++i;
			seekToNonWhiteSpace(buffer, i , vertexData);
			getTextureCoordsForOBJ( buffer, i, vertexData );
		}
		if(buffer[i] == '#')
		{
			switchOnOBJMetaData( buffer, i, vertexData, changeOfBasis );
		}
		if(buffer[i] == 'f' && ((isspace(buffer[i - 1])) && (isspace(buffer[i + 1]))))
		{
			++i;
			parseOneLineForIndices( buffer, i, vertexData );
		}
	}

	for( int i = 0; i < (int) vertexData.m_vertexes.size(); ++i )
	{
		vertexData.m_vertexes[i].m_pos = changeOfBasis.multiplyVec3By3x3Matrix( vertexData.m_vertexes[i].m_pos );
	}
	for( int i = 0; i < (int) vertexData.m_normals.size(); ++i )
	{
		vertexData.m_normals[i] = changeOfBasis.multiplyVec3By3x3Matrix( vertexData.m_normals[i] );
	}

	std::vector<ModelVertex> uniqueVertexes;
	std::vector<int> m_indexes;
	std::vector< ModelVertex > tempVertexes;
	
	uniqueVertexes.reserve(vertexData.m_indexes.size());
	m_indexes.reserve(vertexData.m_indexes.size());
	tempVertexes.reserve(vertexData.m_indexes.size());
	
	std::unordered_map< ModelVertex, int, MyHash<ModelVertex>> vertexToIDMap;
	vertexToIDMap.rehash(tempVertexes.size());
	
	for(int i = 0; i < (int) vertexData.m_indexes.size(); ++i)
	{
		tempVertexes.push_back(vertexData.m_vertexes[vertexData.m_indexes[i] - 1]);

		if((tempVertexes.size() - 1) < vertexData.m_normalIndexes.size() && hasNormals )
		{
			int normalIndex = vertexData.m_normalIndexes[i] - 1;
			tempVertexes[tempVertexes.size() - 1].m_normal = vertexData.m_normals[normalIndex];
		}
		if((tempVertexes.size() - 1) < vertexData.m_textureIndexes.size() )
		{
			int textureIndex = vertexData.m_textureIndexes[i] - 1;
			tempVertexes[tempVertexes.size() - 1].m_texCoords = vertexData.m_textureCoords[textureIndex];
		}

		//----------------------------------------------------------------------------------
		//----------------------------------------------------------------------------------
		m_indexes.push_back(i);

		//bool wasFound = false;

		if ( vertexToIDMap.find(tempVertexes[i]) == vertexToIDMap.end() )
		{
			// not found
			vertexToIDMap[tempVertexes[i]] = uniqueVertexes.size();
			m_indexes[i] = uniqueVertexes.size();
			uniqueVertexes.push_back(tempVertexes[i]);
		} else {
			// found
			m_indexes[i] = vertexToIDMap[tempVertexes[i]];
		}

	}

	vertexData.m_vertexes.clear();
	vertexData.m_vertexes.swap( uniqueVertexes );

	//NOT WORKING
	//if(!hasNormals)
	//{
	//	//vector<ModelVertex> temp;
	//	//for(size_t index = 0; index < m_indexes.size(); ++index)
	//	//{
	//	//	temp.push_back(vertexData.m_vertexes[m_indexes[index] - 1 ]);
	//	//}
	//	//vertexData.m_vertexes.clear();
	//	//vertexData.m_vertexes.swap( temp );
	//	for( size_t index = 0; index < m_indexes.size(); index = index + 3 )
	//	{
	//		Vec3 prev  = vertexData.m_vertexes[index].m_pos;
	//		Vec3 currentPoint = vertexData.m_vertexes[index + 1].m_pos;

	//		Vec3 next  = vertexData.m_vertexes[index + 2].m_pos;

	//		Vec3 V = next - prev;
	//		Vec3 U = currentPoint - prev;
	//		Vec3 theNormal = Vec3();

	//		theNormal.x = U.y * V.x - U.z * V.y;
	//		theNormal.y = U.z * V.x - U.x * V.z;
	//		theNormal.z = U.x * V.y - U.y * V.x;
	//		theNormal.normalize();

	//		vertexData.m_vertexes[ index ].m_normal = theNormal;
	//		vertexData.m_vertexes[ index + 1 ].m_normal = theNormal;
	//		vertexData.m_vertexes[ index + 2 ].m_normal = theNormal;
	//	}
	//}

	for( size_t index = 0; index < m_indexes.size(); index = index + 3 )
	{
		
		ModelVertex& first = vertexData.m_vertexes[m_indexes[index]];
		ModelVertex& second = vertexData.m_vertexes[m_indexes[index + 1]];
		ModelVertex& third = vertexData.m_vertexes[m_indexes[index + 2]];

		ComputeSurfaceTangentsAtVertex(
			second.m_tangent,
			second.m_bitangent,
			second.m_normal,
			second.m_pos,
			second.m_texCoords,
			first.m_pos,
			first.m_texCoords,
			third.m_pos,
			third.m_texCoords );

		first.m_bitangent = second.m_bitangent;
		first.m_tangent = second.m_tangent;
		third.m_bitangent = second.m_bitangent;
		third.m_tangent = second.m_tangent;
	}

	vertexData.m_indexes.swap( m_indexes );

	ObjAndC23Converter cookData;

	unsigned char version = 1;
	unsigned char subType = 2;
	cookData.writeToFile( filePath, fileName, vertexData, subType, version );
}


//----------------------------------------------------------------------------------------------
void Mesh::switchOnOBJMetaData( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, Matrix_4x4& changeOfBasis )
{
	while(buffer[index] == '#')
	{
		++index;
	}
	string metaDataType;
	while(!isspace(buffer[index]) )
	{
		metaDataType += buffer[index];
		++index;
	}
	if(buffer[index] != '#')
	{		
		++index;
	}
	if(metaDataType == "GrimGrumMeta")
	{
		switchOnOBJGrimGrumMetaData( buffer, index, storedVertexes, changeOfBasis );
	}
	if(buffer[index] == '#')
	{
		switchOnOBJMetaData( buffer, index, storedVertexes, changeOfBasis );
	}
}

//----------------------------------------------------------------------------------------------
void Mesh::switchOnOBJGrimGrumMetaData( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, Matrix_4x4& changeOfBasis )
{
	string metaDataType;
	while(!isspace(buffer[index]))
	{
		metaDataType += buffer[index];
		++index;
	}
	seekToNonWhiteSpace(buffer, index, storedVertexes);
	if(metaDataType == "unitsPerMeter")
	{
		string unitsPerMeter;
		while(!isspace(buffer[index]))
		{
			unitsPerMeter += buffer[index];
			++index;
		}
		float scale = 1.f / stof(unitsPerMeter);
		changeOfBasis.setScale(scale);
	}
	if(metaDataType == "basis")
	{
		vector<string> xyz;
		xyz.push_back(string());
		xyz.push_back(string());
		xyz.push_back(string());

		for( int i = 0; i < (int) xyz.size(); ++i )
		{
			while(buffer[index] == 'x' 
				|| buffer[index] == 'y' 
				|| buffer[index] == 'z' 
				|| buffer[index] == '=' )
			{
				++index;
			}
			while( !isspace(buffer[index]) && buffer[index] != '#')
			{
				xyz[i] += buffer[index];
				if(buffer[index] != '#')
				{		
					++index;
				}
			}
			if(buffer[index] != '#')
			{		
				++index;
			}
		}

		changeOfBasis.setTo3x3Matrix(GetVec3FromStringDirection(xyz[0]), GetVec3FromStringDirection(xyz[1]), GetVec3FromStringDirection(xyz[2]));
	}
}

//----------------------------------------------------------------------------------
void Mesh::get3VertexNumbersForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes  )
{
	ModelVertex newVertex;
	std::vector<float> numbersCollected;

	std::string currentNumber; 

	while( numbersCollected.size() < 3 )
	{
		if( !isspace(buffer[index]))
		{
			currentNumber += buffer[index];
			++index;
		}
		else
		{
			seekToNonWhiteSpace( buffer, index, storedVertexes );
			if( !currentNumber.empty() )
			{
				float convertedValue = std::stof( currentNumber );
				currentNumber.clear();
				numbersCollected.push_back(convertedValue);
			}
		}
	}
	--index;
	newVertex.m_pos = Vec3(numbersCollected[0], numbersCollected[1], numbersCollected[2]);
	storedVertexes.m_vertexes.push_back(newVertex);
}

//----------------------------------------------------------------------------------
void Mesh::get3NormalNumbersForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes  )
{
	Vec3 newNormal;
	std::vector<float> numbersCollected;

	std::string currentNumber; 

	while( numbersCollected.size() < 3 )
	{
		if( !isspace(buffer[index]))
		{
			currentNumber += buffer[index];
			++index;
		}
		else
		{
			seekToNonWhiteSpace( buffer, index, storedVertexes );
			if( !currentNumber.empty() )
			{
				float convertedValue = std::stof( currentNumber );
				currentNumber.clear();
				numbersCollected.push_back(convertedValue);
			}
		}
	}
	--index;
	newNormal = Vec3(numbersCollected[0], numbersCollected[1], numbersCollected[2]);
	newNormal.normalize();
	storedVertexes.m_normals.push_back(newNormal);
}

//----------------------------------------------------------------------------------
void Mesh::getTextureCoordsForOBJ( unsigned char* buffer, int& index, MeshVertexData& storedVertexes  )
{
	Vec2 newTextureCoord;
	std::vector<float> numbersCollected;

	std::string currentNumber; 

	while( numbersCollected.size() < 2 )
	{
		if( !isspace(buffer[index]))
		{
			currentNumber += buffer[index];
			++index;
		}
		else
		{
			seekToNonWhiteSpace( buffer, index, storedVertexes );
			if( !currentNumber.empty() )
			{
				float convertedValue = std::stof( currentNumber );
				currentNumber.clear();
				numbersCollected.push_back(convertedValue);
			}
		}
	}
	--index;
	
	numbersCollected[1] = 1 - numbersCollected[1];
	newTextureCoord = Vec2(numbersCollected[0], numbersCollected[1]);
	storedVertexes.m_textureCoords.push_back(newTextureCoord);
}

//----------------------------------------------------------------------------------
void Mesh::seekToNonWhiteSpace( unsigned char* buffer, int& index, MeshVertexData& storedVertexes  )
{
	UNUSED(storedVertexes);
	while( isspace(buffer[index]) )
	{
		++index;
	}
}

//----------------------------------------------------------------------------------
void Mesh::seekToWhiteSpace( unsigned char* buffer, int& index, MeshVertexData& storedVertexes  )
{
	UNUSED(storedVertexes);
	while( !isspace(buffer[index]) )
	{
		++index;
	}
}

//----------------------------------------------------------------------------------
void Mesh::seekToNextNonSpaceAndNonDigit( unsigned char* buffer, int& index, MeshVertexData& storedVertexes )
{
	UNUSED(storedVertexes);
	while( isspace( buffer[index] ) || isdigit( buffer[index] ) )
	{
		++index;
	}
}

//----------------------------------------------------------------------------------
void Mesh::seekToNextCharOfType( char type, unsigned char* buffer, int& index, MeshVertexData& storedVertexes )
{
	UNUSED(storedVertexes);
	while( buffer[index] != type )
	{
		++index;
	}
}

//----------------------------------------------------------------------------------
void Mesh::parseOneLineForIndices( unsigned char* buffer, int& index, MeshVertexData& storedVertexes )
{
	ModelVertex newVertex;
	std::vector<float> vertexIndexesCollected;
	std::vector<float> normalIndexesCollected;
	std::vector<float> textureIndexesCollected;
	std::string currentNumber; 

	int currentState = 0; 

	seekToNonWhiteSpace( buffer, index, storedVertexes );

	while( buffer[index] != '\n' && buffer[index] != '\r' )
	{
		if( isdigit(buffer[index]) || ( buffer[index] == '-' /*&& currentNumber.size() < 1*/) )
		{
			currentNumber += buffer[index];
			++index;
		}
		else if( buffer[index] == '/'  && vertexIndexesCollected.size() < 3)
		{
			if(currentState == 0)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = std::stof( currentNumber );
					vertexIndexesCollected.push_back(convertedValue);
				}
				currentNumber.clear();
				++index;
				++currentState;
			}
			else if(currentState == 1)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = std::stof( currentNumber );
					textureIndexesCollected.push_back(convertedValue);
				}
				currentNumber.clear();
				++index;
				++currentState;
			}
			else if(currentState == 2)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = std::stof( currentNumber );
					normalIndexesCollected.push_back(convertedValue);
				}
				currentNumber.clear();
				currentState = 0;

				std::vector<char> charsToStopOn;
				charsToStopOn.push_back('f');
				charsToStopOn.push_back('\n');
				charsToStopOn.push_back('\r');
				charsToStopOn.push_back(' ');
				seekUntilYouReachOneOfTheseChars(buffer, index, storedVertexes, charsToStopOn, true );
				++index;
			}
			//++index;

		}
		else if( buffer[index] == '/'  && vertexIndexesCollected.size() >= 3)
		{
			if(currentState == 0)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = (float) atoi( currentNumber.c_str() );
					vertexIndexesCollected.push_back(vertexIndexesCollected[0]);
					vertexIndexesCollected.push_back(vertexIndexesCollected[vertexIndexesCollected.size() - 2]);
					vertexIndexesCollected.push_back(convertedValue);

				}
				currentNumber.clear();
				++index;
				++currentState;
			}
			else if(currentState == 1)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = std::stof( currentNumber );
					if(vertexIndexesCollected.size() > 3)
					{
						textureIndexesCollected.push_back(textureIndexesCollected[0]);
						textureIndexesCollected.push_back(textureIndexesCollected[textureIndexesCollected.size() - 2]);
					}
					textureIndexesCollected.push_back(convertedValue);
				}
				currentNumber.clear();
				++index;
				++currentState;
			}
			else if(currentState == 2)
			{
				if( !currentNumber.empty())
				{
					float convertedValue = (float) atoi( currentNumber.c_str() );
					normalIndexesCollected.push_back(normalIndexesCollected[0]);
					normalIndexesCollected.push_back(normalIndexesCollected[normalIndexesCollected.size() - 2]);
					normalIndexesCollected.push_back(convertedValue);
				}
				currentNumber.clear();

				currentState = 0;

				std::vector<char> charsToStopOn;
				charsToStopOn.push_back('f');
				charsToStopOn.push_back('\n');
				charsToStopOn.push_back('\r');
				charsToStopOn.push_back(' ');
				seekUntilYouReachOneOfTheseChars(buffer, index, storedVertexes, charsToStopOn, true );

				++index;
			}
		}
		else if( isspace(buffer[index]) )
		{
			if(currentState == 2)
			{
				if(currentNumber.size() > 0)
				{
					if(vertexIndexesCollected.size() <= 3 )
					{
						float convertedValue = std::stof( currentNumber );
						normalIndexesCollected.push_back(convertedValue);
					}
					else
					{
						float convertedValue = (float) atoi( currentNumber.c_str() );
						normalIndexesCollected.push_back(normalIndexesCollected[0]);
						normalIndexesCollected.push_back(normalIndexesCollected[normalIndexesCollected.size() - 2]);
						normalIndexesCollected.push_back(convertedValue);
					}
				}
				currentState = 0;
			}
			currentNumber.clear();
			++index;
		}

	}

	if(currentState == 2)
	{
		if(currentNumber.size() > 0)
		{
			if(vertexIndexesCollected.size() <= 3 )
			{
				float convertedValue = std::stof( currentNumber );
				normalIndexesCollected.push_back(convertedValue);
			}
			else
			{
				float convertedValue = (float) atoi( currentNumber.c_str() );
				normalIndexesCollected.push_back(normalIndexesCollected[0]);
				normalIndexesCollected.push_back(normalIndexesCollected[normalIndexesCollected.size() - 2]);
				normalIndexesCollected.push_back(convertedValue);
			}
		}
		currentState = 0;
	}

	seekToNextNonSpaceAndNonDigit( buffer, index, storedVertexes );
	if(buffer[index] != '\n' && buffer[index] != '\r')
		--index;
	//Store
	for( int i = 0; i < (int) vertexIndexesCollected.size(); ++i)
	{
		addNumberToIndexes(storedVertexes.m_vertexes.size(), storedVertexes.m_indexes, (int) vertexIndexesCollected[i]);
	}
	for( int i = 0; i < (int) normalIndexesCollected.size(); ++i)
	{
		addNumberToIndexes(storedVertexes.m_normals.size(), storedVertexes.m_normalIndexes, (int) normalIndexesCollected[i]);
	}
	for( int i = 0; i < (int) textureIndexesCollected.size(); ++i)
	{
		addNumberToIndexes(storedVertexes.m_textureCoords.size(), storedVertexes.m_textureIndexes, (int) textureIndexesCollected[i]);
	}
}

//----------------------------------------------------------------------------------
void Mesh::seekUntilYouReachOneOfTheseChars( unsigned char* buffer, int& index, MeshVertexData& storedVertexes, std::vector<char>& chars, bool includeIsSpace )
{
	UNUSED(storedVertexes);
	bool keepChecking = true;
	while( keepChecking )
	{
		for( int charToCheckIndex = 0; charToCheckIndex < (int) chars.size(); ++charToCheckIndex )
		{
			if( buffer[index] == chars[charToCheckIndex] )
			{
				return;
			}
			if(includeIsSpace)
			{
				if(isspace(buffer[index]))
					return;
			}
		}
		++index;
	}
}

//----------------------------------------------------------------------------------
void Mesh::addNumberToIndexes( int sizeOfVectorToBeIndexed, std::vector<int>& storedIndices, int indexToAdd )
{
	if((int) storedIndices.size() < 0)
		return;
	if( indexToAdd < 0)
	{
		storedIndices.push_back( sizeOfVectorToBeIndexed + indexToAdd + 1 );
	}
	else
	{
		storedIndices.push_back( indexToAdd );
	}
}

//----------------------------------------------------------------------------------
int Mesh::convertStringToInt( std::string& toConvert )
{
	std::string tempBuffer;
	for(int i = 0; i < (int) toConvert.size(); ++i)
	{
		if( toConvert[i] == '-' && i != 0)
		{
			i = toConvert.size();
			continue;
		}
		else if( isdigit(toConvert[i]) || toConvert[i] == '-')
		{
			tempBuffer += toConvert[i];
		}
		else
		{
			i = toConvert.size();
			continue;
		}
	}
	return (int) std::stof( tempBuffer );
}

