#pragma once
#include <string>

class Material;

class FBO
{
public:
	FBO(void);
	~FBO(void);
	
	void startFramebuffer();
	void disableFramebuffer();

	unsigned int m_framebufferObjectID;
	unsigned int m_framebufferColorTextureID;
	unsigned int m_framebufferDepthTextureID;

	/*int m_framebufferShaderColorTextureUniformLocation;
	int m_framebufferShaderDepthTextureUniformLocation;*/

	//int m_framebufferEffectShaderProgramID;

	Material* m_material;
};

