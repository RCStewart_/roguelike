#include "Engine\Renderer\Renderer.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Renderer\Shader.hpp"
#include "Engine\Math\Vec3.hpp"
#include "Engine\Core\Texture.hpp"
#include "Engine\Math\IntVec2.hpp"
#include "Engine\Math\Time.hpp"
//#include "GameCommon.hpp"
#include "Engine\Renderer\Material.hpp"
#include "Engine\Math\RandomNumber.hpp"
#include "Engine\Core\UtilityFunctions.hpp"
#include "Engine\Renderer\FBO.hpp"
#include <sstream>


class SpriteSheet;
const float DEG_TO_RAD = 0.0174532925f;
Renderer* g_theRenderer = nullptr;
//----------------------------------------------------------------------------------
Renderer::Renderer(void)
{
	if (!g_theRenderer)
		g_theRenderer = this;
}

//----------------------------------------------------------------------------------
void Renderer::init()
{
	clearStack();
	m_nextAttribIDNum = 1;
	m_currentMaterialID = 0;
}

//----------------------------------------------------------------------------------
Renderer::~Renderer(void)
{

}

//----------------------------------------------------------------------------------
int Renderer::createMaterial( const string& fragShaderPath, const string& vertexShaderPath )
{
	m_materials.push_back( new Material(fragShaderPath, vertexShaderPath, m_nextAttribIDNum));
	return m_materials.size() - 1;
}

//----------------------------------------------------------------------------------
void Renderer::addTexturesToMaterial(int materialIndex, Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive)
{
	m_materials[materialIndex]->addTextures( diffuse, normal, specular, emissive );
}

//----------------------------------------------------------------------------------
void Renderer::setUniforms()
{
	for( int i = 0; i < (int) m_materials.size(); ++i )
	{
		m_materials[i]->setShaderUniforms();
	}
}

//----------------------------------------------------------------------------------
void Renderer::disableShaderProgram()
{
	GLWrapperDisableShader();
}

//----------------------------------------------------------------------------------
void Renderer::generateAndRenderSphere(float radius, unsigned int rings, unsigned int sectors, Vec3 position, int materialID)
{	
	m_currentMaterialID = materialID;
	m_materials[materialID]->useProgram();
	m_materials[materialID]->setShaderUniforms();
	GLWModernSphere(radius, rings, sectors, position, m_materials[ materialID ] );
	disableShaderProgram();
}

//----------------------------------------------------------------------------------
void Renderer::generateAndRenderTexturedAABB( const Vec3& minPos, const Vec3& maxPos, Vec3 translation, int materialID )
{	
	m_currentMaterialID = materialID;

	Vec2 minCoords = Vec2(0.f, 0.f);
	Vec2 maxCoords = Vec2(1.f, 1.f);

	vector<Vertex3D_PCT> vertexes;
	
	for(int i = 0; i < 24; ++i)
	{
		vertexes.push_back(Vertex3D_PCT());
	}
	
	for(int i = 0; i < 24; ++i)
	{
		vertexes[i].m_color = RGBAColors(1, 1, 1, 1);
	}

	for(int i = 0; i < 24; i += 4)
	{
		vertexes[i + 3].m_texCoords = Vec2(minCoords.x, minCoords.y);
		vertexes[i + 0].m_texCoords = Vec2(minCoords.x, maxCoords.y);
		vertexes[i + 1].m_texCoords = Vec2(maxCoords.x, maxCoords.y);
		vertexes[i + 2].m_texCoords = Vec2(maxCoords.x, minCoords.y);
	}
	
	//Bot
	vertexes[0].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[0].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[0].m_normal = Vec3(0.f, 0.f, -1.f);

	vertexes[1].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[1].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[1].m_normal = Vec3(0.f, 0.f, -1.f);

	vertexes[2].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[2].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[2].m_normal = Vec3(0.f, 0.f, -1.f);

	vertexes[3].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[3].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[3].m_normal = Vec3(0.f, 0.f, -1.f);

	vertexes[0].m_position.x = minPos.x;
	vertexes[0].m_position.y = maxPos.y;
	vertexes[0].m_position.z = minPos.z;

	vertexes[1].m_position.x = maxPos.x;
	vertexes[1].m_position.y = maxPos.y;
	vertexes[1].m_position.z = minPos.z;

	vertexes[2].m_position.x = maxPos.x;
	vertexes[2].m_position.y = minPos.y;
	vertexes[2].m_position.z = minPos.z;
	
	vertexes[3].m_position.x = minPos.x;
	vertexes[3].m_position.y = minPos.y;
	vertexes[3].m_position.z = minPos.z;
	
	//Top
	vertexes[4].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[4].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[4].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[5].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[5].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[5].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[6].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[6].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[6].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[7].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[7].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[7].m_normal = Vec3(0.f, 0.f, 1.f);
	
	vertexes[4].m_position.x = minPos.x;
	vertexes[4].m_position.y = minPos.y;
	vertexes[4].m_position.z = maxPos.z;

	vertexes[5].m_position.x = maxPos.x;
	vertexes[5].m_position.y = minPos.y;
	vertexes[5].m_position.z = maxPos.z;
	
	vertexes[6].m_position.x = maxPos.x;
	vertexes[6].m_position.y = maxPos.y;
	vertexes[6].m_position.z = maxPos.z;

	vertexes[7].m_position.x = minPos.x;
	vertexes[7].m_position.y = maxPos.y;
	vertexes[7].m_position.z = maxPos.z;
	
	//South
	vertexes[11].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[11].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[11].m_normal = Vec3(0.f, -1.f, 0.f);

	vertexes[8].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[8].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[8].m_normal = Vec3(0.f, -1.f,0.f);

	vertexes[9].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[9].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[9].m_normal = Vec3(0.f, -1.f, 0.f);

	vertexes[10].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[10].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[10].m_normal = Vec3(0.f, -1.f, 0.f);

	vertexes[11].m_position.x = minPos.x;
	vertexes[11].m_position.y = minPos.y;
	vertexes[11].m_position.z = maxPos.z;
	
	vertexes[8].m_position.x = minPos.x;
	vertexes[8].m_position.y = minPos.y;
	vertexes[8].m_position.z = minPos.z;
	
	vertexes[9].m_position.x = maxPos.x;
	vertexes[9].m_position.y = minPos.y;
	vertexes[9].m_position.z = minPos.z;
	
	vertexes[10].m_position.x = maxPos.x;
	vertexes[10].m_position.y = minPos.y;
	vertexes[10].m_position.z = maxPos.z;
	
	//East
	vertexes[15].m_tangent = Vec3(0.f, 1.f, 0.f);
	vertexes[15].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[15].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[12].m_tangent = Vec3(0.f, 1.f, 0.f);
	vertexes[12].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[12].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[13].m_tangent = Vec3(0.f, 1.f, 0.f);
	vertexes[13].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[13].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[14].m_tangent = Vec3(0.f, 1.f, 0.f);
	vertexes[14].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[14].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[15].m_position.x = maxPos.x;
	vertexes[15].m_position.y = minPos.y;
	vertexes[15].m_position.z = maxPos.z;
	
	vertexes[12].m_position.x = maxPos.x;
	vertexes[12].m_position.y = minPos.y;
	vertexes[12].m_position.z = minPos.z;
	
	vertexes[13].m_position.x = maxPos.x;
	vertexes[13].m_position.y = maxPos.y;
	vertexes[13].m_position.z = minPos.z;
	
	vertexes[14].m_position.x = maxPos.x;
	vertexes[14].m_position.y = maxPos.y;
	vertexes[14].m_position.z = maxPos.z;
	
	//North
	vertexes[19].m_tangent = Vec3(-1.f, 0.f, 0.f);
	vertexes[19].m_biTangent = Vec3(0.f, 0.f, 1.f);
	vertexes[19].m_normal = Vec3(0.f, 1.f, 0.f);

	vertexes[16].m_tangent = Vec3(-1.f, 0.f, 0.f);
	vertexes[16].m_biTangent = Vec3(0.f, 0.f, 1.f);
	vertexes[16].m_normal = Vec3(0.f, 1.f, 0.f);

	vertexes[17].m_tangent = Vec3(-1.f, 0.f, 0.f);
	vertexes[17].m_biTangent = Vec3(0.f, 0.f, 1.f);
	vertexes[17].m_normal = Vec3(0.f, 1.f, 0.f);

	vertexes[18].m_tangent = Vec3(-1.f, 0.f, 0.f);
	vertexes[18].m_biTangent = Vec3(0.f, 0.f, 1.f);
	vertexes[18].m_normal = Vec3(0.f, 1.f, 0.f);


	vertexes[19].m_position.x = maxPos.x;
	vertexes[19].m_position.y = maxPos.y;
	vertexes[19].m_position.z = maxPos.z;
	
	vertexes[16].m_position.x = maxPos.x;
	vertexes[16].m_position.y = maxPos.y;
	vertexes[16].m_position.z = minPos.z;
	
	vertexes[17].m_position.x = minPos.x;
	vertexes[17].m_position.y = maxPos.y;
	vertexes[17].m_position.z = minPos.z;
	
	vertexes[18].m_position.x = minPos.x;
	vertexes[18].m_position.y = maxPos.y;
	vertexes[18].m_position.z = maxPos.z;
	
	//West
	vertexes[23].m_tangent = Vec3(0.f, -1.f, 0.f);
	vertexes[23].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[23].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[20].m_tangent = Vec3(0.f, -1.f, 0.f);
	vertexes[20].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[20].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[21].m_tangent = Vec3(0.f, -1.f, 0.f);
	vertexes[21].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[21].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[22].m_tangent = Vec3(0.f, -1.f, 0.f);
	vertexes[22].m_biTangent = Vec3(0.f, 0.f, -1.f);
	vertexes[22].m_normal = Vec3(1.f, 0.f, 0.f);

	vertexes[23].m_position.x = minPos.x;
	vertexes[23].m_position.y = maxPos.y;
	vertexes[23].m_position.z = maxPos.z;
	
	vertexes[20].m_position.x = minPos.x;
	vertexes[20].m_position.y = maxPos.y;
	vertexes[20].m_position.z = minPos.z;
	
	vertexes[21].m_position.x = minPos.x;
	vertexes[21].m_position.y = minPos.y;
	vertexes[21].m_position.z = minPos.z;
	
	vertexes[22].m_position.x = minPos.x;
	vertexes[22].m_position.y = minPos.y;
	vertexes[22].m_position.z = maxPos.z;
	
	m_materials[materialID]->useProgram();
	m_materials[materialID]->setShaderUniforms();

	Vec2 perlinTextCoordsX = Vec2(translation.x * .01f, translation.x * .01f + maxPos.x * .01f);
	Vec2 perlinTextCoordsY = Vec2(translation.y * .01f, translation.y * .01f- maxPos.y * .01f);

	vector<Vec2> perlinCoords;
	for(int i = 0; i < 24; ++i)
	{
		perlinCoords.push_back(Vec2());
	}

	for(int i = 0; i < 24; i += 4)
	{
		vertexes[i + 3].m_perlinCoords = Vec2(perlinTextCoordsX.x, perlinTextCoordsY.x);
		vertexes[i + 0].m_perlinCoords = Vec2(perlinTextCoordsX.x, perlinTextCoordsY.y);
		vertexes[i + 1].m_perlinCoords = Vec2(perlinTextCoordsX.y, perlinTextCoordsY.y);
		vertexes[i + 2].m_perlinCoords = Vec2(perlinTextCoordsX.y, perlinTextCoordsY.x);
	}

	
	GLWModernVertexArray( vertexes, translation, GL_WRAPPER_QUADS, m_materials[ materialID ] );
	//GLWrapperEnableVertexAttribArrayTextCoords( m_materials[materialID]->m_shader->m_perlinTexCoords, perlinCoords );
	disableShaderProgram();
}

//----------------------------------------------------------------------------------
void Renderer::initiateStack()
{
	m_matrixStack.push_back(Matrix_4x4()); //Pushes back the identity matrix
}

//----------------------------------------------------------------------------------
void Renderer::clearStack()
{
	vector<Matrix_4x4> newStack;
	m_matrixStack = newStack;
	m_matrixStack.push_back(Matrix_4x4());
}

//----------------------------------------------------------------------------------
int	Renderer::stackSize()
{
	return (int) m_matrixStack.size();
}

//----------------------------------------------------------------------------------
void Renderer::stackPush( Matrix_4x4& toPushBack )
{
	Matrix_4x4 newMatrix = m_matrixStack.back().transformByMatrix( toPushBack );
	m_matrixStack.push_back( newMatrix );
}

//----------------------------------------------------------------------------------
void Renderer::stackPop()
{
	m_matrixStack.pop_back();
}

//----------------------------------------------------------------------------------
void Renderer::translateMatrix(float x, float y, float z)
{
	Matrix_4x4 translationMatrix;
	translationMatrix.setAsATranslationMatrix(x, y, z);
	stackPush( translationMatrix );
}

//----------------------------------------------------------------------------------
void Renderer::scaleMatrix(float x, float y, float z)
{
	Matrix_4x4 scaleMatrix;
	scaleMatrix.setAsAScaleMatrix(x, y, z);
	stackPush( scaleMatrix );
}

//----------------------------------------------------------------------------------
void Renderer::loadIdentity()
{
	Matrix_4x4 identityMatrix;
	m_matrixStack.push_back(identityMatrix);
}

//----------------------------------------------------------------------------------
void Renderer::rotationMatrix(float angle, float x, float y, float z)
{
	//https://www.talisman.org/opengl-1.1/Reference/glRotate.html
	//xx (1 - c) + c		xy (1 - c)  - zs	xz (1 - c) + ys		0
	//yx (1 - c) + zs		yy (1 - c) + c		yz	(1 - c)  - xs		0
	//zx (1 - c)  - ys		zy (1 - c) + xs		zz	(1 - c) + c			0
	//0						0					0					1

	float c = cos(angle * DEG_TO_RAD);
	float s = sin(angle * DEG_TO_RAD);
	Matrix_4x4 rotMat;
	rotMat.m_matrix[0] = x*x * (1.f - c) + c;
	rotMat.m_matrix[1] = y*x * (1.f - c) + z*s;
	rotMat.m_matrix[2] = z*x * (1.f - c) - y*s;
	rotMat.m_matrix[3] = 0.f;
	rotMat.m_matrix[4] = x*y * (1.f - c) - z*s;
	rotMat.m_matrix[5] = y*y * (1.f - c) + c;
	rotMat.m_matrix[6] = z*y * (1.f - c) + x*s;
	rotMat.m_matrix[7] = 0.f;
	rotMat.m_matrix[8] = x*z * (1.f -c) + y*s;
	rotMat.m_matrix[9] = y*z * (1.f - c) - x*s;
	rotMat.m_matrix[10] = z*z * (1.f - c) + c;
	rotMat.m_matrix[11] = 0.f;
	rotMat.m_matrix[12] = 0.f;
	rotMat.m_matrix[13] = 0.f;
	rotMat.m_matrix[14] = 0.f;
	rotMat.m_matrix[15] = 1.f;
	stackPush( rotMat );
}

//----------------------------------------------------------------------------------
Matrix_4x4 Renderer::getBackOfMatrix()
{
	return m_matrixStack.back();
}

//From http://www.songho.ca/opengl/gl_matrix.html
//----------------------------------------------------------------------------------
Matrix_4x4 Renderer::setFrustum(float l, float r, float b, float t, float n, float f)
{
	Matrix_4x4 mat;
	mat.m_matrix[0]  = 2 * n / (r - l);
	mat.m_matrix[5]  = 2 * n / (t - b);
	mat.m_matrix[8]  = (r + l) / (r - l);
	mat.m_matrix[9]  = (t + b) / (t - b);
	mat.m_matrix[10] = -(f + n) / (f - n);
	mat.m_matrix[11] = -1;
	mat.m_matrix[14] = -(2 * f * n) / (f - n);
	mat.m_matrix[15] = 0;
	return mat;
}

//From http://www.songho.ca/opengl/gl_matrix.html
//----------------------------------------------------------------------------------
Matrix_4x4 Renderer::setPerspectiveFrustum(float fovY, float aspect, float front, float back)
{
	clearStack();
	//loadIdentity();
	float tangent = tanf(fovY/2 * DEG_TO_RAD);	// tangent of half fovY
	float height = front * tangent;				// half height of near plane
	float width = height * aspect;				// half width of near plane

	Matrix_4x4 temp =  setFrustum(-width, width, -height, height, front, back);

	stackPush( temp );
	// params: left, right, bottom, top, near, far
	return temp;
}

//From http://www.songho.ca/opengl/gl_matrix.html
//----------------------------------------------------------------------------------
Matrix_4x4 Renderer::setOrthoFrustum(float l, float r, float b, float t, float n, float f)
{
	clearStack();
	//loadIdentity();
	Matrix_4x4 mat;
	mat.m_matrix[0]  = 2 / (r - l);
	mat.m_matrix[5]  = 2 / (t - b);
	mat.m_matrix[10] = -2 / (f - n);
	mat.m_matrix[12] = -(r + l) / (r - l);
	mat.m_matrix[13] = -(t + b) / (t - b);
	mat.m_matrix[14] = -(f + n) / (f - n);

	stackPush( mat );
	return mat;
}

//----------------------------------------------------------------------------------
void Renderer::renderFBOOnScreen(  int FBOIndex, const Vec3& minPos, const Vec3& maxPos, Vec3& translation  )
{

	Vec2 minCoords = Vec2(0.f, 1.f);
	Vec2 maxCoords = Vec2(1.f, 0.f);

	vector<Vertex3D_PCT> vertexes;

	for(int i = 0; i < 4; ++i)
	{
		vertexes.push_back(Vertex3D_PCT());
	}

	for(int i = 0; i < 4; ++i)
	{
		vertexes[i].m_color = RGBAColors(100, 100, 100, 255);
	}

	for(int i = 0; i < 4; i += 4)
	{
		vertexes[i + 3].m_texCoords = Vec2(minCoords.x, minCoords.y);
		vertexes[i + 0].m_texCoords = Vec2(minCoords.x, maxCoords.y);
		vertexes[i + 1].m_texCoords = Vec2(maxCoords.x, maxCoords.y);
		vertexes[i + 2].m_texCoords = Vec2(maxCoords.x, minCoords.y);
	}
	vertexes[0].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[0].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[0].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[1].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[1].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[1].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[2].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[2].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[2].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[3].m_tangent = Vec3(1.f, 0.f, 0.f);
	vertexes[3].m_biTangent = Vec3(0.f, 1.f, 0.f);
	vertexes[3].m_normal = Vec3(0.f, 0.f, 1.f);

	vertexes[0].m_position.x = minPos.x;
	vertexes[0].m_position.y = minPos.y;
	vertexes[0].m_position.z = maxPos.z;

	vertexes[1].m_position.x = maxPos.x;
	vertexes[1].m_position.y = minPos.y;
	vertexes[1].m_position.z = maxPos.z;

	vertexes[2].m_position.x = maxPos.x;
	vertexes[2].m_position.y = maxPos.y;
	vertexes[2].m_position.z = maxPos.z;

	vertexes[3].m_position.x = minPos.x;
	vertexes[3].m_position.y = maxPos.y;
	vertexes[3].m_position.z = maxPos.z;


	m_FBOs[FBOIndex]->m_material->useProgram();
	m_FBOs[FBOIndex]->m_material->setShaderUniforms();

	GLWrapperFBOVertexArray( vertexes, translation, GL_WRAPPER_QUADS, m_FBOs[FBOIndex] );
	disableShaderProgram();
}

//TODO: gather all sprites to draw then sort by spriteSheetID
//----------------------------------------------------------------------------------
void Renderer::render2DSprite(int spriteSheetID, IntVec2& spriteCoords, const Vec2& translation, const Vec2& dimensionsOfEntity, const RGBAColors& color)
{
	vector<Vertex3D_PCT> vertexes;
	float currentX = translation.x;
	int textureID = 0;
	
	{
		SpriteSheet* refToSprites = m_spriteSheets[spriteSheetID];
		textureID = refToSprites->m_texture->m_openglTextureID;

		Vec2 min = refToSprites->getMinTextureCoordsForTile(spriteCoords);
		Vec2 max = refToSprites->getMaxTextureCoordsForTile(spriteCoords);

		vertexes.push_back(Vertex3D_PCT());
		vertexes.push_back(Vertex3D_PCT());
		vertexes.push_back(Vertex3D_PCT());
		vertexes.push_back(Vertex3D_PCT());

		int index = vertexes.size() - 4;

		vertexes[index].m_texCoords = min;
		vertexes[index].m_position = Vec3(0.0f, dimensionsOfEntity.y, 0.0f);
		++index;

		vertexes[index].m_texCoords = Vec2(min.x, max.y);
		vertexes[index].m_position = Vec3(0.0f, 0.0f, 0.0f);
		++index;

		vertexes[index].m_texCoords = max;
		vertexes[index].m_position = Vec3(dimensionsOfEntity.x, 0.0f, 0.0f);
		++index;

		vertexes[index].m_texCoords = Vec2(max.x, min.y);
		vertexes[index].m_position = Vec3(dimensionsOfEntity.x, dimensionsOfEntity.y, 0.0f);
		++index;
	}

	for (int i = 0; i < (int)vertexes.size(); ++i)
	{
		vertexes[i].m_color = color;
	}

	Vec3 translation3D(translation.x, translation.y, 0.0f);
	GLWrapperVertexArray(vertexes, translation3D, GL_WRAPPER_QUADS, textureID);
}

//----------------------------------------------------------------------------------
int Renderer::getSpriteSheetIDFromFileName(std::string& fileName, int spritesWide, int spritesHigh)
{
	for (int sheetIndex = 0; sheetIndex < m_spriteSheets.size(); ++sheetIndex)
	{
		if (fileName == m_spriteSheets[sheetIndex]->m_imageFileName)
		{
			return sheetIndex;
		}
	}

	m_spriteSheets.push_back(new SpriteSheet(fileName, spritesWide, spritesHigh));
	return m_spriteSheets.size() - 1;
}

//----------------------------------------------------------------------------------
int Renderer::getSpriteSheetIDFromFileName(std::string& fileName)
{
	int x;
	int y;
	std::vector<std::string> separatedExtensions = UtilityFunctions::Split(fileName, '.');
	std::vector<std::string> coords = UtilityFunctions::Split(separatedExtensions[1], 'x');

	return getSpriteSheetIDFromFileName(fileName, std::stoi(coords[0]), std::stoi(coords[1]));
}