#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Core\RGBAColors.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Renderer\SpriteSheet.hpp"
#include <cmath>
#include "Engine\Core\Texture.hpp"
#include "Engine\Renderer\Shader.hpp"
#include "Engine\Math\Matrix_4x4.hpp"
//#include "TheGame.hpp"
#include "Engine\Renderer\Renderer.hpp"
#include "Engine\Renderer\Material.hpp"
#include "Engine\Renderer\ShaderProgram.hpp"
#include "Engine\Renderer\FBO.hpp"
#include "Engine\Renderer\MeshVertexData.hpp"
#include "Engine\Renderer\Mesh.hpp"

#define STBI_HEADER_FILE_ONLY
#include "Engine\Core\stb_image.c"

#include "DrawDebug.hpp"
#include "Engine/Files/FileSystem.hpp"

//#include "Engine2013/MemoryManager/NewAndDelete.hpp"

// Globals "function pointer" variables
PFNGLGENBUFFERSPROC				glGenBuffers			= nullptr;
PFNGLBINDBUFFERPROC				glBindBuffer			= nullptr;
PFNGLBUFFERDATAPROC				glBufferData			= nullptr;
PFNGLGENERATEMIPMAPPROC			glGenerateMipmap		= nullptr;
PFNGLDELETEBUFFERSPROC			glDeleteBuffers			= nullptr;

//For shaders
PFNGLCREATESHADERPROC			glCreateShader			= nullptr;
PFNGLSHADERSOURCEPROC			glShaderSource			= nullptr;
PFNGLCOMPILESHADERPROC			glCompileShader			= nullptr;
PFNGLGETSHADERIVPROC			glGetShaderiv			= nullptr;
PFNGLCREATEPROGRAMPROC			glCreateProgram			= nullptr;
PFNGLATTACHSHADERPROC			glAttachShader			= nullptr;
PFNGLLINKPROGRAMPROC			glLinkProgram			= nullptr;
PFNGLGETPROGRAMIVPROC			glGetProgramiv			= nullptr;
PFNGLUSEPROGRAMPROC				glUseProgram			= nullptr;
PFNGLACTIVETEXTUREPROC			glActiveTexture			= nullptr;
PFNGLGETUNIFORMLOCATIONPROC		glGetUniformLocation	= nullptr;	
PFNGLUNIFORM1FPROC				glUniform1f				= nullptr;
PFNGLUNIFORM3FPROC				glUniform3f				= nullptr;
PFNGLUNIFORM4FPROC				glUniform4f				= nullptr;
PFNGLUNIFORM1FVPROC				glUniform1fv			= nullptr;
PFNGLUNIFORM3FVPROC				glUniform3fv			= nullptr;
PFNGLUNIFORM4FVPROC				glUniform4fv			= nullptr;
PFNGLUNIFORM1IPROC				glUniform1i				= nullptr;
PFNGLGETSHADERINFOLOGPROC		glGetShaderInfoLog		= nullptr;
PFNGLGETPROGRAMINFOLOGPROC		glGetProgramInfoLog		= nullptr;

PFNGLVERTEXATTRIBPOINTERPROC	glVertexAttribPointer	= nullptr;
PFNGLBINDATTRIBLOCATIONPROC		glBindAttribLocation	= nullptr;
PFNGLGETATTRIBLOCATIONPROC		glGetAttribLocation		= nullptr;
PFNGLENABLEVERTEXATTRIBARRAYPROC	glEnableVertexAttribArray	= nullptr;
PFNGLDISABLEVERTEXATTRIBARRAYPROC	glDisableVertexAttribArray	= nullptr;
PFNGLUNIFORMMATRIX4FVPROC			glUniformMatrix4fv			= nullptr;
PFNGLUSEPROGRAMOBJECTARBPROC		glUseProgramObjectARB		= nullptr;

PFNGLGENFRAMEBUFFERSPROC			glGenFramebuffers			= nullptr;
PFNGLBINDFRAMEBUFFERPROC			glBindFramebuffer			= nullptr;
PFNGLFRAMEBUFFERTEXTURE2DPROC		glFramebufferTexture2D		= nullptr;

struct vec4f
{
	float r;
	float g;
	float b;
	float a;
};

// Call some function like this during startup, after creating window & OpenGL rendering context
void InitializeAdvancedOpenGLFunctions()
{
	glGenBuffers				= (PFNGLGENBUFFERSPROC)					wglGetProcAddress( "glGenBuffers" );
	glBindBuffer				= (PFNGLBINDBUFFERPROC)					wglGetProcAddress( "glBindBuffer" );
	glBufferData				= (PFNGLBUFFERDATAPROC)					wglGetProcAddress( "glBufferData" );
	glGenerateMipmap			= (PFNGLGENERATEMIPMAPPROC)				wglGetProcAddress( "glGenerateMipmap" );
	glDeleteBuffers				= (PFNGLDELETEBUFFERSPROC)				wglGetProcAddress( "glDeleteBuffers" );

	glCreateShader				= (PFNGLCREATESHADERPROC	)			wglGetProcAddress( "glCreateShader" );
	glShaderSource				= (PFNGLSHADERSOURCEPROC	)			wglGetProcAddress( "glShaderSource" );
	glCompileShader				= (PFNGLCOMPILESHADERPROC	)			wglGetProcAddress( "glCompileShader" );
	glGetShaderiv				= (PFNGLGETSHADERIVPROC		)			wglGetProcAddress( "glGetShaderiv" );
	glCreateProgram				= (PFNGLCREATEPROGRAMPROC	)			wglGetProcAddress( "glCreateProgram" );
	glAttachShader				= (PFNGLATTACHSHADERPROC	)			wglGetProcAddress( "glAttachShader" );
	glLinkProgram				= (PFNGLLINKPROGRAMPROC		)			wglGetProcAddress( "glLinkProgram" );
	glGetProgramiv				= (PFNGLGETPROGRAMIVPROC	)			wglGetProcAddress( "glGetProgramiv" );
	glUseProgram				= (PFNGLUSEPROGRAMPROC		)			wglGetProcAddress( "glUseProgram" );	
	glActiveTexture				= (PFNGLACTIVETEXTUREPROC	)			wglGetProcAddress( "glActiveTexture");
	glGetUniformLocation		= (PFNGLGETUNIFORMLOCATIONPROC )		wglGetProcAddress( "glGetUniformLocation");
	glUniform1f					= (PFNGLUNIFORM1FPROC		)			wglGetProcAddress( "glUniform1f" );
	glUniform3f					= (PFNGLUNIFORM3FPROC)					wglGetProcAddress( "glUniform3f" );
	glUniform4f					= (PFNGLUNIFORM4FPROC)					wglGetProcAddress( "glUniform4f" );
	glUniform1fv				= (PFNGLUNIFORM1FVPROC		)			wglGetProcAddress( "glUniform1fv" );
	glUniform3fv				= (PFNGLUNIFORM3FVPROC)					wglGetProcAddress( "glUniform3fv" );
	glUniform4fv				= (PFNGLUNIFORM4FVPROC)					wglGetProcAddress( "glUniform4fv" );
	glUniform1i					= (PFNGLUNIFORM1IPROC		)			wglGetProcAddress( "glUniform1i" );
	glGetShaderInfoLog			= (PFNGLGETSHADERINFOLOGPROC	)		wglGetProcAddress( "glGetShaderInfoLog" );
	glGetProgramInfoLog			= (PFNGLGETPROGRAMINFOLOGPROC	)		wglGetProcAddress( "glGetProgramInfoLog" );

	glVertexAttribPointer		= (PFNGLVERTEXATTRIBPOINTERPROC)		wglGetProcAddress( "glVertexAttribPointer" );
	glBindAttribLocation		= (PFNGLBINDATTRIBLOCATIONPROC)			wglGetProcAddress( "glBindAttribLocation" );
	glGetAttribLocation			= (PFNGLGETATTRIBLOCATIONPROC)			wglGetProcAddress( "glGetAttribLocation" );
	glEnableVertexAttribArray	= (PFNGLENABLEVERTEXATTRIBARRAYPROC)	wglGetProcAddress( "glEnableVertexAttribArray" );
	glDisableVertexAttribArray	= (PFNGLDISABLEVERTEXATTRIBARRAYPROC)	wglGetProcAddress( "glDisableVertexAttribArray" );
	glUniformMatrix4fv			= (PFNGLUNIFORMMATRIX4FVPROC)			wglGetProcAddress( "glUniformMatrix4fv" );
	glUseProgramObjectARB		= (PFNGLUSEPROGRAMOBJECTARBPROC)		wglGetProcAddress( "glUseProgramObjectARB" );

	glGenFramebuffers			= (PFNGLGENFRAMEBUFFERSPROC)			wglGetProcAddress( "glGenFramebuffers" );
	glBindFramebuffer			= (PFNGLBINDFRAMEBUFFERPROC)			wglGetProcAddress( "glBindFramebuffer" );
	glFramebufferTexture2D		= (PFNGLFRAMEBUFFERTEXTURE2DPROC)		wglGetProcAddress( "glFramebufferTexture2D" );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform1f( int locationID, float value )
{
	glUniform1f( locationID, value );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform3f( int locationID, Vec3& value )
{
	glUniform3f( locationID, value.x, value.y, value.z );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform4f( int locationID, RGBAColors& value )
{
	float convertion = 1.f/255.f;
	float r = value.m_red * convertion;
	float g = value.m_green * convertion;
	float b = value.m_blue * convertion;
	float a = value.m_alpha * convertion;
	glUniform4f( locationID, r, g, b, a );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform1fv( int locationID, vector<float>& input)
{
	if (input.size() > 0)
	glUniform1fv( locationID, input.size(), &input[0] );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform3fv( int locationID, vector<Vec3>& input)
{
	if (input.size() > 0)
	glUniform3fv( locationID, input.size(), &input[0].x );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform4fv( int locationID, vector<RGBAColors>& input)
{
	float convertion = 1.f/255.f;
	vector<vec4f> values;
	for( int i = 0; i < (int) input.size(); ++i )
	{
		values.push_back(vec4f());
		values[i].r = (float) input[i].m_red * convertion;
		values[i].g = (float) input[i].m_green * convertion;
		values[i].b = (float) input[i].m_blue * convertion;
		values[i].a = (float) input[i].m_alpha * convertion;
	}

	if (input.size() > 0)
	glUniform4fv( locationID, input.size(), &values[0].r );
}

//----------------------------------------------------------------------------------
void GLWrapperUniform1i( int locationID, int value )
{
	glUniform1i( locationID, value );
}

//----------------------------------------------------------------------------------
int GLWrapperGetShaderUniformLocation( int programShaderID, string& name )
{
	return glGetUniformLocation( programShaderID, (const GLchar*) name.c_str() );
}

//Code is from http://stackoverflow.com/questions/5988686/creating-a-3d-sphere-in-opengl-using-visual-c
//by datenwolf
//----------------------------------------------------------------------------------
void GLWrapperDrawTexturedSphere(float radius, unsigned int rings, unsigned int sectors, Vec3 position, int textureID)
{
	vector<GLfloat> vertices;
	vector<GLfloat> normals;
	vector<GLfloat> texcoords;
	vector<GLushort> indices;

	const double M_PI = 3.141592653589793238462643383279502884197;
	float const R = 1.f/(float)(rings-1);
	float const S = 1.f/(float)(sectors-1);
	int r, s;

	vertices.resize(rings * sectors * 3);
	normals.resize(rings * sectors * 3);
	texcoords.resize(rings * sectors * 2);
	std::vector<GLfloat>::iterator v = vertices.begin();
	std::vector<GLfloat>::iterator n = normals.begin();
	std::vector<GLfloat>::iterator t = texcoords.begin();
	for(r = 0; r < (int) rings; r++) for(s = 0; s < (int) sectors; s++) {
		float const y = (float const) sin( -M_PI * .5 + M_PI * r * R );
		float const x = (float const) (cos(2*M_PI * s * S) * sin( M_PI * r * R ));
		float const z = (float const) (sin(2*M_PI * s * S) * sin( M_PI * r * R ));

		*t++ = s*S;
		*t++ = r*R;

		*v++ = x * radius;
		*v++ = y * radius;
		*v++ = z * radius;

		*n++ = x;
		*n++ = y;
		*n++ = z;
	}

	indices.resize(rings * sectors * 4);
	std::vector<GLushort>::iterator i = indices.begin();
	for(r = 0; r < (int) rings-1; r++) for(s = 0; s < (int) sectors-1; s++) {
		*i++ = (unsigned short)(r * sectors + s);
		*i++ = (unsigned short)(r * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + s);
	}

	glBindTexture(GL_TEXTURE_2D, textureID);
	glColor4f(1.f, 1.f, 1.f, 1.f);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_NORMAL_ARRAY);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glVertexPointer(3, GL_FLOAT, 0, &vertices[0]);
	glNormalPointer(GL_FLOAT, 0, &normals[0]);
	glTexCoordPointer(2, GL_FLOAT, 0, &texcoords[0]);
	glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);
	glPopMatrix();

	glBindTexture(GL_TEXTURE_2D, 0);
}


//----------------------------------------------------------------------------------
void GLWrapperUseProgram( int programID )
{
	glUseProgram( (GLuint) programID );
}

//----------------------------------------------------------------------------------
void GLWrapperLinkProgram( int programID )
{
	glLinkProgram( (GLuint) programID );
}

//----------------------------------------------------------------------------------
void GLWrapperAttachShader( int programID, int shaderID )
{
	glAttachShader( (GLuint) programID, (GLuint) shaderID );
}

//----------------------------------------------------------------------------------
int GLWrapperGenerateShaderProgram()
{
	return glCreateProgram();
}

//----------------------------------------------------------------------------------
void GLWrapperCheckForShaderProgramLinkErrors(int& programID,  Shader* fragment, Shader* vertex )
{
	GLint wasSuccessful = 1;
	glGetProgramiv( programID, GL_LINK_STATUS, &wasSuccessful );

	if(wasSuccessful == GL_FALSE)
	{
		GLint length = 0;

		glGetProgramiv( programID, GL_INFO_LOG_LENGTH, &length);
		char* log = new char[length];
		GLint lengthOfReturnedLog = 0;
		glGetProgramInfoLog( programID, length, &lengthOfReturnedLog, log);

		string filePathAndLog = "";
		string tempLog = log;

		size_t found = tempLog.find_first_of("vertex");

		bool isVertex = false;
		if(found!=std::string::npos)
		{
			filePathAndLog += vertex->m_filePath;
			isVertex = true;
		}
		else
		{
			filePathAndLog += fragment->m_filePath;
		}

		found = tempLog.find_first_of("(");
		if(found > 0)
			tempLog.erase(0 , found);

		filePathAndLog += "  ";
		filePathAndLog += tempLog;

		filePathAndLog += GLWrapperGetOpenGLAndGLSLVersion();
		filePathAndLog += "\n";

		OutputDebugStringA( filePathAndLog.c_str() );

		string caption = "Shader Linker Error";

		string message = "GLSL Shader Linker Error \n\n";
		string error = log;
		found = error.find_first_of("error");
		error.erase(0, found);
		error[0] = 'E'; error[1] = 'R'; error[2] = 'R'; error[3] = 'O'; error[4] = 'R';
		message += error;
		message += "\n";
		message += GLWrapperGetOpenGLAndGLSLVersion();
		message += "\n\n";

		message += "File Location:\n";
		message += "Fragment Shader: ";
		message += fragment->m_filePath;
		message += "\nVertex Shader: ";
		message += vertex->m_filePath;
		message += "\n\n";

		message += "RAW ERROR LOG:\n";
		message += log;
		message += "\n\n";

		message += "The application will now close";

		WindowsWrapperMessageBox(message, caption);

		exit(0);
	}
}

//----------------------------------------------------------------------------------
std::string GLWrapperGetOpenGLAndGLSLVersion()
{
	string openGLinfo = "OpenGL Version: ";
	openGLinfo += (const char*) glGetString(GL_VERSION);
	openGLinfo += "\n";
	openGLinfo += "GLSL Version: ";
	openGLinfo += (const char*) glGetString(GL_SHADING_LANGUAGE_VERSION);

	return openGLinfo;
}


//----------------------------------------------------------------------------------
void GLWrapperGenerateShader( unsigned int& generatedShaderID, const std::string& shaderFilePath, GL_TYPES shaderType, unsigned char* shaderBuffer, unsigned int* numOfBytes )
{
	std::string temp((char*) shaderBuffer);
	generatedShaderID = glCreateShader( (GLuint) shaderType ); // GL_VERTEX_SHADER or GL_FRAGMENT_SHADER

	unsigned char** buffer = new unsigned char*;
	buffer[0] = shaderBuffer;

	glShaderSource( (GLuint) generatedShaderID, 1, (const GLchar* const*) buffer, /*nullptr*/(const GLint*) numOfBytes);
	glCompileShader( (GLuint) generatedShaderID );

	int wasSuccessful = 0;
	glGetShaderiv( generatedShaderID, GL_COMPILE_STATUS, &wasSuccessful );

	if(wasSuccessful == GL_FALSE)
	{
		GLint length = 0;

		glGetShaderiv( generatedShaderID, GL_INFO_LOG_LENGTH, &length);
		char* log = new char[length];
		GLint lengthOfReturnedLog = 0;
		glGetShaderInfoLog( generatedShaderID, length, &lengthOfReturnedLog, log);

		string filePathAndLog = "";
		string tempLog = log;

		size_t found = tempLog.find_first_of("(");

		if(found > 0)
		tempLog.erase(0 , found);

		filePathAndLog += shaderFilePath;
		filePathAndLog += " ";
		filePathAndLog += tempLog;

		filePathAndLog += GLWrapperGetOpenGLAndGLSLVersion();
		filePathAndLog += "\n";

		OutputDebugStringA( filePathAndLog.c_str() );

		string caption = "Shader Compile Error";

		string message = "GLSL Shader Compile Error \n\n";
		string error = log;
		found = error.find_first_of("error");
		error.erase(0, found);
		if(error.size() > 0)
		{error[0] = 'E'; error[1] = 'R'; error[2] = 'R'; error[3] = 'O'; error[4] = 'R';}
		message += error;
		message += "\n";
		message += GLWrapperGetOpenGLAndGLSLVersion();
		message += "\n\n";

		message += "File Location:\n";
		message += shaderFilePath;
		message += "\n\n";

		message += "RAW ERROR LOG:\n";
		message += log;
		message += "\n\n";

		message += "The application will now close";
		
		WindowsWrapperMessageBox(message, caption);
		exit(0);
	}

	//delete[] shaderBuffer;
}

//----------------------------------------------------------------------------------
void GLWrapperGenerateTexture(unsigned int& generatedOutputedID, int& sizeX, int& sizeY, const std::string& imageFilePath )
{
	int numComponents = 0; // Filled in for us to indicate how many color/alpha components the image had (e.g. 3=RGB, 4=RGBA)
	int numComponentsRequested = 0; // don't care; we support 3 (RGB) or 4 (RGBA)

	if (!g_theFileSystem)
	{
		g_theFileSystem = new FileSystem(nullptr);
	}

	FileBuffer toLoad;
	//Don't store in memory since the texture class already stores it
	g_theFileSystem->loadFile(imageFilePath, toLoad, false);
	unsigned char* imageData = stbi_load_from_memory(toLoad.buffer, toLoad.size, &sizeX, &sizeY, &numComponents, numComponentsRequested);//stbi_load( imageFilePath.c_str(), &sizeX, &sizeY, &numComponents, numComponentsRequested );

	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1,  &generatedOutputedID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, generatedOutputedID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	GLenum bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	GLenum internalFormat = bufferFormat; // the format we want the texture to be on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0 = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0 or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer


	stbi_image_free(imageData);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 5);
	glHint( GL_GENERATE_MIPMAP_HINT , GL_NICEST);
	glGenerateMipmap(GL_TEXTURE_2D);
}

void GLWrapperGenerateTexture(unsigned int& generatedOutputedID, int& sizeX, int& sizeY, unsigned char* imageData)
{
	int numComponents = 0; // Filled in for us to indicate how many color/alpha components the image had (e.g. 3=RGB, 4=RGBA)
	//unsigned char* imageData = stbi_load( imageFilePath.c_str(), &sizeX, &sizeY, &numComponents, numComponentsRequested );

	// Enable texturing
	glEnable( GL_TEXTURE_2D );

	// Tell OpenGL that our pixel data is single-byte aligned
	glPixelStorei( GL_UNPACK_ALIGNMENT, 1 );

	// Ask OpenGL for an unused texName (ID number) to use for this texture
	glGenTextures( 1,  &generatedOutputedID );

	// Tell OpenGL to bind (set) this as the currently active texture
	glBindTexture( GL_TEXTURE_2D, generatedOutputedID );

	// Set texture clamp vs. wrap (repeat)
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ); // one of: GL_CLAMP_TO_EDGE, GL_REPEAT, GL_MIRRORED_REPEAT, GL_MIRROR_CLAMP_TO_EDGE, ...

	// Set magnification (texel > pixel) and minification (texel < pixel) filters
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); // one of: GL_NEAREST, GL_LINEAR
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR); // one of: GL_NEAREST, GL_LINEAR, GL_NEAREST_MIPMAP_NEAREST, GL_NEAREST_MIPMAP_LINEAR, GL_LINEAR_MIPMAP_NEAREST, GL_LINEAR_MIPMAP_LINEAR

	GLenum bufferFormat = GL_RGBA; // the format our source pixel data is currently in; any of: GL_RGB, GL_RGBA, GL_LUMINANCE, GL_LUMINANCE_ALPHA, ...
	if( numComponents == 3 )
		bufferFormat = GL_RGB;

	// Todo: What happens if numComponents is neither 3 nor 4?

	GLenum internalFormat = bufferFormat; // the format we want the texture to be on the card; allows us to translate into a different texture format as we upload to OpenGL

	glTexImage2D(			// Upload this pixel data to our new OpenGL texture
		GL_TEXTURE_2D,		// Creating this as a 2d texture
		0,					// Which mipmap level to use as the "root" (0 = the highest-quality, full-res image), if mipmaps are enabled
		internalFormat,		// Type of texel format we want OpenGL to use for this texture internally on the video card
		sizeX,//m_size.x,			// Texel-width of image; for maximum compatibility, use 2^N + 2^B, where N is some integer in the range [3,10], and B is the border thickness [0,1]
		sizeY,//m_size.y,			// Texel-height of image; for maximum compatibility, use 2^M + 2^B, where M is some integer in the range [3,10], and B is the border thickness [0,1]
		0,					// Border size, in texels (must be 0 or 1)
		bufferFormat,		// Pixel format describing the composition of the pixel data in buffer
		GL_UNSIGNED_BYTE,	// Pixel color components are unsigned bytes (one byte per color/alpha channel)
		imageData );		// Location of the actual pixel data bytes/buffer

	//stbi_image_free( imageData );

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 5);
	glHint( GL_GENERATE_MIPMAP_HINT , GL_NICEST);
	glGenerateMipmap(GL_TEXTURE_2D);
}


//----------------------------------------------------------------------------------
GLWrapper::GLWrapper(void)
{
}


//----------------------------------------------------------------------------------
GLWrapper::~GLWrapper(void)
{
}


//----------------------------------------------------------------------------------
void GLWrapperGenerateVBO(unsigned int& vboID, vector<Vertex3D_PCT>& vertexes)
{
	if( vboID == 0 )
	{
		glGenBuffers( 1, &vboID );
	}

	glBindBuffer( GL_ARRAY_BUFFER, vboID );
	glBufferData( GL_ARRAY_BUFFER, sizeof( Vertex3D_PCT ) * vertexes.size(), vertexes.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}


//----------------------------------------------------------------------------------
void GLWrapperRenderModernVBO(unsigned int& vboID, vector<Vertex3D_PCT>& vertexes, Vec3& translateXYZ)
{
	glPushMatrix();
	glTranslatef( translateXYZ.x, translateXYZ.y, translateXYZ.z );
	glBindBuffer( GL_ARRAY_BUFFER, vboID );

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	glVertexPointer(	3, GL_FLOAT, sizeof( Vertex3D_PCT ), (const GLvoid*) offsetof( Vertex3D_PCT, m_position ) );	
	glColorPointer(		4, GL_UNSIGNED_BYTE, sizeof( Vertex3D_PCT ), (const GLvoid*) offsetof( Vertex3D_PCT, m_color ) );	
	glTexCoordPointer(	2, GL_FLOAT, sizeof( Vertex3D_PCT ), (const GLvoid*) offsetof( Vertex3D_PCT, m_texCoords ) );	

	glDrawArrays( GL_QUADS, 0, vertexes.size() );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glPopMatrix();

	glEnd();
}

//----------------------------------------------------------------------------------
void GLWrapperRenderModernVBO(unsigned int& vboID, int numberOfVertexes, Vec3& translation, Material* material, GL_TYPES typeOfDrawing )
{
	material->useProgram();
	material->setShaderUniforms();

	glBindBuffer( GL_ARRAY_BUFFER, vboID );

	//if(vertexes.empty())
	//	return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 rotateToWorldMatrix = Matrix_4x4();
	rotateToWorldMatrix.setAsARotationMatrix(0.f, 0.f, 0.f, 0.0f);
	g_theRenderer->rotationMatrix(0.0f, 0.0f, 0.0f, 0.0f);

	rotateToWorldMatrix.transformByMatrix(objectToWorldMatrix);
	objectToWorldMatrix.transformByMatrix( rotateToWorldMatrix );

	for(int i = 0; i < (int) material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( material->m_textureMaps[i]->m_openglTextureID, i );
	}

	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_pos ) );

	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_color ) );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_texCoords ) );


	glEnableVertexAttribArray( material->m_shader->m_tangentID );
	glVertexAttribPointer(	material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_tangent ) );

	glEnableVertexAttribArray( material->m_shader->m_biTangentID );
	glVertexAttribPointer(	material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_bitangent ) );
	glEnableVertexAttribArray( material->m_shader->m_normalID );
	glVertexAttribPointer(	material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_normal ) );

	glUniformMatrix4fv( material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );


	glDrawArrays( typeOfDrawing, 0, numberOfVertexes );

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );
	glDisableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );

	glDisableVertexAttribArray( material->m_shader->m_tangentID );
	glDisableVertexAttribArray( material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( material->m_shader->m_normalID );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );

	g_theRenderer->stackPop();
	g_theRenderer->stackPop();

	GLWrapperDisableShader();
}


//----------------------------------------------------------------------------------
void GLWrapperDeleteVBOBuffers(int size, unsigned int& vboID)
{
	glDeleteBuffers(size, &vboID);
}


//----------------------------------------------------------------------------------
void GLWrapperDrawVectorOfPoints(const vector<Vec3>& points, const vector<RGBAColors>& color, const Vec3& translation, int sizeOfPoint)
{
	glEnable(GL_PROGRAM_POINT_SIZE);
	glPointSize( (GLfloat) sizeOfPoint);

	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBegin(GL_POINTS);
	{
		for( int i = 0; i < (int) points.size(); ++i)
		{
			glColor4f(color[i].m_red * g_charToFloat, color[i].m_green * g_charToFloat, color[i].m_blue * g_charToFloat, color[i].m_alpha * g_charToFloat);
			glVertex3f(points[i].x, points[i].y, points[i].z);
		}
	}
	glEnd();

	glPopMatrix();

	glColor4f(1.0f, 1.0f, 1.0f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}


//----------------------------------------------------------------------------------
void GLWrapperColorBytes(const RGBAColors& color)
{
	glColor4f(color.m_red * g_charToFloat, color.m_green * g_charToFloat, color.m_blue * g_charToFloat, color.m_alpha * g_charToFloat);
}


//----------------------------------------------------------------------------------
void GLWrapperColorFloat( const Vec3& color )
{
	glColor4f(color.x, color.y, color.z, 1.f);
}

//----------------------------------------------------------------------------------
void GLWrapperEnableGLType(GL_TYPES toEnable)
{
	glEnable(toEnable);
}


//----------------------------------------------------------------------------------
void GLWrapperDisableGLType(GL_TYPES toDisable)
{
	glDisable(toDisable);
}


//----------------------------------------------------------------------------------
void GLWrapperEnableGLCullFace(GL_TYPES cullFace)
{
	glCullFace(cullFace);
}


//----------------------------------------------------------------------------------
void GLWrapperBindGLTexture(GL_TYPES type, unsigned int textureID)
{
	glBindTexture(type, textureID);
}


//----------------------------------------------------------------------------------
void GLWrapperEnableGLWrapperOrtho()
{
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho( 0.0 , g_maxScreenX , 0.0 , g_maxScreenY , 0 , 1 );
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glDisable(GL_DEPTH_TEST);
}


//----------------------------------------------------------------------------------
void GLWrapperDisableGLWrapperOrtho()
{
	glEnable( GL_DEPTH_TEST);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	GLWrapperSetUpPerspectiveProjection();
	glLoadIdentity();
	glMatrixMode(GL_MODELVIEW);
	
}

//----------------------------------------------------------------------------------
void GLWrapperEnableModernOrtho()
{
	//glClearColor( 0.35f, 0.35f, 0.35f, 1.f );
	glClearDepth( 1.f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glDisable( GL_DEPTH_TEST );
	glDepthMask( GL_FALSE );

	g_theRenderer->clearStack();
	g_theRenderer->setOrthoFrustum( 0.0f, g_maxScreenX, 0.0f, g_maxScreenY, 0.0f, 1.0f );
	g_theRenderer->clearStack();
	glDisable(GL_DEPTH_TEST);
}


//----------------------------------------------------------------------------------
void GLWrapperDisableModernOrtho()
{
	//g_theRenderer->stackPop();
	g_theRenderer->clearStack();
	GLWrapperSetUpPerspectiveProjection();
	g_theRenderer->clearStack();
	glEnable( GL_DEPTH_TEST );
	glDepthMask( GL_TRUE );
}


//---------------------------------------------------------------------------
void GLWrapperSetUpPerspectiveProjection()
{
	float aspect = (16.f / 9.f); // VIRTUAL_SCREEN_WIDTH / VIRTUAL_SCREEN_HEIGHT;
	float fovX = 70.f;
	float fovY = (fovX / aspect);
	float zNear = 0.1f;
	float zFar = g_zFar;

	gluPerspective( fovY, aspect, zNear, zFar ); // Note: Absent in OpenGL ES 2.0

	g_theRenderer->setPerspectiveFrustum( fovY, aspect, zNear, zFar );
}


//----------------------------------------------------------------------------------
void GLWrapperSetUpInitialPerspectiveProjection()
{
	float aspect = (16.f / 9.f); // VIRTUAL_SCREEN_WIDTH / VIRTUAL_SCREEN_HEIGHT;
	float fovX = 90.f;
	//float fovX = 150.f;

	float fovY = (fovX / aspect);
	float zNear = 0.1f;
	float zFar = 1000.f;

	glLoadIdentity();
	gluPerspective( fovY, aspect, zNear, zFar ); // Note: Absent in OpenGL ES 2.0

	g_theRenderer->setPerspectiveFrustum( fovY, aspect, zNear, zFar );
	
	RGBAColors fogColor = RGBAColors(0, 0, 0, 1);//g_theRenderer->m_materials[g_theRenderer->m_currentMaterialID]->m_fogColorAndIntensity;
	glClearColor( fogColor.m_red, fogColor.m_green, fogColor.m_blue, 1.f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glEnable( GL_DEPTH_TEST);
	glClearDepth(1.0f);
}


//----------------------------------------------------------------------------------
void GLWrapperVertex3f(const Vec3& vertex)
{
	glVertex3f(vertex.x, vertex.y, vertex.z);
}


//----------------------------------------------------------------------------------
void GLWrapperDrawLines( vector<Vec3>& pointPairs, vector<RGBAColors>& colorPerLine, Vec3& translation )
{
	glDisable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);
	
	glBegin( GL_LINES );
	{
		for(int i = 0; i < (int) pointPairs.size(); i += 2)
		{
			glColor4f(colorPerLine[(unsigned int) (i * .5)].m_red * g_charToFloat, colorPerLine[(unsigned int) (i * .5)].m_green * g_charToFloat, colorPerLine[(unsigned int) (i * .5)].m_blue * g_charToFloat, colorPerLine[(unsigned int) (i * .5)].m_alpha * g_charToFloat);
			glVertex3f(pointPairs[i].x, pointPairs[i].y, pointPairs[i].z);
			glVertex3f(pointPairs[i + 1].x, pointPairs[i + 1].y, pointPairs[i + 1].z);
		}
	}
	glEnd();

	glColor4f(1.f, 1.f, 1.f, 1.f);
	glPopMatrix();
}


//----------------------------------------------------------------------------------
void GLWrapperDrawLines( vector<Vertex3D_PCT>& vertexes, Vec3& translation )
{
	GLWrapperVertexArray( vertexes, translation, GL_WRAPPER_DRAW_LINES );
}

//----------------------------------------------------------------------------------
void GLWrapperVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID )
{
	if(vertexes.empty())
		return;

	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	glVertexPointer(	3, GL_FLOAT, sizeof( Vertex3D_PCT ), &vertexes[0].m_position );	
	glColorPointer(		4, GL_UNSIGNED_BYTE, sizeof( Vertex3D_PCT ), &vertexes[0].m_color );	
	glTexCoordPointer(	2, GL_FLOAT, sizeof( Vertex3D_PCT ), &vertexes[0].m_texCoords );	

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

//----------------------------------------------------------------------------------
void GLWrapperVertexArray( const vector<Vertex2D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID )
{
	if(vertexes.empty())
		return;

	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	glVertexPointer(	3, GL_FLOAT, sizeof( Vertex2D_PCT ), &vertexes[0].m_position );	
	glColorPointer(		4, GL_UNSIGNED_BYTE, sizeof( Vertex2D_PCT ), &vertexes[0].m_color );	
	glTexCoordPointer(	2, GL_FLOAT, sizeof( Vertex2D_PCT ), &vertexes[0].m_texCoords );	

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();
}

//----------------------------------------------------------------------------------
void GLWrapperVertexArray( const vector<ModelVertex>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, int textureID )
{
	if(vertexes.empty())
		return;

	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBindTexture(GL_TEXTURE_2D, textureID);

	glEnableClientState( GL_VERTEX_ARRAY );
	glEnableClientState( GL_COLOR_ARRAY );
	glEnableClientState( GL_TEXTURE_COORD_ARRAY );

	glVertexPointer(	3, GL_FLOAT, sizeof( ModelVertex ), &vertexes[0].m_pos );	
	glColorPointer(		4, GL_UNSIGNED_BYTE, sizeof( ModelVertex ), &vertexes[0].m_color );	
	glTexCoordPointer(	2, GL_FLOAT, sizeof( ModelVertex ), &vertexes[0].m_texCoords );	

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableClientState( GL_VERTEX_ARRAY );
	glDisableClientState( GL_COLOR_ARRAY );
	glDisableClientState( GL_TEXTURE_COORD_ARRAY );

	glBindTexture(GL_TEXTURE_2D, 0);
	glPopMatrix();

}

//----------------------------------------------------------------------------------
void GLWrapperEnableVertexAttribArrayTextCoords( int locationID, vector<Vec2>& textCoords )
{
	glEnableVertexAttribArray( locationID );
	glVertexAttribPointer(	locationID , 2, GL_FLOAT, GL_FALSE, 0, &textCoords );
}

//----------------------------------------------------------------------------------
void GLWModernVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, Material* material )
{
	if(vertexes.empty())
		return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 rotateToWorldMatrix = Matrix_4x4();
	rotateToWorldMatrix.setAsARotationMatrix(0.f, 0.f, 0.f, 0.0f);
	g_theRenderer->rotationMatrix(0.0f, 0.0f, 0.0f, 0.0f);

	rotateToWorldMatrix.transformByMatrix(objectToWorldMatrix);
	objectToWorldMatrix.transformByMatrix( rotateToWorldMatrix );

	for(int i = 0; i < (int) material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( material->m_textureMaps[i]->m_openglTextureID, i );
	}

	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_position );

	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_color );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_texCoords );

	
	glEnableVertexAttribArray( material->m_shader->m_tangentID );
	glVertexAttribPointer(	material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_tangent );

	glEnableVertexAttribArray( material->m_shader->m_biTangentID );
	glVertexAttribPointer(	material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_biTangent );

	glEnableVertexAttribArray( material->m_shader->m_normalID );
	glVertexAttribPointer(	material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_normal );
	
	//float myMatrix[16];
	//glGetFloatv (  GL_MODELVIEW_MATRIX ,  myMatrix );
	
	glEnableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );
	glVertexAttribPointer(	material->m_shader->m_perlinTexCoordsID, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3D_PCT), &vertexes[0].m_perlinCoords );

	glUniformMatrix4fv( material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );
	glDisableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );
	
	glDisableVertexAttribArray( material->m_shader->m_tangentID );
	glDisableVertexAttribArray( material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( material->m_shader->m_normalID );

	g_theRenderer->stackPop();
	g_theRenderer->stackPop();
}

//----------------------------------------------------------------------------------
void GLWModernVertexArray( const vector<ModelVertex>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, Material* material )
{
	material->useProgram();
	material->setShaderUniforms();

	glPointSize(1.f);

	if(vertexes.empty())
		return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 rotateToWorldMatrix = Matrix_4x4();
	rotateToWorldMatrix.setAsARotationMatrix(0.f, 0.f, 0.f, 0.0f);
	g_theRenderer->rotationMatrix(0.0f, 0.0f, 0.0f, 0.0f);

	rotateToWorldMatrix.transformByMatrix(objectToWorldMatrix);
	objectToWorldMatrix.transformByMatrix( rotateToWorldMatrix );

	for(int i = 0; i < (int) material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( material->m_textureMaps[i]->m_openglTextureID, i );
	}

	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_pos );

	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_color );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_texCoords );


	glEnableVertexAttribArray( material->m_shader->m_tangentID );
	glVertexAttribPointer(	material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_tangent );

	glEnableVertexAttribArray( material->m_shader->m_biTangentID );
	glVertexAttribPointer(	material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_bitangent );
	glEnableVertexAttribArray( material->m_shader->m_normalID );
	glVertexAttribPointer(	material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), &vertexes[0].m_normal );

	//float myMatrix[16];
	//glGetFloatv (  GL_MODELVIEW_MATRIX ,  myMatrix );

	//glEnableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );
	//glVertexAttribPointer(	material->m_shader->m_perlinTexCoordsID, 2, GL_FLOAT, GL_FALSE, sizeof(ModelVertex), &vertexes[0].m_perlinCoords );

	glUniformMatrix4fv( material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );
	glDisableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );

	glDisableVertexAttribArray( material->m_shader->m_tangentID );
	glDisableVertexAttribArray( material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( material->m_shader->m_normalID );

	g_theRenderer->stackPop();
	g_theRenderer->stackPop();

	GLWrapperDisableShader();
}

//----------------------------------------------------------------------------------
void GLWrapperFBOVertexArray( const vector<Vertex3D_PCT>& vertexes, const Vec3& translation, const GL_TYPES typeOfDrawing, FBO* fbo )
{
	if(vertexes.empty())
		return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	for(int i = 0; i < (int) fbo->m_material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( fbo->m_material->m_textureMaps[i]->m_openglTextureID, i );
	}

	glActiveTexture( GL_TEXTURE0 + 6 );
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, fbo->m_framebufferDepthTextureID );

	glActiveTexture( GL_TEXTURE0 + 5 );
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, fbo->m_framebufferColorTextureID );

	glEnableVertexAttribArray( fbo->m_material->m_shader->m_vertexID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_position );

	glEnableVertexAttribArray( fbo->m_material->m_shader->m_colorID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_color );

	glEnableVertexAttribArray( fbo->m_material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_texCoords );


	glEnableVertexAttribArray( fbo->m_material->m_shader->m_tangentID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_tangent );

	glEnableVertexAttribArray( fbo->m_material->m_shader->m_biTangentID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_biTangent );

	glEnableVertexAttribArray( fbo->m_material->m_shader->m_normalID );
	glVertexAttribPointer(	fbo->m_material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( Vertex3D_PCT ), &vertexes[0].m_normal );

	//float myMatrix[16];
	//glGetFloatv (  GL_MODELVIEW_MATRIX ,  myMatrix );

	//glEnableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );
	//glVertexAttribPointer(	material->m_shader->m_perlinTexCoordsID, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex3D_PCT), &vertexes[0].m_perlinCoords );

	glUniformMatrix4fv( fbo->m_material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( fbo->m_material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );

	glDrawArrays( typeOfDrawing, 0, vertexes.size() );

	glDisableVertexAttribArray( fbo->m_material->m_shader->m_vertexID );
	glDisableVertexAttribArray( fbo->m_material->m_shader->m_colorID );
	glDisableVertexAttribArray( fbo->m_material->m_shader->m_textCoordsID );

		
	//glDisableVertexAttribArray( fbo->m_material->m_shader->m_perlinTexCoordsID );

	glDisableVertexAttribArray( fbo->m_material->m_shader->m_tangentID );
	glDisableVertexAttribArray( fbo->m_material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( fbo->m_material->m_shader->m_normalID );

	//g_theRenderer->stackPop();
	g_theRenderer->stackPop();
}

//Code is from http://stackoverflow.com/questions/5988686/creating-a-3d-sphere-in-opengl-using-visual-c
//by datenwolf
//----------------------------------------------------------------------------------
void GLWModernSphere( float radius, unsigned int rings, unsigned int sectors, Vec3 position, Material* material)
{
	vector<GLfloat> vertices;
	vector<GLfloat> normals;
	vector<GLfloat> texcoords;
	vector<GLushort> indices;
	vector<GLubyte> color;

	const double M_PI = 3.141592653589793238462643383279502884197;
	float const R = 1.f/(float)(rings-1);
	float const S = 1.f/(float)(sectors-1);
	int r, s;

	color.resize(rings * sectors * 4);
	vertices.resize(rings * sectors * 3);
	normals.resize(rings * sectors * 3);
	texcoords.resize(rings * sectors * 2);
	std::vector<GLfloat>::iterator v = vertices.begin();
	std::vector<GLfloat>::iterator n = normals.begin();
	std::vector<GLfloat>::iterator t = texcoords.begin();
	for(r = 0; r < (int) rings; r++) for(s = 0; s < (int) sectors; s++) {
		float const y = (float const) sin( -M_PI * .5 + M_PI * r * R );
		float const x = (float const) (cos(2*M_PI * s * S) * sin( M_PI * r * R ));
		float const z = (float const) (sin(2*M_PI * s * S) * sin( M_PI * r * R ));

		*t++ = s*S;
		*t++ = r*R;

		*v++ = x * radius;
		*v++ = y * radius;
		*v++ = z * radius;

		*n++ = x;
		*n++ = y;
		*n++ = z;
	}

	indices.resize(rings * sectors * 4);
	std::vector<GLushort>::iterator i = indices.begin();
	for(r = 0; r < (int) rings-1; r++) for(s = 0; s < (int) sectors-1; s++) {
		*i++ = (unsigned short)(r * sectors + s);
		*i++ = (unsigned short)(r * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + (s+1));
		*i++ = (unsigned short)((r+1) * sectors + s);
	}

	GLWrapperEnableShaderTextures( 0, 0, material->m_textureMaps[1]->m_openglTextureID, material->m_textureMaps[0]->m_openglTextureID );
	glColor4f(1.f, 1.f, 1.f, 1.f);

	glPushMatrix();
	glTranslatef(position.x, position.y, position.z);

	for(int i = 0;  i < (int) color.size(); ++i)
	{
		color[i] = 1;
	}
	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, 0, &vertices[0] );
	
	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_FALSE, 0, &color[0] );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, 0, &texcoords[0] );

	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );


	glDrawElements(GL_QUADS, indices.size(), GL_UNSIGNED_SHORT, &indices[0]);

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );

	glPopMatrix();
}


//----------------------------------------------------------------------------------
void GLWrapperTranslate(const Vec3& translation)
{
	glTranslatef(translation.x, translation.y, translation.z);
}


//----------------------------------------------------------------------------------
void GLWrapperRotate(const Vec3& rotation, float angle)
{
	glRotatef(angle, rotation.x, rotation.y, rotation.z);
}


//----------------------------------------------------------------------------------
void GLWrapperDrawQuads( vector<Vec3>& points, vector<RGBAColors>& colorPerLine, Vec3& translation)
{
	glDisable(GL_TEXTURE_2D);
	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBegin( GL_QUADS );
	{
		for(int i = 0; i < (int) points.size(); ++i)
		{
			glColor4f(colorPerLine[i].m_red * g_charToFloat, colorPerLine[i].m_green * g_charToFloat, colorPerLine[i].m_blue * g_charToFloat, colorPerLine[i].m_alpha * g_charToFloat);
			glVertex3f(points[i].x, points[i].y, points[i].z);
		}
	}
	glEnd();

	glColor4f(1.f, 1.f, 1.f, 1.f);
	glPopMatrix();
}


//----------------------------------------------------------------------------------
void GLWrapperPushMatrix()
{
	glPushMatrix();
}


//----------------------------------------------------------------------------------
void GLWrapperPopMatrix()
{
	glPopMatrix();
}


//----------------------------------------------------------------------------------
void GLWrapperVertex2f(float x, float y)
{
	glVertex2f(x, y);
}


//----------------------------------------------------------------------------------
void GLWrapperTexCoord2f(float x, float y)
{
	glTexCoord2f(x, y);
}


//----------------------------------------------------------------------------------
void GLWrapperEnd()
{
	glEnd();
}


//----------------------------------------------------------------------------------
void GLWrapperBegin( GL_TYPES toBegin )
{
	glBegin( toBegin );
}


//----------------------------------------------------------------------------------
void GLWrapperScale( const Vec3& scale )
{
	glScalef( scale.x, scale.y, scale.z );
}


//----------------------------------------------------------------------------------
void GLWrapperDrawTexturedQuads( vector<Vec2>& textureCoords, vector<Vec3>& points, vector<RGBAColors>& colorPerLine, Vec3& translation, SpriteSheet* sprites )
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, sprites->m_texture->m_openglTextureID);

	glPushMatrix();
	glTranslatef(translation.x, translation.y, translation.z);

	glBegin( GL_QUADS );
	{
		for(int i = 0; i < (int) points.size(); ++i)
		{
			glColor4f( colorPerLine[i].m_red * g_charToFloat, colorPerLine[i].m_green * g_charToFloat, colorPerLine[i].m_blue * g_charToFloat, colorPerLine[i].m_alpha * g_charToFloat );
			glTexCoord2f( textureCoords[i].x, textureCoords[i].y );
			glVertex3f( points[i].x, points[i].y, points[i].z );
		}
	}
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	glColor4f(1.f, 1.f, 1.f, 1.f);
}


//----------------------------------------------------------------------------------
void GLWrapperPointSize( int size )
{
	glPointSize( (GLfloat) size );
}


//----------------------------------------------------------------------------------
void GLWrapperSwapBuffers(HDC displayDeviceContext)
{
	SwapBuffers(displayDeviceContext);
}


//----------------------------------------------------------------------------------------------
void GLWrapperEnableBlending()
{
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

//----------------------------------------------------------------------------------
void GLWrapperBlendMode(GL_TYPES one, GL_TYPES two)
{
	glBlendFunc( one, two );
}

//----------------------------------------------------------------------------------
void GLWrapperSetDepthTestToFalse()
{
	glDepthMask(GL_FALSE);
}

//----------------------------------------------------------------------------------
void GLWrapperSetDepthTestToTrue()
{
	glDepthMask(GL_TRUE);
}

//----------------------------------------------------------------------------------
void GLWrapperLineWidth(float newWidth)
{
	glLineWidth(newWidth);
}

//Code for drawing spheres is from Kevin Harris
//----------------------------------------------------------------------------------
void GLWrapperDrawSphere( const Vec3& translation, const RGBAColors& color, float radius )
{
	glPushMatrix();
	glTranslatef( translation.x, translation.y, translation.z );

	GLWrapperColorBytes(color);

	glLineWidth(.5f);
	int i,j;

	/* Adjust z and radius as stacks and slices are drawn. */

	double r;
	double x,y,z;

	/* Pre-computed circle */

	double *sint1,*cost1;
	double *sint2,*cost2;
	GLWrappercircleTableSphere(&sint1,&cost1,-30  );
	GLWrappercircleTableSphere(&sint2,&cost2, 30*2);

	/* Draw a line loop for each stack */

	for (i=1; i<30; i++)
	{
		z = cost2[i];
		r = sint2[i];

		glBegin(GL_LINE_LOOP);

		for(j=0; j<=30; j++)
		{
			x = cost1[j];
			y = sint1[j];

			//glNormal3d(x,y,z);
			glVertex3d(x*r*radius,y*r*radius,z*radius);
			//glVertex3d(x*r*2*m_radius,y*r*m_radius*2,z*m_radius*2);
		}

		glEnd();
	}

	/* Draw a line loop for each slice */

	for (i=0; i<30; i++)
	{
		glBegin(GL_LINE_STRIP);

		for(j=0; j<=30; j++)
		{
			x = cost1[i]*sint2[j];
			y = sint1[i]*sint2[j];
			z = cost2[j];

			//glNormal3d(x,y,z);
			glVertex3d(x*radius,y*radius,z*radius);
			//glVertex3d(x*r*2*m_radius,y*r*m_radius*2,z*m_radius*2);
		}

		glEnd();
	}

	/* Release sin and cos tables */

	free(sint1);
	free(cost1);
	free(sint2);
	free(cost2);

	glPopMatrix();

}

//Code for drawing spheres is from Kevin Harris
//----------------------------------------------------------------------------------
void GLWrappercircleTableSphere(double **sint,double **cost,const int n)
{
	int i;

	/* Table size, the sign of n flips the circle direction */

	const int size = abs(n);

	/* Determine the angle between samples */

	const double angle = 2*g_PI/(double)n;

	/* Allocate memory for n samples, plus duplicate of first entry at the end */

	*sint = (double *) calloc(sizeof(double), size+1);
	*cost = (double *) calloc(sizeof(double), size+1);

	/* Bail out if memory allocation fails, fgError never returns */

	if (!(*sint) || !(*cost))
	{
		free(*sint);
		free(*cost);
		//fgError("Failed to allocate memory in circleTable");
	}

	/* Compute cos and sin around the circle */

	for (i=0; i<size; i++)
	{
		(*sint)[i] = sin(angle*i);
		(*cost)[i] = cos(angle*i);
	}

	/* Last sample is duplicate of the first */

	(*sint)[size] = (*sint)[0];
	(*cost)[size] = (*cost)[0];
}

//----------------------------------------------------------------------------------
void GLWrapperEnableTexture( int textureID, int whereToBindTexture )
{
	if( textureID > -1 )
	{
		glActiveTexture( GL_TEXTURE0 + whereToBindTexture );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, textureID );
	}
}

//----------------------------------------------------------------------------------
void GLWrapperEnableShaderTextures( int emissiveTextureID, int specularTextureID, int normalTextureID, int diffuseTextureID )
{
	if( emissiveTextureID > -1 )
	{
		glActiveTexture( GL_TEXTURE0 + 3 );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, emissiveTextureID );
	}

	if( specularTextureID > -1 )
	{
		glActiveTexture( GL_TEXTURE0 + 2 );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, specularTextureID );
	}

	if( normalTextureID > -1 )
	{
		glActiveTexture( GL_TEXTURE0 + 1 );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, normalTextureID );
	}

	if( diffuseTextureID > -1 )
	{
		glActiveTexture( GL_TEXTURE0 );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, diffuseTextureID );
	}
}

//----------------------------------------------------------------------------------
void GLWrapperBindAttribLocation( int program, int index, const char* name )
{
	glBindAttribLocation( program, index, name );
}

//----------------------------------------------------------------------------------
void GLWrapperEnableAttribArray( int ID )
{
	glEnableVertexAttribArray( ID );
}

//----------------------------------------------------------------------------------
int GLWrapperGetTexture0Value()
{
	return GL_TEXTURE0;
}

//----------------------------------------------------------------------------------
void GLWrapperInitFBOColorTexture( unsigned int& textureID )
{
	// Create color framebuffer texture
	//glActiveTexture( GL_TEXTURE0 + 5 );
	glGenTextures( 1, &textureID );
	glBindTexture( GL_TEXTURE_2D, textureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) g_maxScreenX, (GLsizei) g_maxScreenY, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL ); // NOTE: RGBA and UNSIGNED BYTE
	//GL_TEXTURE_2D, 0,GL_RGB, 1024, 768, 0,GL_RGB, GL_UNSIGNED_BYTE, 0
}

//----------------------------------------------------------------------------------
void GLWrapperInitFBODepthTexture( unsigned int& textureID )
{
	// Create depth framebuffer texture
	//glActiveTexture( GL_TEXTURE0 + 6 );
	glGenTextures( 1, &textureID );
	glBindTexture( GL_TEXTURE_2D, textureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, (GLsizei) g_maxScreenX, (GLsizei) g_maxScreenY, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL ); // NOTE: DEPTH and UNSIGNED INT
}

//----------------------------------------------------------------------------------
void GLWrapperGenerateFBO( unsigned int& fboID, int colorBufferID, int depthBufferID )
{
	UNUSED(fboID);
	// Create an FBO (Framebuffer Object) and activate it
	//glGenFramebuffers( 1, &fboID );
	//glBindFramebuffer( GL_FRAMEBUFFER, fboID );

	// Attach our color and depth textures to the FBO, in the color0 and depth FBO "slots"
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBufferID, 0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthBufferID, 0 );
}

//----------------------------------------------------------------------------------
void GLWrapperBindFramebuffer( int fboID )
{
	glBindFramebuffer( GL_FRAMEBUFFER, fboID );
}

//----------------------------------------------------------------------------------
void GLWrapperGenerateVBO(unsigned int& vboID, vector<ModelVertex>& vertexes)
{
	if( vboID == 0 )
	{
		glGenBuffers( 1, &vboID );
	}

	glBindBuffer( GL_ARRAY_BUFFER, vboID );
	glBufferData( GL_ARRAY_BUFFER, sizeof( ModelVertex ) * vertexes.size(), vertexes.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
}

//----------------------------------------------------------------------------------
void GLWrapperGenerateIBO(unsigned int& iboID, vector<int>& vertexOrder)
{
	if( iboID == 0 )
	{
		glGenBuffers( 1, &iboID );
	}

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, iboID );
	glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( int ) * vertexOrder.size(), vertexOrder.data(), GL_STATIC_DRAW );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}

//----------------------------------------------------------------------------------
void GLWrapperRenderModernVBOWithIBO(unsigned int& vboID, unsigned int& iboID, int numberOfIndices, Vec3& translation, Material* material, GL_TYPES typeOfDrawing )
{
	material->useProgram();
	material->setShaderUniforms();

	glBindBuffer( GL_ARRAY_BUFFER, vboID );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, iboID );

	//if(vertexes.empty())
	//	return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 rotateToWorldMatrix = Matrix_4x4();
	rotateToWorldMatrix.setAsARotationMatrix(0.f, 0.f, 0.f, 0.0f);
	g_theRenderer->rotationMatrix(0.0f, 0.0f, 0.0f, 0.0f);

	rotateToWorldMatrix.transformByMatrix(objectToWorldMatrix);
	objectToWorldMatrix.transformByMatrix( rotateToWorldMatrix );

	for(int i = 0; i < (int) material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( material->m_textureMaps[i]->m_openglTextureID, i );
	}

	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_pos ) );

	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_color ) );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_texCoords ) );


	glEnableVertexAttribArray( material->m_shader->m_tangentID );
	glVertexAttribPointer(	material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_tangent ) );

	glEnableVertexAttribArray( material->m_shader->m_biTangentID );
	glVertexAttribPointer(	material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_bitangent ) );
	glEnableVertexAttribArray( material->m_shader->m_normalID );
	glVertexAttribPointer(	material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_normal ) );

	glUniformMatrix4fv( material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );


	glDrawElements( typeOfDrawing, numberOfIndices, GL_UNSIGNED_INT, 0);

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );
	glDisableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );

	glDisableVertexAttribArray( material->m_shader->m_tangentID );
	glDisableVertexAttribArray( material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( material->m_shader->m_normalID );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	g_theRenderer->stackPop();
	g_theRenderer->stackPop();

	GLWrapperDisableShader();
}

//----------------------------------------------------------------------------------
void GLWrapperRenderModernVBOWithIBO(Mesh& mesh, Vec3& translation, Material* material, GL_TYPES typeOfDrawing )
{
	material->useProgram();
	material->setShaderUniforms();

	glBindBuffer( GL_ARRAY_BUFFER, mesh.m_vboID );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mesh.m_iboID );

	//if(vertexes.empty())
	//	return;

	g_theRenderer->translateMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 objectToWorldMatrix = Matrix_4x4();
	objectToWorldMatrix.setAsATranslationMatrix( translation.x, translation.y, translation.z );

	Matrix_4x4 rotateToWorldMatrix = Matrix_4x4();
	rotateToWorldMatrix.setAsARotationMatrix(0.f, 0.f, 0.f, 0.0f);
	g_theRenderer->rotationMatrix(0.0f, 0.0f, 0.0f, 0.0f);

	rotateToWorldMatrix.transformByMatrix(objectToWorldMatrix);
	objectToWorldMatrix.transformByMatrix( rotateToWorldMatrix );

	for(int i = 0; i < (int) material->m_textureMaps.size(); ++i)
	{
		GLWrapperEnableTexture( material->m_textureMaps[i]->m_openglTextureID, i );
	}

	GLWrapperEnableTexture(  mesh.m_diffuseID->m_openglTextureID, 0 );
	GLWrapperEnableTexture(  mesh.m_normalID->m_openglTextureID, 1 );
	GLWrapperEnableTexture(  mesh.m_specularID->m_openglTextureID, 2 );
	GLWrapperEnableTexture(  mesh.m_emissiveID->m_openglTextureID, 3 );

	glActiveTexture( GL_TEXTURE0 );
	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, mesh.m_diffuseID->m_openglTextureID );

	glEnableVertexAttribArray( material->m_shader->m_vertexID );
	glVertexAttribPointer(	material->m_shader->m_vertexID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_pos ) );

	glEnableVertexAttribArray( material->m_shader->m_colorID );
	glVertexAttribPointer(	material->m_shader->m_colorID , 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_color ) );

	glEnableVertexAttribArray( material->m_shader->m_textCoordsID );
	glVertexAttribPointer(	material->m_shader->m_textCoordsID , 2, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_texCoords ) );


	glEnableVertexAttribArray( material->m_shader->m_tangentID );
	glVertexAttribPointer(	material->m_shader->m_tangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_tangent ) );

	glEnableVertexAttribArray( material->m_shader->m_biTangentID );
	glVertexAttribPointer(	material->m_shader->m_biTangentID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_bitangent ) );
	glEnableVertexAttribArray( material->m_shader->m_normalID );
	glVertexAttribPointer(	material->m_shader->m_normalID , 3, GL_FLOAT, GL_FALSE, sizeof( ModelVertex ), (const GLvoid*) offsetof( ModelVertex, m_normal ) );

	glUniformMatrix4fv( material->m_shader->m_uniforms.objectToWorldLocation, 1, false, &objectToWorldMatrix.m_matrix[0] );
	glUniformMatrix4fv( material->m_shader->m_uniforms.projectionMatrixLocation, 1, false, &g_theRenderer->getBackOfMatrix().m_matrix[0] );


	glDrawElements( typeOfDrawing, mesh.m_numIndexes, GL_UNSIGNED_INT, 0);

	glDisableVertexAttribArray( material->m_shader->m_vertexID );
	glDisableVertexAttribArray( material->m_shader->m_colorID );
	glDisableVertexAttribArray( material->m_shader->m_textCoordsID );
	glDisableVertexAttribArray( material->m_shader->m_perlinTexCoordsID );

	glDisableVertexAttribArray( material->m_shader->m_tangentID );
	glDisableVertexAttribArray( material->m_shader->m_biTangentID );
	glDisableVertexAttribArray( material->m_shader->m_normalID );

	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );

	g_theRenderer->stackPop();
	g_theRenderer->stackPop();

	GLWrapperDisableShader();
}