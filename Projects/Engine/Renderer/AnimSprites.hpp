#pragma once

#include "Texture.hpp"

class Sprites
{
public:
	Sprites(char* textureName , Vec2i spriteDimentions , float timePerFrame = 0.0f , Vec2i animationIndexRange = Vec2i(0,0) );
	Sprites(Texture* texture, Vec2i spriteDimentions , float timePerFrame = 0.0f , Vec2i animationIndexRange = Vec2i(0,0), bool loopMultiple = false );
	~Sprites(void);
	Vec2f GetCoordinateByIndex(const int index);
	Vec2f GetCoordinateByXY(const int x , const int y);
	void SetRenderSize(const Vec2f size);
	void Update(const Vec2f& position);
	void Draw();
	void Draw(const int index);

	void SetAnimationRange( const Vec2i& animationIndexRange );

	Vec2f m_sizeOfEachSprites;
	int m_numOfSprites;
	int m_currentSpritesIndex;
	Texture* m_texture;

	Vec2f m_halfRenderSize;
	Vec2i m_spriteDimentions;
	Vec2f m_position;

	float m_deltaTime;
	Vec2i m_currentIndexRange;
	int m_currentIndex;
	float m_startingTimeOfCurrentImageFrame;
	bool m_loopMultiple;
	bool m_startingIndex;
	bool m_leftStartingIndex;
};