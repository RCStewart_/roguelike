#include "Engine\Renderer\FBO.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Renderer\Material.hpp"
#include "Engine\Renderer\ShaderProgram.hpp"
#include "Engine\Core\GameCommon.hpp"

FBO::FBO(void)
{
	//Provided by Squirrel Eiserloh
	m_framebufferObjectID = 0;
	m_framebufferColorTextureID = 0;
	m_framebufferDepthTextureID = 0;

	// Create color framebuffer texture
	glActiveTexture( GL_TEXTURE0 + 5 );
	glGenTextures( 1, &m_framebufferColorTextureID );
	glBindTexture( GL_TEXTURE_2D, m_framebufferColorTextureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) g_maxScreenX, (GLsizei) g_maxScreenY, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL ); // NOTE: RGBA and UNSIGNED BYTE

	// Create depth framebuffer texture
	glActiveTexture( GL_TEXTURE0 + 6 );
	glGenTextures( 1, &m_framebufferDepthTextureID );
	glBindTexture( GL_TEXTURE_2D, m_framebufferDepthTextureID );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, (GLsizei) g_maxScreenX, (GLsizei) g_maxScreenY, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL ); // NOTE: DEPTH and UNSIGNED INT

	// Create an FBO (Framebuffer Object) and activate it
	glGenFramebuffers( 1, &m_framebufferObjectID );
	glBindFramebuffer( GL_FRAMEBUFFER, m_framebufferObjectID );

	// Attach our color and depth textures to the FBO, in the color0 and depth FBO "slots"
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_framebufferColorTextureID, 0 );
	glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_framebufferDepthTextureID, 0);
}

//Once per scene
//----------------------------------------------------------------------------------
void FBO::startFramebuffer()
{
	GLWrapperBindFramebuffer( m_framebufferObjectID );
}

//Once per scene
//----------------------------------------------------------------------------------
void FBO::disableFramebuffer()
{
	GLWrapperBindFramebuffer( 0 );
}

FBO::~FBO(void)
{
}
