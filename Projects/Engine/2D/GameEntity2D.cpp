#include "GameEntity2D.hpp"

int GameEntity2D::s_nextGameEntity2DID = 1;

//----------------------------------------------------------------------------------
GameEntity2D::GameEntity2D(const std::vector<Component*>& components)
{
	m_ID = s_nextGameEntity2DID++;
	m_components = components;
}

//----------------------------------------------------------------------------------
GameEntity2D::GameEntity2D()
{
	m_ID = s_nextGameEntity2DID++;
}

//----------------------------------------------------------------------------------
GameEntity2D::~GameEntity2D()
{
	clearComponents();
}


//----------------------------------------------------------------------------------

void GameEntity2D::update(float delta)
{

	updateTimeDead(delta);
}

//----------------------------------------------------------------------------------
void GameEntity2D::clearComponents()
{
	for (int componentIndex = 0; componentIndex < m_components.size(); ++componentIndex)
	{
		delete m_components[componentIndex];
	}
}

//----------------------------------------------------------------------------------
void GameEntity2D::updateTimeDead(float delta)
{
	if (m_timeDead == 0)
	{

	}
	else
	{
		m_timeDead += delta;
	}
}

//----------------------------------------------------------------------------------
Component* GameEntity2D::getComponentByName(const std::string& name)
{
	for (int i = 0; i < m_components.size(); ++i)
	{
		if (m_components[i]->m_name == name)
		{
			return m_components[i];
		}
	}

	return nullptr;
}