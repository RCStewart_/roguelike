#include "TimeComponent.hpp"

ComponentRegistration TimeComponent::s_timeComponentRegistration("TIME_COMPONENT", &TimeComponent::createComponent);

//----------------------------------------------------------------------------------
Component* TimeComponent::createComponent(const std::string& name)
{
	return new TimeComponent();
}

//----------------------------------------------------------------------------------
Component* TimeComponent::createComponent(const std::string& name, TiXmlElement* componentData)
{
	return new TimeComponent(componentData);
}

//----------------------------------------------------------------------------------
void TimeComponent::init()
{
	initialized = true;
}

//----------------------------------------------------------------------------------
TimeComponent::TimeComponent(TimeComponent& toCopy)
{
	initialized = true;
	m_name = "TimeComponent";
	m_referenceToOwner = nullptr;
	m_timers = toCopy.m_timers;

	for (int i = 0; i < m_timers.size(); ++i)
	{
		m_timers[i].m_timeAddedSoFar = 0;
		m_timers[i].m_timeOfLastTrigger = GetAbsoluteTimeSeconds();
	}
}

//----------------------------------------------------------------------------------
TimeComponent::TimeComponent(TiXmlElement* componentData)
{
	m_name = "TimeComponent";

	for (TiXmlElement* triggerToAdd = componentData->FirstChildElement(); triggerToAdd != NULL; triggerToAdd = triggerToAdd->NextSiblingElement())
	{
		int timesToTrigger = -1;
		double timeDelay = 0.0;

		AnyDataTypeMap verbAttributes;
		Verb* baseVerb;
		std::string nameOfBehavior = triggerToAdd->Attribute("name");

		baseVerb = Verb::s_generatedVerbs[nameOfBehavior]->getClone();
		baseVerb->fillAnyDataMapWithAttributesFromXMLElement(triggerToAdd, verbAttributes);

		verbAttributes.fillTargetMapWithDataFromSelf(baseVerb->m_attributes);

		timesToTrigger = std::stoi(triggerToAdd->Attribute("TIMES_TO_TRIGGER"));
		timeDelay = std::stod(triggerToAdd->Attribute("TIME_DELAY"));

		m_timers.push_back(TimedTrigger(timesToTrigger, baseVerb, timeDelay));
	}
	 
	initialized = false;
}

//----------------------------------------------------------------------------------
TimeComponent::TimeComponent()
{
	initialized = true;
	m_name = "TimeComponent";
}

//----------------------------------------------------------------------------------
TimeComponent::~TimeComponent()
{

}

//----------------------------------------------------------------------------------
void TimeComponent::update(float delta)
{
	if (!initialized)
	{
		return;
	}

	for (int i = 0; i < m_timers.size(); ++i)
	{
		m_timers[i].update(delta, m_referenceToOwner);
	}

	for (std::vector<TimedTrigger>::iterator it = m_timers.begin(); it != m_timers.end();)
	{
		if (it->m_timesToTrigger == 0)
			it = m_timers.erase(it);
		else
			++it;
	}
}

//----------------------------------------------------------------------------------
void TimeComponent::placeAttributesIntoGameEntity(Noun* owner)
{
	m_referenceToOwner = owner;
}

//----------------------------------------------------------------------------------
void TimeComponent::registerTimer(int timesToTrigger, double timeDelay, const std::string& nameOfVerb, AnyDataTypeMap& verbAttributes)
{
	Verb* toAdd = Verb::s_generatedVerbs[nameOfVerb]->getClone(verbAttributes);
	toAdd->setAttributes(verbAttributes);

	TimedTrigger timer = TimedTrigger(timesToTrigger, toAdd, timeDelay);
	m_timers.push_back(timer);
}

//----------------------------------------------------------------------------------
TimeComponent* TimeComponent::getClone(AnyDataTypeMap& withTheseAttributeModifications)
{
	return new TimeComponent(*this);
}

//----------------------------------------------------------------------------------
TimeComponent* TimeComponent::getClone()
{
	return new TimeComponent(*this);
}