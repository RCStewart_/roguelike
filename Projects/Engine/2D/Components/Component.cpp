#include "Engine/Language/Noun.hpp"
#include "Component.hpp"

ComponentRegistrationMap* ComponentRegistration::s_componentRegistration = nullptr;
ComponentMap Component::s_generatedComponents;

//----------------------------------------------------------------------------------
Component* ComponentRegistration::createComponent()
{
	return (*s_componentRegistration)[m_name]->m_registrationFunc(m_name);
}

//----------------------------------------------------------------------------------
Component* Component::createComponent(const std::string& name, TiXmlElement* verb)
{
	return new Component();
}

//----------------------------------------------------------------------------------
Component* Component::createComponent(const std::string& name)
{
	return new Component();
}

//----------------------------------------------------------------------------------
Component::Component()
{
	m_name = "Component";
}


//----------------------------------------------------------------------------------
Component::~Component()
{

}

//----------------------------------------------------------------------------------
void Component::update(float delta)
{

}


//----------------------------------------------------------------------------------
void Component::placeAttributesIntoGameEntity(Noun* owner)
{

}

//----------------------------------------------------------------------------------
int Component::createComponents()
{
	int numOfRegistrations = 0;
	ComponentRegistrationMap* registrations = ComponentRegistration::getComponentRegistrations();
	if (registrations)
	{
		for (ComponentRegistrationMap::iterator registerIterator = registrations->begin(); registerIterator != registrations->end(); registerIterator++)
		{
			++numOfRegistrations;
			ComponentRegistration* registration = registerIterator->second;
			Component* componentGenerator = registration->createComponent();
			std::string name = registration->getName();
			s_generatedComponents[name] = componentGenerator;
		}
	}
	return numOfRegistrations;
}