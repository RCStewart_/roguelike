#pragma once
#include "Core2D.hpp";

#include "Engine/Math/Vec2.hpp"
#include "Engine/Core/RGBAColors.hpp"
#include "Engine/Core/AnyDataTypeMap.hpp"
#include "Engine/Core/AnyDataType.hpp"
#include "Component.hpp"

class Noun;

class Core2D : Component
{
public:
	Core2D(Core2D& toCopy);
	Core2D();
	Core2D(TiXmlElement* componentData);

	Core2D(Vec2& pos, Vec2& size, Vec2& rotation, RGBAColors& color, float lifeSpan, float currentHealth, float maxHealth,
		float radius) :
		m_position(pos),
		m_size(size),
		m_rotation(rotation),
		m_color(color),
		m_lifeSpan(lifeSpan),
		m_currentHealth(currentHealth),
		m_maxHealth(maxHealth),
		m_radius(radius){};

	virtual ~Core2D();

	void placeAttributesIntoGameEntity(Noun* owner);

	virtual Core2D* getClone(AnyDataTypeMap& withTheseAttributeModifications);
	virtual Core2D* getClone();

	static Component* createComponent(const std::string& name);
	virtual Component* createComponent(const std::string& name, TiXmlElement* componentData);

	Vec2 m_position;
	Vec2 m_size;
	Vec2 m_rotation;

	RGBAColors m_color;

	float m_lifeSpan;
	float m_currentHealth;
	float m_maxHealth;
	float m_radius;

protected:
	static ComponentRegistration s_core2DComponentRegistration;
};