#pragma once
#include "Component.hpp"
#include "Engine/Renderer/SpriteSheet.hpp"
#include "Engine/Math/IntVec2.hpp"
#include <string>
#include <vector>

class Render2D : public Component
{
public:
	Render2D(Render2D& toCopy);
	Render2D();
	Render2D(TiXmlElement* componentData);
	Render2D(const IntVec2& spriteCoords, int zDepth, bool hasTransparency, std::string& spriteSheetFileName);
	virtual ~Render2D();

	virtual void update(float delta);
	virtual void placeAttributesIntoGameEntity(Noun* owner);
	virtual void render();

	void addSpriteSheetBasedOnFileName(std::string& fileName, int spritesWide, int spritesHigh);

	static void sRenderAllRender2DComponents();
	static void sRemoveIDFromList(int id);
	static void sAddComponentToList(Render2D* componentToAdd);

	virtual Render2D* getClone(AnyDataTypeMap& withTheseAttributeModifications);
	virtual Render2D* getClone();
	static  Component* createComponent(const std::string& name);
	virtual  Component* createComponent(const std::string& name, TiXmlElement* componentData);
	virtual void init();

	IntVec2 m_spriteCoords;
	int m_spriteSheetID;
	int m_zDepth;
	int m_renderComponentID;

	static int s_nextRenderComponentID;
	static std::vector<Render2D*> s_renderComponents;

	Noun* m_referenceToOwner;
	bool m_hasTransparency;

protected:
	static ComponentRegistration s_render2DComponentRegistration;

};