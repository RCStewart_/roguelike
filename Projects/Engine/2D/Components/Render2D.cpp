#include "Render2D.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Language/Noun.hpp"
#include <algorithm>

int Render2D::s_nextRenderComponentID = 1;
std::vector<Render2D*> Render2D::s_renderComponents;

ComponentRegistration Render2D::s_render2DComponentRegistration("RENDER_2D_COMPONENT", &Render2D::createComponent);

//----------------------------------------------------------------------------------
Component* Render2D::createComponent(const std::string& name, TiXmlElement* componentData)
{
	return new Render2D(componentData);
}

//----------------------------------------------------------------------------------
Component* Render2D::createComponent(const std::string& name)
{
	return new Render2D();
}

//----------------------------------------------------------------------------------
Render2D::Render2D()
{
	m_name = "Render2D";
}

//----------------------------------------------------------------------------------
Render2D::Render2D(Render2D& toCopy)
{
	m_spriteCoords = toCopy.m_spriteCoords;
	m_spriteSheetID = toCopy.m_spriteSheetID;
	m_zDepth = toCopy.m_zDepth;

	m_renderComponentID = s_nextRenderComponentID;
	++s_nextRenderComponentID;

	m_referenceToOwner = nullptr;
	m_hasTransparency = toCopy.m_hasTransparency;
}

//----------------------------------------------------------------------------------
Render2D::Render2D(TiXmlElement* componentData)
{
	m_name = "Render2D";

	m_hasTransparency = true;
	m_zDepth = 0;
	m_spriteCoords = IntVec2(0, 0);

	if (!g_theRenderer)
	{
		g_theRenderer = new Renderer();
		g_theRenderer->init();
	}

	m_renderComponentID = s_nextRenderComponentID;
	++s_nextRenderComponentID;

	const char* attrib = nullptr;

	//Transparency
	attrib = componentData->Attribute("HAS_TRANSPARENCY");
	if (attrib != nullptr)
		m_hasTransparency = std::stoi(attrib);

	//Z Depth
	attrib = componentData->Attribute("Z_DEPTH");
	if (attrib != nullptr)
		m_zDepth = std::stoi(attrib);

	//Sprite Coords
	attrib = componentData->Attribute("SPRITE_COORDS_X");
	if (attrib != nullptr)
		m_spriteCoords.x = std::stoi(attrib);

	attrib = componentData->Attribute("SPRITE_COORDS_Y");
	if (attrib != nullptr)
		m_spriteCoords.y = std::stoi(attrib);

	//File Path
	attrib = componentData->Attribute("SPRITE_SHEET");
	if (attrib != nullptr)
		m_spriteSheetID = g_theRenderer->getSpriteSheetIDFromFileName(std::string(attrib));

}

//----------------------------------------------------------------------------------
void Render2D::init()
{
	//add self to list
	Render2D::sAddComponentToList(this);
}

//----------------------------------------------------------------------------------
bool SortRender2DComp(Render2D* lhs, Render2D* rhs)
{
	if (lhs->m_zDepth == rhs->m_zDepth)
	{
		return lhs->m_hasTransparency < rhs->m_hasTransparency;
	}
	return lhs->m_zDepth > rhs->m_zDepth;
}

//----------------------------------------------------------------------------------
Render2D::Render2D(const IntVec2& spriteCoords, int zDepth, bool hasTransparency, std::string& spriteSheetFileName)
	: m_hasTransparency(hasTransparency)
	, m_zDepth(zDepth)
	, m_spriteCoords(spriteCoords)
{
	if (!g_theRenderer)
	{
		g_theRenderer = new Renderer();
		g_theRenderer->init();
	}

	m_renderComponentID = s_nextRenderComponentID;
	++s_nextRenderComponentID;

	m_spriteSheetID = g_theRenderer->getSpriteSheetIDFromFileName(spriteSheetFileName);
	m_name = "Render2D";
}

//----------------------------------------------------------------------------------
Render2D::~Render2D()
{
	//Remove component from list
	Render2D::sRemoveIDFromList(m_renderComponentID);
}

//----------------------------------------------------------------------------------
void Render2D::update(float delta)
{

}

//----------------------------------------------------------------------------------
void Render2D::render()
{
	if (m_referenceToOwner)
	{
		g_theRenderer->render2DSprite(
			m_spriteSheetID,
			m_spriteCoords,
			m_referenceToOwner->m_attributes.getRawData<Vec2>("POSITION"),
			m_referenceToOwner->m_attributes.getRawData<Vec2>("SIZE"),
			m_referenceToOwner->m_attributes.getRawData<RGBAColors>("COLOR"));
	}
}

//----------------------------------------------------------------------------------
void Render2D::placeAttributesIntoGameEntity(Noun* owner)
{
	m_referenceToOwner = owner;
	owner->m_attributes.set<IntVec2>("SPRITE_COORDS", m_spriteCoords);
	owner->m_attributes.set<int>("SPRITE_SHEET_ID", m_spriteSheetID);
	owner->m_attributes.set<int>("Z_DEPTH", m_zDepth);
	owner->m_attributes.set<int>("RENDER_COMPONENT_ID", m_renderComponentID);
}

//----------------------------------------------------------------------------------
void Render2D::sAddComponentToList(Render2D* componentToAdd)
{
	s_renderComponents.push_back(componentToAdd);
	std::sort(s_renderComponents.begin(), s_renderComponents.end(), SortRender2DComp);
}

//----------------------------------------------------------------------------------
void Render2D::sRenderAllRender2DComponents()
{
	for (int i = 0; i < s_renderComponents.size(); ++i)
	{
		s_renderComponents[i]->render();
	}
}


//----------------------------------------------------------------------------------
void Render2D::sRemoveIDFromList(int id)
{
	for (int i = 0; i < s_renderComponents.size(); ++i)
	{
		if (id == s_renderComponents[i]->m_renderComponentID)
		{
			s_renderComponents.erase(s_renderComponents.begin() + i);
			return;
		}
	}
}

//----------------------------------------------------------------------------------
void Render2D::addSpriteSheetBasedOnFileName(std::string& fileName, int spritesWide, int spritesHigh)
{
	m_spriteSheetID = g_theRenderer->getSpriteSheetIDFromFileName(fileName, spritesWide, spritesHigh);
}

//----------------------------------------------------------------------------------
Render2D* Render2D::getClone(AnyDataTypeMap& withTheseAttributeModifications)
{
	return new Render2D(*this);
}

//----------------------------------------------------------------------------------
Render2D* Render2D::getClone()
{
	return new Render2D(*this);
}