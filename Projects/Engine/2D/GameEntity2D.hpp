#pragma once
#include <vector>
#include "Components/Component.hpp"
#include <string>

class GameEntity2D{
public:
	GameEntity2D();
	GameEntity2D(const std::vector<Component*>& components);
	virtual ~GameEntity2D();

	virtual void update(float delta);
	void updateTimeDead(float delta);
	void clearComponents();

	Component* getComponentByName(const std::string& name);

	int m_ID;
	float m_timeDead;

private:
	static int s_nextGameEntity2DID;

protected:
	std::vector<Component*> m_components;
};