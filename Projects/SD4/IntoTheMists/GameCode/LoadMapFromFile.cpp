#include "LoadMapFromFile.hpp"
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"
#include "Map.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Core/GameCommon.hpp"

MapGeneratorRegistration LoadMapFromFile::s_loadMapRegistration( "Load From File", &LoadMapFromFile::createGenerator );

//----------------------------------------------------------------------------------
LoadMapFromFile::LoadMapFromFile(const std::string& name) : BaseMapGenerator( "Load From File" )
{
	UNUSED( name );
}

//----------------------------------------------------------------------------------
LoadMapFromFile::~LoadMapFromFile(void)
{
}

//----------------------------------------------------------------------------------
int LoadMapFromFile::loadMapInformationFromXML(std::string& xmlFileName, Map* map)
{
	TiXmlDocument doc;

	if(!doc.LoadFile(xmlFileName.c_str()))
	{
		return 0;
	}

	TiXmlElement* root = doc.FirstChildElement();

	if(root == NULL)
	{
		doc.Clear();
		return 0;
	}

	TiXmlElement* cellData = root->FirstChildElement( "CellData" );

	const char* cellTypeBuffer = cellData->GetText();
	std::string bufferContainer = cellTypeBuffer;
	int sizeOfMap = map->m_cells.size();

	int mapIndex = sizeOfMap - map->m_numOfCellsWide;
	for(int cellTypeBufferIndex = 0; cellTypeBufferIndex < (int) map->m_cells.size(); ++cellTypeBufferIndex)
	{
		if(bufferContainer[cellTypeBufferIndex] != ' ')
		{
			map->m_cells[mapIndex].setTypeWithCharGlyph(bufferContainer[cellTypeBufferIndex]);
			++mapIndex;
		}
		else
		{
			mapIndex = mapIndex - 2 * map->m_numOfCellsWide;
		}
	}
	return 0;
}

//----------------------------------------------------------------------------------
void LoadMapFromFile::generateMap( Map* map )
{
	//TODO: Get input to select an XML file

	std::string filePath = "Data/Maps/";
	std::string xmlFileName = "testMap.xml";
	std::string fullPath = filePath + xmlFileName;
	loadMapInformationFromXML( fullPath, map );

}

//----------------------------------------------------------------------------------
void LoadMapFromFile::processOneStep( Map* map )
{
	UNUSED(map);
}

//----------------------------------------------------------------------------------
void LoadMapFromFile::processSeveralSteps( Map* map )
{
	UNUSED(map);
}

//----------------------------------------------------------------------------------
BaseMapGenerator* LoadMapFromFile::createGenerator( const std::string& name )
{
	return new LoadMapFromFile( name );
}