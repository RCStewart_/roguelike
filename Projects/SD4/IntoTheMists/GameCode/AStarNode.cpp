#include "AStarNode.hpp"


AStarNode::AStarNode( const IntVec2& position, const float& currentCost, const float& hueristic, AStarNode* parent )
	:m_position( position )
	,m_currentCost( currentCost )
	,m_hueristic( hueristic )
	,m_parent(parent)
{
}

//----------------------------------------------------------------------------------
AStarNode::~AStarNode(void)
{
}

//----------------------------------------------------------------------------------
bool AStarNode::howToSortAStarNodeInInverseOrder( AStarNode* left, AStarNode* right )
{
	return left->getFullCost() > right->getFullCost();
}

//----------------------------------------------------------------------------------
float AStarNode::getFullCost()
{
	m_f = m_currentCost + m_hueristic;
	return m_currentCost + m_hueristic;
}

/*
 push start node on list
while(!done)
{
	pullOff lowest f cost, insert into closed
	get adjacent squares(of one just checked)
	for(adjacents)
	{
		if (on closed)
			continue;
		if(open list)
		{
			if( its g <= the new g?)
			{
				continue;
			}
			else
				update
		}
		push on openList
	}
}
*/