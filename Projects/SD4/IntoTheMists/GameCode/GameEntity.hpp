#pragma once

#include "Engine/Math/IntVec2.hpp"
#include <string>
#include "Engine/Core/RGBAColors.hpp"
#include <map>
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"

class Map;
class FontGenerator;
class TiXmlElement;
struct DefendData;
struct AttackData;

struct gameEntityStats
{
	std::string name;		
	int ID;				
	int positionIndex;
	IntVec2 position;		
};

class GameEntity
{
public:
	GameEntity( Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color );

	~GameEntity(void);

	virtual void update( Map* map, double deltaSeconds );
	virtual void render( Map* map, FontGenerator* font );
	
	bool isDebug( int debugLevel = 1) const;
	virtual void setMaxHealth( const int& maxHealth );
	virtual void setCurrentHealth( const int& currentHealth);
	virtual void getDefense( const AttackData& inAttackData, DefendData& outDefendData );
	virtual void getOffense( AttackData& outDefendData );

	virtual void setCombatStats( const IntVec2& damageRange, const IntVec2& baseChanceToHit, int bonusChanceToHit, int armorClass, int health );

	virtual void saveToXML( TiXmlElement* gameEntity );
	virtual void loadFromXML( TiXmlElement* gameEntityNode );
	virtual void resolvePointers( std::map<int, GameEntity*>& oldIDsToEntityMap );

	void appendAttributeToXMLElement( const std::string& attributeName, const std::string& toAdd, const std::string& defaultValue, TiXmlElement* parent );
	
	template<typename T>
	void applyPointerIDFromXMLToGameEntity( const std::string& attributeName, TiXmlElement* element, T*& pointerToAdd );

	static gameEntityStats getGeneralGameEntityStatsFromXMLNode( TiXmlElement* gameEntity, Map* map );

	IntVec2 m_position;
	IntVec2 m_startingPosition;
	std::string m_name;
	int m_ID;
	char m_glyph;
	RGBAColors m_color;
	int m_currentHealth;
	int m_maxHealth;
	int m_debugLevel;
	static int s_nextGameEntityID;
	Map* m_map;
	bool m_isDead;


	int m_armorClass;
	IntVec2 m_reflectiveDamageRange;

	IntVec2 m_damageRange;
	IntVec2 m_baseChanceToHit;
	int m_bonusChanceToHit;
};

template<typename T>
void GameEntity::applyPointerIDFromXMLToGameEntity( const std::string& attributeName, TiXmlElement* element, T*& pointerToAdd )
{
	if(element->Attribute( attributeName.c_str() ) )
	{
		int ID = std::stoi( element->Attribute( attributeName.c_str() ));
		pointerToAdd = (T*)( ID ); 
	}
}