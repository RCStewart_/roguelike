#include "Chase.hpp"
#include "Engine/Math/Vec2.hpp"
#include "AStarPathing.hpp"
#include "Agent.hpp"
#include "Player.hpp"
#include "FactionManager.hpp"
#include "Engine/Core/GameCommon.hpp"

AIBehaviorRegistration Chase::s_ChaseRegistration( "Chase", &Chase::createAndGetAIBehavior );

Chase::Chase( std::string name ) 
	: BaseAIBehavior( name ), topTarget(nullptr)
{

}

Chase::~Chase(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Chase::createAndGetAIBehavior( const std::string& name )
{
	return new Chase( name );
}

//----------------------------------------------------------------------------------
bool Chase::Think( Map* map, Agent* thinker )
{
	bool thoughtResult = true;

	if(topTarget)
	{
		AStarPathing pathFinder;
		pathFinder.generatePath(thinker->m_currentPath, thinker->m_position, topTarget->m_position, map, thinker);
	}

	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Chase::calculateImportance( Map* map, Agent* thinker)
{
	//float importance = 0.f;
	float maxImportance = (float) thinker->m_lineOfSight;

	if(rand() % 10 < 1)
	{
		return 0.0f;
	}

	//IntVec2 positionOfPlayer = map->m_thePlayer->m_position;

	std::set<int>::iterator it;
	int lowestCalculatedLoyalty = 0;
	float threat = 0.f;
	topTarget = nullptr;
	for (it = thinker->m_cellsCanSee.begin(); it != thinker->m_cellsCanSee.end(); ++it)
	{
		if(map->m_cells[*it].m_occupant)
		{
			if( FactionManager::isEnemy( map->m_cells[*it].m_occupant, thinker ))
			{
				int howHated = thinker->getLoyaltyForAgentBasedOnID( map->m_cells[*it].m_occupant );
				float totalThreat = 0.f;
				{
					IntVec2 cellPos = map->m_cells[*it].m_occupant->m_position;
					float	xDisplacement = (float) ( cellPos.x - thinker->m_position.x );
					float	yDisplacement = (float) ( cellPos.y - thinker->m_position.y );
					Vec2	lengthVector = Vec2(xDisplacement, yDisplacement);
					float	length = lengthVector.getMagnetude();
					totalThreat = ((maxImportance / (length + 1.f)) * howHated * .5f + howHated) * -1.f;
				}

				if( totalThreat > threat)
				{
					lowestCalculatedLoyalty = howHated;
					threat = totalThreat;					
					topTarget = map->m_cells[*it].m_occupant;
				}
			}
		}
	}

	if(topTarget)
	{
		IntVec2 cellPos = topTarget->m_position;
		float xDisplacement = (float) ( cellPos.x - thinker->m_position.x );
		float yDisplacement = (float) ( cellPos.y - thinker->m_position.y );
		Vec2 lengthVector = Vec2(xDisplacement, yDisplacement);
		float length = lengthVector.getMagnetude();
		return maxImportance / (length + 1.f);
	}

	return 0.0f;
}

//----------------------------------------------------------------------------------
void Chase::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	UNUSED(name);
	UNUSED(attributeAsString);
}