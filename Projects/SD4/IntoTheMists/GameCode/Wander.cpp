#include "Wander.hpp"
#include "Engine/Core/GameCommon.hpp"

AIBehaviorRegistration Wander::s_wanderRegistration( "Wander", &Wander::createAndGetAIBehavior );

Wander::Wander( std::string name ) 
	: BaseAIBehavior( name )
	, m_chanceToMoveStrait(0.f)
	, m_chanceToRest(0.f)
{

}

Wander::~Wander(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Wander::createAndGetAIBehavior( const std::string& name )
{
	return new Wander( name );
}

//----------------------------------------------------------------------------------
bool Wander::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	bool thoughtResult = true;

	thinker->m_currentPath.clear();
	int x = 0;
	int y = 0;

	int numOfTries = 0;
	while( x == 0 && y == 0 && numOfTries < 20 )
	{
		x = rand() % 3 - 1;
		y = rand() % 3 - 1;

		IntVec2 position = IntVec2( x, y );
		int index = map->getCellIndexFromPosition( position );

		if( index != -1 && map->m_cells[index].howWalkableIsCellType(thinker) == 2 )
		{
			break;
		}

		++numOfTries;
	}

	x += thinker->m_position.x;
	y += thinker->m_position.y;

	thinker->m_currentPath.push_back( IntVec2(x, y) );
	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Wander::calculateImportance( Map* map, Agent* toThink)
{
	UNUSED( map );
	UNUSED( toThink );
	return 1.0f;
}

//----------------------------------------------------------------------------------
void Wander::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	if(name == "chanceToContinueStraight")
	{
		m_chanceToMoveStrait = std::stof(attributeAsString);
	}
	if(name == "chanceToRest")
	{
		m_chanceToRest = std::stof(attributeAsString);
	}
}