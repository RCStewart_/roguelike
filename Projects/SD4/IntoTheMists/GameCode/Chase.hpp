#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"
#include "Agent.hpp"

class Chase : public BaseAIBehavior
{
public:
	Chase( const std::string name );
	~Chase(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	Agent* topTarget;
protected:
	static AIBehaviorRegistration s_ChaseRegistration;
};

