#include "Messages.hpp"
#include "Engine/Font/FontGenerator.hpp"
#include "Engine/Core/RGBAColors.hpp"
#include "Player.hpp"
#include "TheGame.hpp"


Messages* g_messages = nullptr;

//----------------------------------------------------------------------------------
Messages::Messages(void) : m_messageIndex(0), m_referenceToPlayer(nullptr)
{

}

//----------------------------------------------------------------------------------
Messages::~Messages(void)
{

}

//----------------------------------------------------------------------------------
void Messages::addMessage( const std::string& messageToAdd, const IntVec2& locationOfEvent )
{
	if( m_referenceToPlayer == nullptr)
	{
		m_messages.push_back( messageToAdd );
	}
	else
	{
		std::set<int>::iterator it;
		for(it = m_referenceToPlayer->m_cellsCanSee.begin(); it != m_referenceToPlayer->m_cellsCanSee.end(); ++it)
		{
			IntVec2 pos = locationOfEvent;
			if( *it == g_theGame->m_map->getCellIndexFromPosition(pos))
			{
				m_messages.push_back( messageToAdd );
			}
		}
	}
}

//----------------------------------------------------------------------------------
bool Messages::hasMoreMessages()
{
	bool hasMore = false;
	if(m_messageIndex < (int) m_messages.size() - 1)
	{
		hasMore = true;
	}
	return hasMore;
}

//----------------------------------------------------------------------------------
void Messages::incrementMessage()
{
	if( m_messageIndex < (int) m_messages.size() - 1)
	{
		m_messages.pop_front();
	}
}

//----------------------------------------------------------------------------------
void Messages::clearMessageLog()
{
	m_messages.clear();
}

//----------------------------------------------------------------------------------
void Messages::displayMessage( FontGenerator* font, const Vec2& startingLocationTopLeft  )
{
	if( (int) m_messages.size() > 0)
	{
		std::string toDisplay = m_messages[m_messageIndex];
		if( m_messageIndex < (int) m_messages.size() - 1)
		{
			toDisplay += " -Space To Cont-";
		}
		RGBAColors textColor = RGBAColors( 255, 255, 255, 255);
		Vec2 startingLocation = startingLocationTopLeft;
		font->drawText( toDisplay, GENERATED_FONTS_BIT, 1.5f, startingLocation, textColor, false );
	}
}

