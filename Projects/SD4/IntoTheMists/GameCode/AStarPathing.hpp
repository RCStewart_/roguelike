#pragma once
#include "AStarNode.hpp"
#include <vector>
#include <algorithm>
#include "Engine/Math/IntVec2.hpp"
#include "Map.hpp"

class Agent;

class AStarPathing
{
public:
	AStarPathing(void);
	~AStarPathing(void);

	void generatePath( std::vector<IntVec2>& outPath, IntVec2& currentPos, IntVec2& finalLocation, Map* map, Agent* agentSearchingForAPath );
	void gatherAdjacentPositions( std::vector<IntVec2>& outAdjacengtPositions, Map* map, IntVec2& currentPosition, Agent* agentSearchingForAPath );
	int isIntVec2InVectorOfAStarNodes( IntVec2& toCheck, std::vector<AStarNode*>& container );
	float getCostCalculatedSoFarFromAStarNodeToGivenNeighbor( AStarNode* node, IntVec2& positionOfNeighbor, Agent* agentSearchingForAPath, Map* map );
	void fillPathFromFinalNode( std::vector<IntVec2>& outPath, AStarNode* goldenNode );
};

