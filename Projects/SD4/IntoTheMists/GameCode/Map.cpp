#include "Map.hpp"
#include "Engine\Renderer\GLWrapper.hpp"
#include "Engine\Math\Vec2.hpp"
#include "Engine\Math\Vec3.hpp"
#include "Engine\Math\IntVec2.hpp"
#include "Agent.hpp"
#include <algorithm>
#include "Player.hpp"
#include "Feature.hpp"
#include "FactionManager.hpp"

//----------------------------------------------------------------------------------
Map::Map():m_thePlayer(nullptr)
{
	initialize();
}

//----------------------------------------------------------------------------------
Map::~Map(void)
{
}

//Drawing
//----------------------------------------------------------------------------------
void Map::draw( FontGenerator* font, bool isLit )
{
	drawCells( font, isLit );
}


//TEMP
//----------------------------------------------------------------------------------
void Map::drawCells( FontGenerator* font, bool isLit )
{
	UNUSED(font);
	std::vector<Vertex2D_PCT> vertexes;

	Vec2 minCoords = Vec2(0.f, 0.f);
	Vec2 maxCoords = Vec2(1.f, 1.f);

	for( int i = 0; i < (int) m_cells.size(); ++i )
	{
		IntVec2 cell = getCellPositionFromIndex(i);

		Vertex2D_PCT southWest;
		Vertex2D_PCT southEast;
		Vertex2D_PCT northEast;
		Vertex2D_PCT northWest;

		RGBAColors cellColor = m_cells[i].getColorOfType();

		//RGBAColors cellColor = m_cells[i].getColorOfType();
		southWest.m_color = cellColor;
		southEast.m_color = cellColor;
		northEast.m_color = cellColor;
		northWest.m_color = cellColor;

		if(!isLit)
		{
			//----------------------------------------------------------------------------------
			//SW
			std::vector<IntVec2> toCheck;
			toCheck.push_back( IntVec2( cell.x, cell.y - 1) );
			toCheck.push_back( IntVec2( cell.x - 1, cell.y) );

			float percentLight = getLightingPercentFromNeighboringCells( i, toCheck );
			southWest.m_color = cellColor.getColorMultipliedByValue(percentLight);

			//SE
			toCheck.clear();
			toCheck.push_back( IntVec2( cell.x, cell.y - 1) );
			toCheck.push_back( IntVec2( cell.x + 1, cell.y) );

			percentLight = getLightingPercentFromNeighboringCells( i, toCheck );
			southEast.m_color = cellColor.getColorMultipliedByValue(percentLight);

			//NE
			toCheck.clear();
			toCheck.push_back( IntVec2( cell.x, cell.y + 1) );
			toCheck.push_back( IntVec2( cell.x + 1, cell.y) );

			percentLight = getLightingPercentFromNeighboringCells( i, toCheck );
			northEast.m_color = cellColor.getColorMultipliedByValue(percentLight);

			//NW
			toCheck.clear();
			toCheck.push_back( IntVec2( cell.x, cell.y + 1) );
			toCheck.push_back( IntVec2( cell.x - 1, cell.y) );

			percentLight = getLightingPercentFromNeighboringCells( i, toCheck );
			northWest.m_color = cellColor.getColorMultipliedByValue(percentLight);
		}

		southWest.m_texCoords = Vec2(minCoords.x, minCoords.y);
		southEast.m_texCoords = Vec2(minCoords.x, maxCoords.y);
		northEast.m_texCoords = Vec2(maxCoords.x, maxCoords.y);
		northWest.m_texCoords = Vec2(maxCoords.x, minCoords.y);

		Vec2 maxPos = Vec2(1.f, 1.f);
		Vec2 minPos = Vec2(0.f ,0.f);

		southWest.m_position.x = minPos.x + cell.x;
		southWest.m_position.y = minPos.y + cell.y;
		southWest.m_position.z = 0.f;

		southEast.m_position.x = maxPos.x + cell.x;
		southEast.m_position.y = minPos.y + cell.y;
		southEast.m_position.z = 0.f;

		northEast.m_position.x = maxPos.x + cell.x;
		northEast.m_position.y = maxPos.y + cell.y;
		northEast.m_position.z = 0.f;

		northWest.m_position.x = minPos.x + cell.x;
		northWest.m_position.y = maxPos.y + cell.y;
		northWest.m_position.z = 0.f;

		vertexes.push_back(southWest);
		vertexes.push_back(southEast);
		vertexes.push_back(northEast);
		vertexes.push_back(northWest);
	}

	m_translation = Vec3(0.f, 10.f, 0.f);
	GLWrapperVertexArray( vertexes, m_translation, GL_WRAPPER_QUADS, 0 );
}

//Updating
//----------------------------------------------------------------------------------
void Map::update()
{
	for(int cellIndex = 0; cellIndex < (int) m_cells.size(); ++cellIndex)
	{
		if(m_cells[cellIndex].m_occupant)
		{
			if(m_cells[cellIndex].m_currentCellType == CELL_TYPE_LAVA)
			{
				if(m_cells[cellIndex].howWalkableIsCellType(m_cells[cellIndex].m_occupant) < 2)
					--m_cells[cellIndex].m_occupant->m_currentHealth;
			}
		}
	}
}

//----------------------------------------------------------------------------------
float Map::getLightingPercentFromNeighboringCells( int cellIndex,  std::vector<IntVec2>& cellsToCheck )
{
	float calculatedLightPercent = m_cells[cellIndex].getPercentLit();
	int numberOfCellsWithRelaventLight = 1;
	
	if(m_cells[cellIndex].isWall() && m_cells[cellIndex].isOccupantVisible())
	{
		return 1.f;
	}
	
	if(m_cells[cellIndex].isOccupantVisible() && !m_cells[cellIndex].isWall())
	{
		for(size_t cellsToCheckIndex = 0; cellsToCheckIndex < cellsToCheck.size(); ++cellsToCheckIndex )
		{
			int indexInMap = getCellIndexFromPosition( cellsToCheck[cellsToCheckIndex] );
			if( indexInMap != -1)
			{
				if( m_cells[indexInMap].isOccupantVisible() )
				{
					++numberOfCellsWithRelaventLight;
					calculatedLightPercent += m_cells[indexInMap].m_percentLit;
				}
			}
		}
	}
	calculatedLightPercent = calculatedLightPercent/numberOfCellsWithRelaventLight;
	
	return calculatedLightPercent;
}

//----------------------------------------------------------------------------------
void Map::initialize()
{
	m_hasBeenGenerated = false;

	m_numOfCellsWide = (int) g_numOfTileUnitsX;
	m_numOfCellsHigh = (int) g_numOfTileUnitsY - 15;

	for( int i = 0; i < m_numOfCellsWide * m_numOfCellsHigh; ++i )
	{
		m_cells.push_back(Cell());
	}
}

//----------------------------------------------------------------------------------------------
void Map::makeThisPercentOfCellsAir( float percentToAir )
{
	float numberOfCellsToConvert = (float) m_cells.size() * percentToAir; 
	for(int i = 0; i < (int) m_cells.size(); ++i)
	{
		m_cells[i].m_timesOverWritten = 0;
	}
	for(int i = 0; i < (int) numberOfCellsToConvert; ++i)
	{
		m_cells[i].m_currentCellType = CELL_TYPE_AIR;
	}
	random_shuffle( m_cells.begin(), m_cells.end());
}

//----------------------------------------------------------------------------------
Agent* Map::getMostHatedEnemyNearby( int radius, IntVec2& location, Agent* heWhoHates )
{
	Agent* toStealFrom = nullptr;
	int midX = location.x;
	int midY = location.y;
	int currentLowest = 99999;
	for( int x = midX - radius; x <= midX + radius; ++x )
	{
		for( int y = midY - radius; y <= midY + radius; ++y )
		{
			IntVec2 checkLocation = IntVec2(x, y);
			int index = getCellIndexFromPosition( checkLocation );
			if( index != -1 )
			{
				if(m_cells[index].m_occupant)
				{
					int loyalty = heWhoHates->getLoyaltyForAgentBasedOnID( m_cells[index].m_occupant );
					if(loyalty < currentLowest)
					{
						loyalty = currentLowest;
						toStealFrom = m_cells[index].m_occupant;
					}
				}
			}
		}
	}
	return toStealFrom;
}

//----------------------------------------------------------------------------------
int Map::getNumOfHatedEnemiesNearby( int radius, IntVec2& location, Agent* heWhoHates )
{
	int midX = location.x;
	int midY = location.y;
	int count = 0;
	for( int x = midX - radius; x <= midX + radius; ++x )
	{
		for( int y = midY - radius; y <= midY + radius; ++y )
		{
			IntVec2 checkLocation = IntVec2(x, y);
			int index = getCellIndexFromPosition( checkLocation );
			if( index != -1 )
			{
				if(m_cells[index].m_occupant)
				{
					if(FactionManager::isEnemy( m_cells[index].m_occupant, heWhoHates))
						++count;
				}
			}
		}
	}
	return count;
}

//----------------------------------------------------------------------------------
int Map::getNumOfTypeAtLocationWithRadius( int radius, IntVec2& location, CellType type )
{
	int midX = location.x;
	int midY = location.y;
	int typeCount = 0;
	for( int x = midX - radius; x <= midX + radius; ++x )
	{
		for( int y = midY - radius; y <= midY + radius; ++y )
		{
			IntVec2 checkLocation = IntVec2(x, y);
			int index = getCellIndexFromPosition( checkLocation );
			if( index != -1 )
			{
				if(m_cells[index].m_currentCellType == type)
				{
					++typeCount;
				}
			}
		}
	}
	return typeCount;
}

//----------------------------------------------------------------------------------
void Map::toggleFeaturesInArea( int radius, IntVec2& location )
{
	int midX = location.x;
	int midY = location.y;
	for( int x = midX - radius; x <= midX + radius; ++x )
	{
		for( int y = midY - radius; y <= midY + radius; ++y )
		{
			IntVec2 checkLocation = IntVec2(x, y);
			int index = getCellIndexFromPosition( checkLocation );
			if( index != -1 )
			{
				if(m_cells[index].m_feature && !m_cells[index].m_occupant)
				{
					m_cells[index].m_feature->toggleActive();
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
void Map::makeAllCellsWalls( )
{
	for(int i = 0; i < (int) m_cells.size(); ++i)
	{
		m_cells[i].m_currentCellType = CELL_TYPE_WALL;
		m_cells[i].m_timesOverWritten = 0;
		m_cells[i].m_percentLit = 0;
	}
}

//----------------------------------------------------------------------------------
void Map::renderEntities( Map* map, FontGenerator* font )
{
	UNUSED(map);
	for(size_t i = 0; i < m_cells.size(); ++i)
	{
		if(m_cells[i].m_occupant)
		{
			//m_cells[i].m_occupant->render( map, font );
		}
		else if( m_cells[i].m_feature )
		{
			IntVec2 int_position = getCellPositionFromIndex( i );

			Vec2 position = Vec2( (float) int_position.x, (float) int_position.y );
			position.x += m_translation.x;
			position.y += m_translation.y;

			if( m_cells[i].isOccupantVisible() )
			{
				if( m_cells[i].m_feature )
				{
					if( m_cells[i].m_feature->m_isActive )
					{
						std::string on = "";
						on += m_cells[i].m_feature->m_glyphOn;
						font->drawText( on, GENERATED_FONTS_BIT, 1.f, position, m_cells[i].m_feature->m_color, false);
					}
					else
					{
						std::string off = "";
						off += m_cells[i].m_feature->m_glyphOff;
						font->drawText( off, GENERATED_FONTS_BIT, 1.f, position, m_cells[i].m_feature->m_color, false);
					}
				}
			}
		}
		else if( m_cells[i].m_items.size() > 0)
		{
			IntVec2 int_position = getCellPositionFromIndex( i );

			Vec2 position = Vec2( (float) int_position.x, (float) int_position.y );
			position.x += m_translation.x;
			position.y += m_translation.y;

			std::string glyph;
			glyph += m_cells[i].m_items[0]->m_glyph;
			//if( m_cells[i].isOccupantVisible() )
			{
			
				font->drawText( glyph, GENERATED_FONTS_BIT, 1.f, position, m_cells[i].m_items[0]->m_color, false);
				if(m_cells[i].m_items.size() > 1)
				{
					position.x += .4f;
					font->drawText( "*", GENERATED_FONTS_BIT, 1.f, position, m_cells[i].m_items[0]->m_color, false);
				}
			
			}
		}

	}
}

//----------------------------------------------------------------------------------
void Map::moveAgentToLocation( IntVec2& newLocation, Agent* movingAgent )
{
	int indexOfNewLocation = getCellIndexFromPosition( newLocation );
	int indexOfCurrentLocation = getCellIndexFromPosition( movingAgent->m_position );

	movingAgent->m_position = newLocation;

	m_cells[indexOfNewLocation].m_occupant = movingAgent;
	m_cells[indexOfCurrentLocation].m_occupant = nullptr;
}

//----------------------------------------------------------------------------------
void Map::makeCellsOnPathWater( std::vector<IntVec2>& path )
{
	for(size_t pathIndex = 0; pathIndex < path.size(); ++pathIndex)
	{
		int cellIndex = getCellIndexFromPosition( path[pathIndex] );
		m_cells[cellIndex].m_currentCellType = CELL_TYPE_SHALLOW_WATER;
	}
}

//----------------------------------------------------------------------------------
void Map::setAllCellsLighting( float percentLit )
{
	for(size_t mapIndex = 0; mapIndex < m_cells.size(); ++mapIndex )
	{
		m_cells[mapIndex].setPercentLit(percentLit);
	}
}

//----------------------------------------------------------------------------------
void Map::setAllCellsToLowerLighting( float percentLit )
{
	for(size_t mapIndex = 0; mapIndex < m_cells.size(); ++mapIndex )
	{
		m_cells[mapIndex].lowerPercentLightingToPercentLit(percentLit);
	}
}

//----------------------------------------------------------------------------------
void Map::clearMap()
{
	for( int mapIndex = 0; mapIndex < (int) m_cells.size(); ++mapIndex )
	{
		m_cells[mapIndex].m_occupant = nullptr;
		m_cells[mapIndex].m_feature = nullptr;
		m_cells[mapIndex].m_items.clear();
	}
}

//----------------------------------------------------------------------------------
void Map::makeAllCellsAir()
{
	for( int mapIndex = 0; mapIndex < (int) m_cells.size(); ++mapIndex )
	{
		m_cells[mapIndex].m_currentCellType = CELL_TYPE_AIR;
	}
}