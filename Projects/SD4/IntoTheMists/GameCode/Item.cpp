#include "Item.hpp"
#include "Map.hpp"
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"

//----------------------------------------------------------------------------------
Item::Item( std::string& name, char glyph, RGBAColors& color, ItemType itemType, int uses, const IntVec2& itemStrength, int bonusChance, Map* map, IntVec2& position )
	:GameEntity( map, position, name, glyph, color)
	,m_itemType(itemType)
	,m_uses(uses)
	,m_itemStrengthRange(itemStrength)
	,m_bonusChance(bonusChance)
{
}

//----------------------------------------------------------------------------------
Item::~Item(void)
{
}

//----------------------------------------------------------------------------------
ItemType Item::sGetItemTypeFromString( const std::string& itemTypeAsString )
{
	ItemType toReturn = ITEM_TYPE_BOOTS;
	if( itemTypeAsString == "shield" )
	{
		toReturn = ITEM_TYPE_SHIELD;
	}
	if( itemTypeAsString == "weapon" )
	{
		toReturn = ITEM_TYPE_WEAPON;
	}
	if( itemTypeAsString == "helmet" )
	{
		toReturn = ITEM_TYPE_HELM;
	}
	if( itemTypeAsString == "chest" )
	{
		toReturn = ITEM_TYPE_CHEST;
	}
	if( itemTypeAsString == "pants" )
	{
		toReturn = ITEM_TYPE_PANTS;
	}
	if( itemTypeAsString == "boots" )
	{
		toReturn = ITEM_TYPE_BOOTS;
	}
	if( itemTypeAsString == "torch" )
	{
		toReturn = ITEM_TYPE_LIGHT;
	}
	return toReturn;
}

//----------------------------------------------------------------------------------
void Item::saveToXML( TiXmlElement* parent )
{
	TiXmlElement* item = new TiXmlElement( "item" ); 
	GameEntity::saveToXML( item );
	parent->LinkEndChild( item );
}