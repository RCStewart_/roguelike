#include "Player.hpp"
#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Font\FontGenerator.hpp"
#include <cmath>
#include "CombatSystem.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Messages.hpp"

//----------------------------------------------------------------------------------
Player::Player( Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color )
	: Agent(map, position, name, glyph, color)
	, m_nickName("The Hero")
	, m_movementVector(IntVec2( 0, 0 ))
	, m_lineOfSight(7)
{
	m_isPlayer = true;
	m_maxHealth = 500;
	m_currentHealth = m_maxHealth;

	m_armorClass = 20;
	m_reflectiveDamageRange = IntVec2( 0, 0 );

	m_damageRange = IntVec2( 1, 5 );
	m_baseChanceToHit = IntVec2( 6, 10 );
	m_bonusChanceToHit = 0;
	m_faction = "human";

	m_armorClassRunBonus = 15;
}

//----------------------------------------------------------------------------------
Player::~Player(void)
{

}

//----------------------------------------------------------------------------------
void Player::update( Map* map, double deltaSeconds )
{
	UNUSED( deltaSeconds );
	
	IntVec2 locationToCheck = IntVec2(m_movementVector.x + m_position.x, m_movementVector.y + m_position.y);
	MoveResults canMove = map->isValidLocation(locationToCheck, this);
	if(canMove.canMoveToLocation)
	{
		//Check if there are any items at the location
		if(!(locationToCheck == m_position) )
		{
			m_isRunning = true;

			int cellIndex = map->getCellIndexFromPosition(locationToCheck);
			if(map->m_cells[cellIndex].m_items.size() > 0)
			{
				Cell& currentCell = map->m_cells[cellIndex];
				for( size_t itemIndex = 0; itemIndex < currentCell.m_items.size(); ++itemIndex )
				{
					std::string itemOnCell;
					if(itemIndex == 0)
					{
						itemOnCell = "Press ',' to pick up ";
						itemOnCell += currentCell.m_items[itemIndex]->m_name;
					}
					else
					{
						itemOnCell = "You found a ";
						itemOnCell += currentCell.m_items[itemIndex]->m_name;
					}
					g_messages->addMessage( itemOnCell, locationToCheck );
				}
			}
		}

		map->moveAgentToLocation( locationToCheck, this );
	}
	else if( canMove.agentOnCell && !(locationToCheck == m_position) )
	{
		AttackData dataToSend;
		getOffense( dataToSend );
		ResultOfAttack results;
		CombatSystem::resolveCombat( this, canMove.agentOnCell, dataToSend, results );
	}

	float xStep = 1.0f;
	float yStep = 1.0f;
	TraceResult collectionOfCells;

	m_cellsCanSee.clear();
	for(int i = 0; i < 360; ++i )
	{
		rayTrace(collectionOfCells, (float) m_lineOfSight + (float) m_lastRecordedExtraLight, m_position, (float) cos( (float) i) * xStep, (float) sin( (float) i) * yStep, map);
	}
	float oneOverLineOfSight = 1.f/(m_lineOfSight + (float) m_lastRecordedExtraLight);
	std::set<int>::iterator it;
	for (it = collectionOfCells.visibleCells.begin(); it != collectionOfCells.visibleCells.end(); ++it)
	{
		IntVec2 cellPosition = map->getCellPositionFromIndex(*it);
		float xDisplacement = (float) abs( cellPosition.x - m_position.x );
		float yDisplacement = (float) abs( cellPosition.y - m_position.y );
		Vec2 lengthVector = Vec2( xDisplacement, yDisplacement );
		float magnetude = lengthVector.getMagnetude();
		map->m_cells[*it].raisePercentLightingToPercentLit( min(1.0f - magnetude * oneOverLineOfSight + .10f, 1.0f));
	}

	m_cellsCanSee = collectionOfCells.visibleCells;
}

//----------------------------------------------------------------------------------
void Player::updateLightOnly( Map* map, double deltaSeconds )
{
	UNUSED(deltaSeconds);
	float xStep = 1.0f;
	float yStep = 1.0f;
	TraceResult collectionOfCells;
	for(int i = 0; i < 360; ++i )
	{
		rayTrace(collectionOfCells, (float) m_lineOfSight + (float) m_lastRecordedExtraLight, m_position, (float) cos( (float) i) * xStep, (float) sin( (float) i) * yStep, map);
	}
	float oneOverLineOfSight = 1.f/(m_lineOfSight + (float) m_lastRecordedExtraLight);
	std::set<int>::iterator it;
	for (it = collectionOfCells.visibleCells.begin(); it != collectionOfCells.visibleCells.end(); ++it)
	{
		IntVec2 cellPosition = map->getCellPositionFromIndex(*it);
		float xDisplacement = (float) abs( cellPosition.x - m_position.x );
		float yDisplacement = (float) abs( cellPosition.y - m_position.y );
		Vec2 lengthVector = Vec2( xDisplacement, yDisplacement );
		float magnetude = lengthVector.getMagnetude();
		map->m_cells[*it].raisePercentLightingToPercentLit( min(1.0f - magnetude * oneOverLineOfSight + .10f, 1.0f));
	}
}

//----------------------------------------------------------------------------------
bool Player::processInput( Map* map )
{
	bool hasMoved = false;
	m_movementVector = IntVec2(0, 0);
	//left
	if(WindowWrapperGetIsInputActiveAndConsume('H'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(-1, 0);
	}

	//down
	if(WindowWrapperGetIsInputActiveAndConsume('J'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(0, -1);
	}

	//up
	if(WindowWrapperGetIsInputActiveAndConsume('K'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(0, 1);
	}

	//right
	if(WindowWrapperGetIsInputActiveAndConsume('L'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(1, 0);
	}

	//NW
	if(WindowWrapperGetIsInputActiveAndConsume('Y'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(-1, 1);
	}

	//NE
	if(WindowWrapperGetIsInputActiveAndConsume('U'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(1, 1);
	}

	//SW
	if(WindowWrapperGetIsInputActiveAndConsume('B'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(-1, -1);
	}

	//SE
	if(WindowWrapperGetIsInputActiveAndConsume('N'))
	{
		hasMoved = true;
		m_movementVector = IntVec2(1, -1);
	}

	if(hasMoved)
	{
		m_isRunning = false;
		g_messages->clearMessageLog();
		++m_numOfTurnsTaken;
	}

	//Pick up an Item
	if(WindowWrapperGetIsInputActiveAndConsume(','))
	{
		int index = map->getCellIndexFromPosition( m_position );
		map->m_cells[index].addTopItemToAgent( this );
	}

	//Toggle nearby features
	if(WindowWrapperGetIsInputActiveAndConsume('O'))
	{
		map->toggleFeaturesInArea( 1, m_position );
	}


	return hasMoved;
}

//----------------------------------------------------------------------------------
void Player::render( Map* map, FontGenerator* font )
{
	UNUSED( map );
	Vec2 position = Vec2(m_map->m_translation.x + m_position.x, m_map->m_translation.y + m_position.y);
	string glyph;
	glyph += m_glyph;
	font->drawText( glyph, GENERATED_FONTS_BIT, 1.f, position, m_color, false);
}