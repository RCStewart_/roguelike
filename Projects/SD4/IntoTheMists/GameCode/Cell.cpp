#include "Cell.hpp"
#include "Agent.hpp"

float Cell::s_basePercentLight(.15f);

//----------------------------------------------------------------------------------
Cell::Cell(void)
	:m_occupant(nullptr)
	,m_percentLit(0.f)
	,m_feature(nullptr)
{
	m_currentCellType = CELL_TYPE_WALL;
	m_timesOverWritten = 0;
}

//----------------------------------------------------------------------------------
Cell::~Cell(void)
{

}

//----------------------------------------------------------------------------------
void Cell::setPercentLit( float percentLit )
{
	m_percentLit = percentLit;
}

//----------------------------------------------------------------------------------
void Cell::lowerPercentLightingToPercentLit( float percentLit )
{
	if(percentLit < m_percentLit)
	{	
		m_percentLit = percentLit;
	}
}

//----------------------------------------------------------------------------------
void Cell::raisePercentLightingToPercentLit( float percentLit )
{
	if(percentLit > m_percentLit)
	{	
		m_percentLit = percentLit;
	}
}

//----------------------------------------------------------------------------------
float Cell::getPercentLit()
{
	return m_percentLit;
}

//----------------------------------------------------------------------------------
void Cell::addTopItemToAgent( Agent* heWhoPicksUpItem )
{
	if( m_items.size() > 0)
	{
		m_items[0]->m_position = IntVec2(-1, -1);
		heWhoPicksUpItem->addItemToInventoryOrEquip( m_items[0] );
		m_items.pop_front();
	}
}

//----------------------------------------------------------------------------------
void Cell::addItemToList( Item* itemToAdd )
{
	m_items.push_back( itemToAdd );
}

//----------------------------------------------------------------------------------
void Cell::setTypeWithCharGlyph(char newType)
{
	switch(newType)
	{
	case '.':
		m_currentCellType = CELL_TYPE_AIR;
		break;
	case '8':
		m_currentCellType = CELL_TYPE_WALL;
		break;
	case 'h':
		m_currentCellType = CELL_TYPE_HALL;
		break;
	case '#':
		m_currentCellType = CELL_TYPE_BUILDING_WALL;
		break;
	case 'f':
		m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
		break;
	case 'w':
		m_currentCellType = CELL_TYPE_SHALLOW_WATER;
		break;
	case 'l':
		m_currentCellType = CELL_TYPE_LAVA;
		break;
	}
}

//----------------------------------------------------------------------------------
char Cell::getGlyph()
{
	char toReturn = '?';

	switch(m_currentCellType)
	{
	case CELL_TYPE_AIR:
		toReturn = '.';
		break;
	case CELL_TYPE_WALL:
		toReturn = '8';
		break;
	case CELL_TYPE_HALL:
		toReturn = 'h';
		break;
	case CELL_TYPE_BUILDING_WALL:
		toReturn = '#';
		break;
	case CELL_TYPE_BUILDING_FLOOR:
		toReturn = 'f';
		break;
	case CELL_TYPE_SHALLOW_WATER:
		toReturn = 'w';
		break;
	case CELL_TYPE_LAVA:
		toReturn = 'l';
		break;
	}

	return toReturn;
}

//----------------------------------------------------------------------------------
int Cell::howWalkableIsCellType( Agent* heWhoIsWalking )
{
	int howWalkable = 0;
	howWalkable = heWhoIsWalking->movementSpeedOnType( m_currentCellType );
	if(m_feature)
	{
		if(m_feature->isAWall())
			howWalkable = 0;
	}
	return howWalkable;
}

//----------------------------------------------------------------------------------
int Cell::gValueOfCellTypeForAgent( Agent* heWhoIsWalking )
{
	int gValue = 999999;
	gValue = heWhoIsWalking->gValueOnType( m_currentCellType );
	if(m_feature)
	{
		if(m_feature->isAWall())
			gValue = 999999;
	}
	return gValue;
}