#pragma once

#include "GameEntity.hpp"
#include <string>
#include "Engine/Math/IntVec2.hpp"
#include <vector>
#include <set>
#include <map>
#include "FactionManager.hpp"

enum CellType;
struct AttackData;
struct DefendData;
class Item;
class Map;
class Font;
class TiXmlElement;

#define NameID int
#define LoyaltyDisplacement int

struct TraceResult
{
	std::set<int> visibleCells;
};

class Agent: public GameEntity
{
public:
	Agent(Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color);
	~Agent(void);

	void render( Map* map, FontGenerator* font );
	void rayTrace( TraceResult& result, float distanceToTravel, IntVec2& position, float xStepSize, float yStepSize, Map* map );
	void addItemToInventoryOrEquip( Item* toAdd );
	bool tryToEquipItem( Item* toAdd );
	int movementSpeedOnType( CellType toCheck );
	int gValueOnType( CellType toCheck );
	bool hasBit( unsigned char bitToCheck, unsigned char numThatMayContainBit );

	void getOffense( AttackData& outAttackData );
	void getDefense( const AttackData& inAttackData, DefendData& outDefendData );
	void updateLoyaltyForAgent( Agent* otherAgent, FactionAction whatHeDid );
	void updateLastRecordedExtraLighting();
	int getLoyaltyForAgentBasedOnID( Agent* otherAgent );
	Item* getAndDisEquipEquippedItem();

	virtual void saveToXML( TiXmlElement* parentNode );
	virtual void loadFromXML( TiXmlElement* gameEntityNode );
	virtual void resolvePointers( std::map<int, GameEntity*>& oldIDsToEntityMap );

	std::string m_faction;
	bool m__isInMap;
	int m_speed;
	std::vector<IntVec2> m_currentPath;

	bool m_isPlayer;
	int m_numOfTurnsTaken;

	std::vector<Item*> m_inventory;

	std::set<int> m_cellsCanSee;

	Item* m_weapon;
	Item* m_shield;
	Item* m_helm;
	Item* m_pants;
	Item* m_boots;
	Item* m_chest;

	int m_lineOfSight;

	bool m_isRunning;
	int  m_armorClassRunBonus;

	std::map<NameID, LoyaltyDisplacement> memory;
	std::map< int, int > tempMemoryForLoading;

	int m_lastRecordedExtraLight;

	unsigned char m_movementBits;
};

