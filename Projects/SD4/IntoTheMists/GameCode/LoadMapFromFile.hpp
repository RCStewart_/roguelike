#pragma once
#include "BaseMapGenerator.hpp"
#include <string>

class LoadMapFromFile: public BaseMapGenerator
{
public:
	LoadMapFromFile( const std::string& name );
	~LoadMapFromFile(void);

	static BaseMapGenerator* createGenerator( const std::string& name );

	virtual void generateMap( Map* map );
	virtual void processOneStep( Map* map );
	virtual void processSeveralSteps( Map* map );

	int loadMapInformationFromXML( std::string& xmlFileName, Map* map );

protected:
	static MapGeneratorRegistration s_loadMapRegistration;
};