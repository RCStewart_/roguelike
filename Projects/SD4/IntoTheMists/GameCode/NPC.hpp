#pragma once
#include "Agent.hpp"
#include "Engine/Core/RGBAColors.hpp"
#include <vector>
#include "BaseAIBehavior.hpp"
#include <map>

class NPC : public Agent
{
public:
	NPC(Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color );
	~NPC(void);

	void update( Map* map, double deltaSeconds );

	std::vector<BaseAIBehavior*> m_personality;
};

