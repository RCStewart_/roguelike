#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"

class DrainLife : public BaseAIBehavior
{
public:
	DrainLife( const std::string name );
	~DrainLife(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	float		minDrain;
	float		maxDrain;
	Agent*		topTarget;

protected:
	static AIBehaviorRegistration s_drainLifeRegistration;
};