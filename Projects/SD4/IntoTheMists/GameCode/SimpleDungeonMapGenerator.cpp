#include "SimpleDungeonMapGenerator.hpp"
#include "Map.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include "Room.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Engine/Core/GameCommon.hpp"
#include <algorithm>

MapGeneratorRegistration SimpleDungeonMapGenerator::s_simpleDungeonRegistration( "Themed Dungeon with Lava", &SimpleDungeonMapGenerator::createGenerator );

//----------------------------------------------------------------------------------
SimpleDungeonMapGenerator::SimpleDungeonMapGenerator( const std::string& name ) : BaseMapGenerator( "Simple Dungeon" )
{
	UNUSED( name );
}

//----------------------------------------------------------------------------------
SimpleDungeonMapGenerator::~SimpleDungeonMapGenerator(void)
{
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::generateMap( Map* map)
{
	map->makeAllCellsWalls();
	int x = (int) (map->m_numOfCellsWide * .5);
	int y = (int) (map->m_numOfCellsHigh * .5);
	int xDisplacement = (int) RandomNumber::getRandomFloatInRange( 2.f, 4.f );
	int yDisplacement = (int) RandomNumber::getRandomFloatInRange( 2.f, 4.f );
	Vec2 center = Vec2((float) x, (float) y);
	Room centerRoom( xDisplacement, yDisplacement, center );
	makeAir( centerRoom, map );
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::makeAir( const Room& room, Map* map )
{
	for(int x = (int) room.m_center.x - room.m_cellDisplacementFromCenterX; x < (int) room.m_center.x + room.m_cellDisplacementFromCenterX; ++x )
	{
		for(int y = (int) room.m_center.y - room.m_cellDisplacementFromCenterY; y < (int) room.m_center.y + room.m_cellDisplacementFromCenterY; ++y)
		{
			IntVec2 pos(x, y);
			int index = map->getCellIndexFromPosition(pos);
			if( index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide && y >= 0 && y < map->m_numOfCellsHigh )
			{
				map->m_cells[index].m_currentCellType = CELL_TYPE_AIR;
				++map->m_cells[index].m_timesOverWritten;
			}
		}
	}
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::processSeveralSteps( Map* map )
{
	int numOfSteps = 300;
	for(int i = 0; i < numOfSteps; ++i )
	{
		processOneStep( map );
	}
	addLava( map );
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::processOneStep( Map* map )
{
	int numberOfTries = 5000;
	for(int i = 0; i < numberOfTries; ++i)
	{
		int index = rand() % map->m_cells.size();
		Vec2 result = checkIfIValidLocationForNewHall(index, map);
		if(result == Vec2(0.f, 0.f))
		{
			continue;
		}
		Vec2 moveDirection = result;

		//Direction of the empty space is opposite of the direction of movement
		moveDirection.scalarMultiplication( -1.f );
		Vec2 roomLocation = placeHallway(moveDirection, index, map);

		int xDisplacement = (int) RandomNumber::getRandomFloatInRange( 1.f, 5.f );
		int yDisplacement = (int) RandomNumber::getRandomFloatInRange( 1.f, 5.f );
		Room centerRoom( xDisplacement, yDisplacement, roomLocation );
		//placeRoom(moveDirection, index, map, centerRoom);
		placeRandomRoom(moveDirection, index, map, centerRoom);
		return;
	}
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::placeRandomRoom( Vec2& direction, int index, Map* map, const Room& room )
{
	int numOfTries = 1000;
	for(int i = 0; i < numOfTries; ++i)
	{
		Vec2 newLocation( (const float) (rand() % map->m_numOfCellsWide), (const float) (rand() % map->m_numOfCellsHigh) );
		Room newRoom = Room( (int) room.m_center.x, (int) room.m_center.y, newLocation);
		Vec2 finalLocation(placeRoom(direction, index, map, room));
		if(!(finalLocation == room.m_center))
			return;
	}
}

//----------------------------------------------------------------------------------
Vec2 SimpleDungeonMapGenerator::placeHallway( Vec2& direction, int index, Map* map )
{
	Vec2 endingLocation = Vec2(0,0);
	int numOfSteps = rand() & 4 + 3;
	IntVec2 startingLocation = map->getCellPositionFromIndex(index);

	int checkRangeFromCenter = 2;
	int numOfHallsFound = 0;
	for(int y = startingLocation.y - checkRangeFromCenter; y < startingLocation.y + checkRangeFromCenter; ++y)
	{
		for(int x = startingLocation.x - checkRangeFromCenter; x < startingLocation.x + checkRangeFromCenter; ++x)
		{
			IntVec2 currentLocation = IntVec2( x, y );
			int index = map->getCellIndexFromPosition(currentLocation);
			if(index >= 0 && index < (int) map->m_cells.size() && y >= 0 && y < map->m_numOfCellsHigh && x >= 0 && x < map->m_numOfCellsWide)
			{
				if( map->m_cells[index].m_currentCellType == CELL_TYPE_HALL )
				{
					++numOfHallsFound;
				}
			}
		}
	}
	if(numOfHallsFound > 4)
	{
		return Vec2(0.f ,0.f);
	}

	if( direction == g_south )
	{
		for(int y = startingLocation.y; y >= startingLocation.y - numOfSteps; --y)
		{
			IntVec2 currentLocation = IntVec2( (int) startingLocation.x, y );
			int index = map->getCellIndexFromPosition(currentLocation);
			if(index >= 0 && index < (int) map->m_cells.size() && y >= 0 && y < map->m_numOfCellsHigh)
			{
				if( map->m_cells[index].m_currentCellType == CELL_TYPE_HALL )
				{
					return Vec2( (const float) startingLocation.x, (const float) y);
				}
				map->m_cells[index].m_currentCellType = CELL_TYPE_HALL;
			}
		}
		endingLocation = Vec2((const float) startingLocation.x, (const float) (startingLocation.y - numOfSteps));
		return endingLocation;
	}
	if( direction == g_north )
	{
		for(int y = startingLocation.y; y < startingLocation.y + numOfSteps; ++y)
		{
			IntVec2 currentLocation = IntVec2( (int) startingLocation.x, y );
			int index = map->getCellIndexFromPosition(currentLocation);
			if(index >= 0 && index < (int) map->m_cells.size() && y >= 0 && y < map->m_numOfCellsHigh)
			{
				if( map->m_cells[index].m_currentCellType == CELL_TYPE_HALL )
				{
					return Vec2( (const float) startingLocation.x, (const float) y);
				}
				map->m_cells[index].m_currentCellType = CELL_TYPE_HALL;
			}
		}
		endingLocation = Vec2((const float) startingLocation.x, (const float) (startingLocation.y + numOfSteps));
		return endingLocation;
	}
	if( direction == g_west )
	{
		for(int x = startingLocation.x; x >= startingLocation.x - numOfSteps; --x)
		{
			IntVec2 currentLocation = IntVec2( x, (int) startingLocation.y );
			int index = map->getCellIndexFromPosition(currentLocation);
			if(index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide)
			{
				if( map->m_cells[index].m_currentCellType == CELL_TYPE_HALL )
				{
					return Vec2( (const float) x, (const float) startingLocation.y);
				}
				map->m_cells[index].m_currentCellType = CELL_TYPE_HALL;
			}
		}
		endingLocation = Vec2((const float) (startingLocation.x - numOfSteps), (const float) startingLocation.y);
		return endingLocation;
	}
	if( direction == g_east )
	{
		for(int x = startingLocation.x; x < startingLocation.x + numOfSteps; ++x)
		{
			IntVec2 currentLocation = IntVec2( x, (int) startingLocation.y );
			int index = map->getCellIndexFromPosition(currentLocation);
			if(index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide)
			{
				if( map->m_cells[index].m_currentCellType == CELL_TYPE_HALL )
				{
					return Vec2( (const float) x, (const float) startingLocation.y);
				}
				map->m_cells[index].m_currentCellType = CELL_TYPE_HALL;
			}
		}
		endingLocation = Vec2( (const float)(startingLocation.x + numOfSteps), (const float) startingLocation.y );
		return endingLocation;
	}
	return endingLocation;
}


//----------------------------------------------------------------------------------
Vec2 SimpleDungeonMapGenerator::placeRoom( Vec2& direction, int index, Map* map, const Room& room )
{
	CellType toAdd = CELL_TYPE_AIR;

	if(rand() % 5 < 2 )
	{
		toAdd = CELL_TYPE_BUILDING_FLOOR;
	}

	UNUSED(index);
	UNUSED(direction);
	bool isValidLocation = true;
	int numOfClearCellsFound = 0;
	for(int x = (int) room.m_center.x - room.m_cellDisplacementFromCenterX - 1; x < (int) room.m_center.x + room.m_cellDisplacementFromCenterX + 1; ++x )
	{
		for(int y = (int) room.m_center.y - room.m_cellDisplacementFromCenterY - 1; y < (int) room.m_center.y + room.m_cellDisplacementFromCenterY + 1; ++y)
		{
			IntVec2 pos(x, y);
			int index = map->getCellIndexFromPosition(pos);
			if( index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide && y >= 0 && y < map->m_numOfCellsHigh )
			{
				if((map->m_cells[index].m_currentCellType == CELL_TYPE_AIR || map->m_cells[index].m_currentCellType == CELL_TYPE_BUILDING_FLOOR))
					++numOfClearCellsFound;
					//isValidLocation = false;
			}
		}
	}
	if(numOfClearCellsFound > 15)
	{
		isValidLocation = false;
	}
	if(isValidLocation)
	{
		for(int x = (int) room.m_center.x - room.m_cellDisplacementFromCenterX; x < (int) room.m_center.x + room.m_cellDisplacementFromCenterX; ++x )
		{
			for(int y = (int) room.m_center.y - room.m_cellDisplacementFromCenterY; y < (int) room.m_center.y + room.m_cellDisplacementFromCenterY; ++y)
			{
				IntVec2 pos(x, y);
				int index = map->getCellIndexFromPosition(pos);
				if( index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide && y >= 0 && y < map->m_numOfCellsHigh )
				{
					map->m_cells[index].m_currentCellType = toAdd;
					++map->m_cells[index].m_timesOverWritten;
				}
			}
		}
	}
	return Vec2(0, 0);
}


//----------------------------------------------------------------------------------
Vec2 SimpleDungeonMapGenerator::checkIfIValidLocationForNewHall( int index, Map* map )
{
	Vec2 locationOfAirInRelationToIndex = Vec2(0.f, 0.f);
	int numOfNearbyEmptySpace = 0;

	IntVec2 location = map->getCellPositionFromIndex(index);
	int displacementFromCenter = 2;

	for( int x = location.x - displacementFromCenter; x <= location.x + displacementFromCenter; ++x )
	{
		for( int y = location.y - displacementFromCenter; y <= location.y + displacementFromCenter; ++y)
		{
			IntVec2 pos(x, y);
			int innerLoopIndex = map->getCellIndexFromPosition( pos );
			if( map->isValidX(x, innerLoopIndex) && map->isValidY(y, innerLoopIndex))
			{
				CellType type = map->m_cells[innerLoopIndex].m_currentCellType;
				if(innerLoopIndex >= 0 && innerLoopIndex < (int) map->m_cells.size())
				{
					if(type == CELL_TYPE_AIR || type == CELL_TYPE_HALL)
					{
						++numOfNearbyEmptySpace;
					}
				}
			}
		}
	}

	std::vector<Vec2> directionsCanGo;
	if( (location.x + 1) < (map->m_numOfCellsWide) )
	{
		IntVec2 oneStepEastCoords(location.x, location.y - 1);
		int innerLoopIndex = map->getCellIndexFromPosition( oneStepEastCoords );
		if(innerLoopIndex >= 0 && innerLoopIndex < (int) map->m_cells.size())
		{
			CellType type = map->m_cells[innerLoopIndex].m_currentCellType;
			if(type == CELL_TYPE_AIR )
			{
				directionsCanGo.push_back(g_east);
			}
		}
	}
	if( (location.x - 1) >= 0 )
	{
		IntVec2 oneStepWestCoords(location.x, location.y - 1);
		int innerLoopIndex = map->getCellIndexFromPosition( oneStepWestCoords );
		if(innerLoopIndex >= 0 && innerLoopIndex < (int) map->m_cells.size())
		{
			CellType type = map->m_cells[innerLoopIndex].m_currentCellType;
			if(map->m_cells[innerLoopIndex].isWalkable())//(type == CELL_TYPE_AIR )
			{
				directionsCanGo.push_back(g_west);
			}
		}
	}
	if( (location.y + 1) < (map->m_numOfCellsHigh) )
	{
		IntVec2 oneStepNorthCoords(location.x, location.y + 1);
		int innerLoopIndex = map->getCellIndexFromPosition( oneStepNorthCoords );
		if(innerLoopIndex >= 0 && innerLoopIndex < (int) map->m_cells.size())
		{
			CellType type = map->m_cells[innerLoopIndex].m_currentCellType;
			if(map->m_cells[innerLoopIndex].isWalkable() )
			{
				directionsCanGo.push_back(g_north);
			}
		}
	}
	if( (location.y - 1) >= 0 )
	{
		IntVec2 oneStepSouthCoords(location.x, location.y - 1);
		int innerLoopIndex = map->getCellIndexFromPosition( oneStepSouthCoords );
		if(innerLoopIndex >= 0 && innerLoopIndex < (int) map->m_cells.size())
		{
			CellType type = map->m_cells[innerLoopIndex].m_currentCellType;
			if(map->m_cells[innerLoopIndex].isWalkable() )
			{
				directionsCanGo.push_back(g_south);
			}
		}
	}

	if( directionsCanGo.size() > 0 )
	{
		 std::random_shuffle( directionsCanGo.begin(), directionsCanGo.end() );
		 locationOfAirInRelationToIndex = directionsCanGo[0];
	}

	if( numOfNearbyEmptySpace > 6)
	{
		locationOfAirInRelationToIndex = Vec2(0.f, 0.f);
	}

	return locationOfAirInRelationToIndex;
}

//----------------------------------------------------------------------------------
BaseMapGenerator* SimpleDungeonMapGenerator::createGenerator( const std::string& name )
{
	return new SimpleDungeonMapGenerator( name );
}

//----------------------------------------------------------------------------------
void SimpleDungeonMapGenerator::addLava( Map* map )
{
	//Find a valid starting location
	int numOfTriesToSearchForStartLeft = 500;
	while( numOfTriesToSearchForStartLeft >= 0 )
	{
		int x = rand() % map->m_numOfCellsWide;
		int y = rand() % map->m_numOfCellsHigh;

		IntVec2 location = IntVec2( x, y );
		int index = map->getCellIndexFromPosition( location );

		//if( !map->m_cells[index].isBuilding() )
		{
			map->m_cells[index].m_currentCellType = CELL_TYPE_LAVA;
			numOfTriesToSearchForStartLeft = -1;
		}
		--numOfTriesToSearchForStartLeft;
	}

	//Start generating river's shape
	int numOfLavaTilesToAdd = 100;
	for( int numOfTilesAddedSoFar = 0; numOfTilesAddedSoFar < numOfLavaTilesToAdd; ++numOfTilesAddedSoFar )
	{
		int numOfTriesToPlaceTile = 5000;
		while( numOfTriesToPlaceTile >= 0 )
		{
			int x = rand() % map->m_numOfCellsWide;
			int y = rand() % map->m_numOfCellsHigh;

			IntVec2 location = IntVec2( x, y );
			int index = map->getCellIndexFromPosition( location );

			if( isValidLocationForLavaTile(map, location, index) )
			{
				map->m_cells[index].m_currentCellType = CELL_TYPE_LAVA;
				numOfTriesToPlaceTile = -1;
			}
			--numOfTriesToPlaceTile;
		}
	}
}

//----------------------------------------------------------------------------------
bool SimpleDungeonMapGenerator::isValidLocationForLavaTile( Map* map, IntVec2& locationToCheck, int index )
{
	bool isValidLocation = false;
	if( map->m_cells[index].m_currentCellType == CELL_TYPE_LAVA /*|| map->m_cells[index].isBuilding()*/)
	{
		return false;
	}

	int numOfNeighboringLavas =  map->getNumOfTypeAtLocationWithRadius( 1, locationToCheck, CELL_TYPE_LAVA );

	int northIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x, locationToCheck.y + 1 ) );
	if( northIndex >= 0 )
	{
		if( map->m_cells[northIndex].m_currentCellType == CELL_TYPE_LAVA && numOfNeighboringLavas <= 2 )
		{
			return true;
		}
	}
	int southIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x, locationToCheck.y - 1 ) );
	if( southIndex >= 0 )
	{
		if( map->m_cells[southIndex].m_currentCellType == CELL_TYPE_LAVA && numOfNeighboringLavas <= 2 )
		{
			return true;
		}
	}

	int westIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x - 1, locationToCheck.y ) );
	if( westIndex >= 0 )
	{
		if( map->m_cells[westIndex].m_currentCellType == CELL_TYPE_LAVA && numOfNeighboringLavas <= 2 )
		{
			return true;
		}
	}
	int eastIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x + 1, locationToCheck.y ) );
	if( eastIndex >= 0 )
	{
		if( map->m_cells[eastIndex].m_currentCellType == CELL_TYPE_LAVA && numOfNeighboringLavas <= 2 )
		{
			return true;
		}
	}
	//if( map->getNumOfTypeAtLocationWithRadius( 1, locationToCheck, CELL_TYPE_LAVA ) >= 3)
	//{
	//	return true;
	//}

	return isValidLocation;
}