#pragma once
#include <map>
#include <string>
#include <vector>

#define PersonalFaction int
#define OtherFaction int
#define HowMuchPersonalFactionLikesOtherFaction int

class Agent;

enum ActionPerformed
{
	ACTION_PERFORMED_HURT, 
	ACTION_PERFORMED_KILLED
};

enum FactionAction
{
	FA_HURT_SELF = -2,
	FA_HURT_FRIEND = -1,
	FA_KILLED_FRIEND = -5,
	FA_HURT_ENEMY = 2,
	FA_KILLED_ENEMY = 5,
	FA_DOESNT_MATTER = 0
};

enum PerceptionBasedOnLoyalty
{
	ALLY = 3,
	ENEMY = -3
};

class FactionManager
{
public:
	FactionManager(void);
	~FactionManager(void);

	static void whatDoesThisActionMeanToMe(  Agent* actor, Agent* recieving, Agent* Me, ActionPerformed action );
	static bool isAlly( Agent* otherActor, Agent* Me );
	static bool isEnemy( Agent* otherActor, Agent* Me );
	static void updateEveryNPCsLoyalty( Agent* actor, Agent* recieving, ActionPerformed action );
	static void setBaseFactionLoyalty();
	static int getFactionFromName( const std::string& name );
	//map< your faction, map< their faction, your view of that faction>>
	static std::map< PersonalFaction, std::map< OtherFaction, HowMuchPersonalFactionLikesOtherFaction>> s_factionLoyaltyMap;

};

