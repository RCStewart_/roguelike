#pragma once
#include <string>
#include "Agent.hpp"
#include "Map.hpp"

class BaseAIBehavior
{
public:

	BaseAIBehavior();
	BaseAIBehavior(std::string name)
		:/*m_agent(nullptr),*/ m_name(name)
	{};
	virtual ~BaseAIBehavior(void);

	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name ); // add xml node

	virtual float calculateImportance( Map* map, Agent* toThink) = 0;
	virtual bool Think( Map* map, Agent* toThink ) = 0;
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString ) = 0;

protected:
	std::string m_name;
	//Agent* m_agent;
};

