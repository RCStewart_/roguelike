#include "AStarPathing.hpp"
#include "AStarNode.hpp"
#include "Agent.hpp"

unsigned int MAX_INT_VALUE = 99999;

//----------------------------------------------------------------------------------
AStarPathing::AStarPathing(void)
{
}

//----------------------------------------------------------------------------------
AStarPathing::~AStarPathing(void)
{
}

//----------------------------------------------------------------------------------
void AStarPathing::generatePath( std::vector<IntVec2>& outPath, IntVec2& currentPos, IntVec2& finalLocation, Map* map, Agent* agentSearchingForAPath )
{
	std::vector<AStarNode*> openList;	//Check List		
	std::vector<AStarNode*> closedList;	//Ignore List

	outPath.clear();

	float currentCost = 0;
	float hueristic = (float) IntVec2::getManhattenDistance( currentPos, finalLocation );

	AStarNode* currentNode = new AStarNode( currentPos, currentCost, hueristic, nullptr );
	openList.push_back( currentNode );

	while( !openList.empty() )
	{
		std::sort( openList.begin(), openList.end(), AStarNode::howToSortAStarNodeInInverseOrder );

		currentNode = openList[openList.size() - 1];
		openList.pop_back();

		if( currentNode->m_position == finalLocation )
		{
			break; //SUCCESS
		}

		closedList.push_back( currentNode );

		std::vector<IntVec2> adjacentPositions;
		gatherAdjacentPositions( adjacentPositions, map, currentNode->m_position, agentSearchingForAPath );

		//		Watch out for memory leaks

		for( size_t adjacentIndex = 0; adjacentIndex < adjacentPositions.size(); ++adjacentIndex )
		{
			if( isIntVec2InVectorOfAStarNodes( adjacentPositions[adjacentIndex], closedList ) != -1)
			{
				continue;
			}
			int indexInOpenList = isIntVec2InVectorOfAStarNodes( adjacentPositions[adjacentIndex], openList );
			if( indexInOpenList != -1 )
			{
				//	compute newG = new Node G + cost to this adjacent node (dist?);
				float currentCostCalculatedOfNeighbor = getCostCalculatedSoFarFromAStarNodeToGivenNeighbor( currentNode, adjacentPositions[adjacentIndex], agentSearchingForAPath, map );

				if( currentCostCalculatedOfNeighbor > openList[indexInOpenList]->m_currentCost )
				{
					continue;
				}
				else
				{
					openList[indexInOpenList]->m_currentCost = currentCostCalculatedOfNeighbor;
					openList[indexInOpenList]->m_parent = currentNode;
					continue;
				}
			}

			//create new node with position and add to open list
			float currentCostCalculatedOfNeighbor = getCostCalculatedSoFarFromAStarNodeToGivenNeighbor( currentNode, adjacentPositions[adjacentIndex], agentSearchingForAPath, map );
			if(currentCostCalculatedOfNeighbor < (float) MAX_INT_VALUE )
			{
				float currentNeighborHueristic = (float) IntVec2::getManhattenDistance( adjacentPositions[adjacentIndex] ,finalLocation );
				AStarNode* neighborNode = new AStarNode( adjacentPositions[adjacentIndex], currentCostCalculatedOfNeighbor, currentNeighborHueristic, currentNode );
				openList.push_back( neighborNode );
			}
		}
	}

	//Add path to outPath
	fillPathFromFinalNode( outPath, currentNode );

	if(openList.size() > 0)
	{
		delete currentNode;
	}

	//TODO: Clean Up Memory
	for(size_t i = 0; i < openList.size(); ++i)
	{
		delete openList[i];
	}
	for(size_t i = 0; i < closedList.size(); ++i)
	{
		delete closedList[i];
	}
}

//----------------------------------------------------------------------------------
void AStarPathing::fillPathFromFinalNode( std::vector<IntVec2>& outPath, AStarNode* goldenNode )
{
	while( goldenNode->m_parent != nullptr )
	{
		outPath.push_back(goldenNode->m_position);
		goldenNode = goldenNode->m_parent;
	}
	//outPath.push_back(goldenNode->m_position);
	std::reverse( outPath.begin(), outPath.end() );
}

//----------------------------------------------------------------------------------
float AStarPathing::getCostCalculatedSoFarFromAStarNodeToGivenNeighbor( AStarNode* node, IntVec2& positionOfNeighbor, Agent* agentSearchingForAPath, Map* map )
{
	float gCostBase = 1;
	int indexInMap = map->getCellIndexFromPosition( positionOfNeighbor );
	if( indexInMap != -1 )
	{
		gCostBase = ( float ) map->m_cells[indexInMap].gValueOfCellTypeForAgent( agentSearchingForAPath );
	}
	float costOfDiagonal = 1.4f * gCostBase;
	float costOfAdjacent = 1.0f * gCostBase;
	if( node->m_position.x != positionOfNeighbor.x && node->m_position.y != positionOfNeighbor.y)
	{
		return node->m_currentCost + costOfDiagonal; 
	}
	return node->m_currentCost + costOfAdjacent;
}

//----------------------------------------------------------------------------------
void AStarPathing::gatherAdjacentPositions( std::vector<IntVec2>& outAdjacengtPositions, Map* map, IntVec2& currentPosition, Agent* agentSearchingForAPath  )
{
	for( int x = currentPosition.x - 1; x <= currentPosition.x + 1; ++x )
	{
		for( int y = currentPosition.y - 1; y <= currentPosition.y + 1; ++y )
		{
			IntVec2 adjacent(x, y);
			if( !(adjacent == currentPosition) )
			{
				if(x >=0 && x < map->m_numOfCellsWide && y >=0 && y < map->m_numOfCellsHigh )
				{
					int mapIndex = map->getCellIndexFromPosition(adjacent);
					if( map->m_cells[mapIndex].howWalkableIsCellType(agentSearchingForAPath) > 0)
					{
						outAdjacengtPositions.push_back(adjacent);
					}
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
int AStarPathing::isIntVec2InVectorOfAStarNodes( IntVec2& toCheck, std::vector<AStarNode*>& container )
{
	int isInList = -1;
	for( size_t containerIndex = 0; containerIndex < container.size(); ++containerIndex)
	{
		if( container[containerIndex]->m_position == toCheck )
		{
			isInList = containerIndex;
		}
	}

	return isInList;
}