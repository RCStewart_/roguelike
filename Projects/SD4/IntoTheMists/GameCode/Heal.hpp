#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"

class Heal : public BaseAIBehavior
{
public:
	Heal( const std::string name );
	~Heal(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	int			minHeal;
	int			maxHeal;
	int			healThreshhold;
	Agent*		topTarget;

protected:
	static AIBehaviorRegistration s_healRegistration;
};

