#include "Steal.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Item.hpp"
#include "Messages.hpp"
#include <string>

AIBehaviorRegistration Steal::s_stealRegistration( "Steal", &Steal::createAndGetAIBehavior );

Steal::Steal( std::string name ) 
	: BaseAIBehavior( name )
	, m_chanceToMoveStrait(0.f)
	, m_chanceToRest(0.f)
{

}

Steal::~Steal(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Steal::createAndGetAIBehavior( const std::string& name )
{
	return new Steal( name );
}

//----------------------------------------------------------------------------------
bool Steal::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	bool thoughtResult = true;

	thinker->m_currentPath.clear();
	Agent* toStealFrom = map->getMostHatedEnemyNearby( 1, thinker->m_position, thinker );
	if(toStealFrom)
	{
		Item* stolenItem = nullptr;
		if( (int) toStealFrom->m_inventory.size() > 0)
		{
			stolenItem = toStealFrom->m_inventory[toStealFrom->m_inventory.size() - 1];
			toStealFrom->m_inventory.pop_back();
		}
		else
		{
			stolenItem = toStealFrom->getAndDisEquipEquippedItem();
		}

		if(stolenItem)
		{
			std::string message = GetMessageLiteralf( "%s stole %s from %s", thinker->m_name.c_str(), stolenItem->m_name.c_str(), toStealFrom->m_name.c_str() );
			g_messages->addMessage( message, thinker->m_position  );
			thinker->addItemToInventoryOrEquip( stolenItem );
		}
		else
		{
			std::string message = GetMessageLiteralf( "%s tried to steal", thinker->m_name.c_str() );
			g_messages->addMessage( message, thinker->m_position );
		}
	}

	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Steal::calculateImportance( Map* map, Agent* toThink)
{
	int importance = 0;
	importance += (3 - toThink->m_inventory.size());

	int numberOfHated = map->getNumOfHatedEnemiesNearby( 1, toThink->m_position, toThink );

	importance *= numberOfHated;
	if(importance > 0)
	{
		importance = rand() % importance;
	}

	return importance;
}

//----------------------------------------------------------------------------------
void Steal::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	
}