#include "Heal.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "FactionManager.hpp"
#include "AStarPathing.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Messages.hpp"

AIBehaviorRegistration Heal::s_healRegistration( "Heal", &Heal::createAndGetAIBehavior );

Heal::Heal( std::string name ) 
	: BaseAIBehavior( name )
	, minHeal(2)
	, maxHeal(5)
	, healThreshhold(10)
	, topTarget(nullptr)
{

}

Heal::~Heal(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Heal::createAndGetAIBehavior( const std::string& name )
{
	return new Heal( name );
}

//----------------------------------------------------------------------------------
bool Heal::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	thinker->m_currentPath.clear();
	bool thoughtResult = true;

	if(topTarget)
	{
		int toHeal = RandomNumber::getRandomIntInRange( IntVec2(minHeal, minHeal) );
		int currentHealth = topTarget->m_currentHealth;
		int maxHealth = topTarget->m_maxHealth;
		int maxCanHeal = maxHealth - currentHealth;

		int amountToHeal = min( toHeal, maxCanHeal );
		topTarget->m_currentHealth += amountToHeal;
		
		std::string healMessage = GetMessageLiteralf( "%s healed %s for %d hp", thinker->m_name.c_str(), topTarget->m_name.c_str(), amountToHeal);
		g_messages->addMessage( healMessage, topTarget->m_position );
	}
	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Heal::calculateImportance( Map* map, Agent* thinker)
{
	std::set<int>::iterator it;
	float utility = 0.f;
	topTarget = nullptr;
	for (it = thinker->m_cellsCanSee.begin(); it != thinker->m_cellsCanSee.end(); ++it)
	{
		if(map->m_cells[*it].m_occupant)
		{
			if( FactionManager::isAlly(map->m_cells[*it].m_occupant, thinker) )
			{
				int tempUtility = map->m_cells[*it].m_occupant->m_maxHealth - map->m_cells[*it].m_occupant->m_currentHealth;
				
				if( tempUtility > healThreshhold )
				{
					if( tempUtility > utility)
					{
						topTarget = map->m_cells[*it].m_occupant;
						utility = tempUtility;
					}
				}
			}
		}
	}

	if(topTarget)
	{
		return (float) utility * 4.0;
	}

	return 0.0f;
}

//----------------------------------------------------------------------------------
void Heal::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	if(name == "minHeal")
	{
		minHeal = std::stoi(attributeAsString);
	}
	if(name == "maxHeal")
	{
		maxHeal = std::stoi(attributeAsString);
	}
	if(name == "healThreshhold")
	{
		healThreshhold = std::stoi(attributeAsString);
	}
}