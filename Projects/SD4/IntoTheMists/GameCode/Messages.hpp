#pragma once

#include <deque>
#include <string>
#include "Engine/Math/Vec2.hpp"
#include "Engine/Math/IntVec2.hpp"

class FontGenerator;
class Player;

class Messages
{
public:
	Messages(void);
	~Messages(void);

	void addMessage( const std::string& messageToAdd, const IntVec2& locationOfEvent  );
	bool hasMoreMessages();
	void incrementMessage();
	void displayMessage( FontGenerator* font, const Vec2& startingLocationTopLeft );
	void clearMessageLog();
	//void updateMessages();

	int m_messageIndex;
	std::deque<std::string> m_messages;
	Player* m_referenceToPlayer;
};

extern Messages* g_messages;