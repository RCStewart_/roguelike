#include "AIBehaviorRegistration.hpp"

AIBehaviorRegistrationMap* AIBehaviorRegistration::s_behaviorRegistrationMap = nullptr;

AIBehaviorRegistration::~AIBehaviorRegistration(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* AIBehaviorRegistration::createBehavior()
{
	return (*s_behaviorRegistrationMap)[m_name]->m_aiCreateFunc(m_name);
}