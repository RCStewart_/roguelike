#pragma once
#include "Engine\Core\RGBAColors.hpp"
#include "Item.hpp"
#include <deque>
#include "Agent.hpp"
#include "Feature.hpp"

class Agent;
class FontGenerator;

enum CellType
{
	CELL_TYPE_AIR,
	CELL_TYPE_WALL,
	CELL_TYPE_HALL,
	CELL_TYPE_BUILDING_WALL,
	CELL_TYPE_BUILDING_FLOOR,
	CELL_TYPE_SHALLOW_WATER,
	CELL_TYPE_LAVA,
	CELL_TYPE_START,
	CELL_TYPE_END,
	CELL_TYPE_VOID
};

class Cell
{
public:
	Cell(void);
	~Cell(void);

	RGBAColors getColorOfType();
	bool isWalkable();
	bool isWall();
	int howWalkableIsCellType( Agent* heWhoIsWalking );
	int gValueOfCellTypeForAgent( Agent* heWhoIsWalking );
	bool isBuilding();
	void setTypeWithCharGlyph(char newType);
	char getGlyph();

	void setPercentLit( float percentLit );
	void lowerPercentLightingToPercentLit( float percentLit );
	void raisePercentLightingToPercentLit( float percentLit );

	bool isOccupantVisible();
	float getPercentLit();
	
	void addTopItemToAgent( Agent* heWhoPicksUpItem );
	void addItemToList( Item* itemToAdd );

	CellType m_currentCellType;
	Agent* m_occupant;
	Feature* m_feature;

	float m_percentLit;
	int m_timesOverWritten;

	std::deque<Item*> m_items;
	static float s_basePercentLight;
};

//----------------------------------------------------------------------------------
inline RGBAColors Cell::getColorOfType()
{
	switch(m_currentCellType)
	{
	case CELL_TYPE_AIR:
		return RGBAColors(150, 150, 150, 255)	;
		break;
	case CELL_TYPE_START:
		return RGBAColors(175, 132, 98, 255)	;
		break;
	case CELL_TYPE_END:
		return RGBAColors(175, 132, 98, 255)	;
		break;
	case CELL_TYPE_WALL:
		return RGBAColors(75, 75, 35, 255)		;
		break;
	case CELL_TYPE_HALL:
		return RGBAColors(200, 200, 200, 255)	;
		break;
	case CELL_TYPE_SHALLOW_WATER:
		return RGBAColors(25, 25, 200, 255)		;
		break;
	case CELL_TYPE_BUILDING_WALL:
		return RGBAColors(100, 100, 100, 255)	;
		break;
	case CELL_TYPE_BUILDING_FLOOR:
		return RGBAColors(175, 132, 98, 255)	;
		break;
	case CELL_TYPE_LAVA:
		return RGBAColors(155, 0, 0, 255)			;
		break;
	case CELL_TYPE_VOID:
		return RGBAColors(0, 0, 0, 255)			;
		break;
	}
	return RGBAColors(255,255,255,255)			;
}

//----------------------------------------------------------------------------------
inline bool Cell::isWalkable()
{
	if(m_feature)
	{
		if(m_feature->isAWall())
			return false;
	}
	return ((m_currentCellType == CELL_TYPE_AIR) || (m_currentCellType == CELL_TYPE_BUILDING_FLOOR) || (m_currentCellType == CELL_TYPE_HALL) || (m_currentCellType == CELL_TYPE_SHALLOW_WATER) || (m_currentCellType == CELL_TYPE_LAVA) || (m_currentCellType == CELL_TYPE_START) || (m_currentCellType == CELL_TYPE_END));
}

//----------------------------------------------------------------------------------
inline bool Cell::isWall()
{
	if(m_feature)
	{
		if(m_feature->isAWall())
			return true;
	}
	return ((m_currentCellType == CELL_TYPE_BUILDING_WALL) || (m_currentCellType == CELL_TYPE_WALL));
}

//----------------------------------------------------------------------------------
inline bool Cell::isBuilding()
{
	return ((m_currentCellType == CELL_TYPE_BUILDING_FLOOR) || (m_currentCellType == CELL_TYPE_BUILDING_WALL));
}

//----------------------------------------------------------------------------------
inline bool Cell::isOccupantVisible()
{
	return m_percentLit > s_basePercentLight;
}