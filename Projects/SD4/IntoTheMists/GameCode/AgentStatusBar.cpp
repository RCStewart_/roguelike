#include "AgentStatusBar.hpp"
#include "Engine/Font/FontGenerator.hpp"
#include "Engine/Math/Vec2.hpp"
#include <string>
#include "Agent.hpp"
#include "CombatSystem.hpp"
#include "Engine/Core/EngineCommon.hpp"

AgentStatusBar::AgentStatusBar(void)
{
}


AgentStatusBar::~AgentStatusBar(void)
{
}

void AgentStatusBar::displayAgentOnScreen( Agent* toRender, FontGenerator* font, const Vec2& startingLocationTopLeft )
{
	Vec2 positionOnScreen = startingLocationTopLeft;
	float sectionDisplacement = 15.f;
	float heightDisplacement = 1.5f;

	AttackData attack;
	DefendData defense;
	toRender->getDefense( attack, defense );
	toRender->getOffense( attack );
	std::string name = "Name: ";
	name += toRender->m_name;
	font->drawText( name, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	std::string glyph = "Glyph: ";
	glyph += toRender->m_glyph;
	positionOnScreen.y = startingLocationTopLeft.y - heightDisplacement;
	font->drawText( glyph, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	positionOnScreen = startingLocationTopLeft;
	positionOnScreen.x += sectionDisplacement;
	std::string health =  GetMessageLiteralf( " Health: %d/%d", toRender->m_currentHealth, toRender->m_maxHealth );
	font->drawText( health, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	std::string armorClass = "Armor Class: ";
	armorClass += to_string(static_cast<long int>(defense.armorClass));
	armorClass += " + ";
	armorClass += to_string(static_cast<long int>(defense.bonusArmor));
	positionOnScreen.y = startingLocationTopLeft.y - heightDisplacement;
	font->drawText( armorClass, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	std::string damage = "Damage Range: ";
	damage += to_string(static_cast<long int>(attack.damageRange.x));
	damage += " - ";
	damage += to_string(static_cast<long int>(attack.damageRange.y));
	positionOnScreen = startingLocationTopLeft;
	positionOnScreen.x += sectionDisplacement * 2;
	font->drawText( damage, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	std::string hit = "Hit Chance Roll: ";
	hit += to_string(static_cast<long int>(attack.baseHitChance.x));
	hit += " - ";
	hit += to_string(static_cast<long int>(attack.baseHitChance.y));
	hit += " + ";
	hit += to_string(static_cast<long int>(attack.bonusChanceToHit));
	positionOnScreen.y = startingLocationTopLeft.y - heightDisplacement;
	font->drawText( hit, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );

	std::string turnsTaken = "Turns Taken: ";
	turnsTaken += to_string(static_cast<long int>(toRender->m_numOfTurnsTaken));
	positionOnScreen.y = startingLocationTopLeft.y - heightDisplacement * 2;
	font->drawText( turnsTaken, GENERATED_FONTS_BIT, 1.5f, positionOnScreen, RGBAColors(255,255,255,255), false );
}