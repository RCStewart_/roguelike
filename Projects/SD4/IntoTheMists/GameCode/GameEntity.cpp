#include "GameEntity.hpp"
#include "Engine/Font/FontGenerator.hpp"
#include "CombatSystem.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Map.hpp"

int GameEntity::s_nextGameEntityID = 1;

//----------------------------------------------------------------------------------
bool GameEntity::isDebug( int debugLevel ) const 
{
	return (m_debugLevel >= debugLevel);
}

//----------------------------------------------------------------------------------
GameEntity::GameEntity( Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color )
	: m_ID( s_nextGameEntityID++ )
	, m_position(position)
	, m_name( name )
	, m_glyph( glyph )
	, m_map( map )
	, m_color( color )
	, m_isDead( false )
	, m_armorClass( 0 )
	, m_reflectiveDamageRange( 0, 0 )
	, m_damageRange( 4, 10 )
	, m_baseChanceToHit( 1, 1 )
	, m_bonusChanceToHit( 0 )
	, m_startingPosition(position)
{
	m_currentHealth = 15;
	m_maxHealth = 15;
}

//----------------------------------------------------------------------------------
GameEntity::~GameEntity(void)
{
}

//----------------------------------------------------------------------------------
 void GameEntity::update( Map* map, double deltaSeconds )
 {
	 UNUSED( map );
	 UNUSED( deltaSeconds );
 }

 //----------------------------------------------------------------------------------
 void GameEntity::render( Map* map, FontGenerator* font )
 {
	 UNUSED( map );
	 UNUSED( font );
 }

 //----------------------------------------------------------------------------------
 void GameEntity::setMaxHealth( const int& maxHealth )
 {
	 m_maxHealth = maxHealth;
 }

 //----------------------------------------------------------------------------------
 void GameEntity::setCurrentHealth( const int& currentHealth)
 {
	 m_currentHealth = currentHealth;
 }

 //----------------------------------------------------------------------------------
 void GameEntity::getDefense( const AttackData& inAttackData, DefendData& outDefendData )
 {
	 UNUSED( inAttackData );
	 outDefendData.armorClass = m_armorClass;
	 outDefendData.reflectiveDamageRange = m_reflectiveDamageRange;
	 outDefendData.bonusArmor = 0;
 }

 //----------------------------------------------------------------------------------
 void GameEntity::getOffense( AttackData& outAttackData )
 {
	 outAttackData.bonusChanceToHit = m_bonusChanceToHit;
	 outAttackData.baseHitChance = m_baseChanceToHit;
	 outAttackData.damageRange = m_damageRange;
 }

 //----------------------------------------------------------------------------------
 void GameEntity::setCombatStats( const IntVec2& damageRange, const IntVec2& baseChanceToHit, int bonusChanceToHit, int armorClass, int health )
 {
	 m_armorClass = armorClass;
	 m_damageRange = damageRange;
	 m_baseChanceToHit = baseChanceToHit;
	 m_bonusChanceToHit = bonusChanceToHit;
	 m_currentHealth = health;
	 m_maxHealth = health;
 }

 //----------------------------------------------------------------------------------
 void GameEntity::saveToXML( TiXmlElement* gameEntity )
 {
	 appendAttributeToXMLElement( "name", m_name, "", gameEntity );
	 appendAttributeToXMLElement( "ID", to_string((long long) m_ID), to_string((long long) 0 ), gameEntity );
	 appendAttributeToXMLElement( "currentHealth", to_string((long long) m_currentHealth), to_string((long long) m_maxHealth ), gameEntity );

	 int index = m_map->getCellIndexFromPosition( m_position );
	 appendAttributeToXMLElement( "position", to_string((long long) index ), to_string((long long) -1 ), gameEntity);
 }

 //----------------------------------------------------------------------------------
 void GameEntity::loadFromXML( TiXmlElement* gameEntityNode )
 {
	 m_name = gameEntityNode->Attribute( "name" );
	 m_ID = std::stoi(gameEntityNode->Attribute( "ID" ));

	 if(gameEntityNode->Attribute("currentHealth"))
	 {
		 m_currentHealth = std::stoi(gameEntityNode->Attribute( "currentHealth" ));
	 }

	 int index = -1;
	 if(gameEntityNode->Attribute( "position" ))
	 {
		 index = std::stoi(gameEntityNode->Attribute( "position" ));
	 }
	 
	 m_position = m_map->getCellPositionFromIndex( index );
 }

 //----------------------------------------------------------------------------------
 void GameEntity::appendAttributeToXMLElement(  const std::string& attributeName, const std::string& toAdd, const std::string& defaultValue, TiXmlElement* parent )
 {
	 if( toAdd != defaultValue )
	 {
		 parent->SetAttribute( attributeName.c_str(), toAdd.c_str() );
	 }
 }

 //----------------------------------------------------------------------------------
gameEntityStats GameEntity::getGeneralGameEntityStatsFromXMLNode( TiXmlElement* gameEntityNode, Map* map )
 {
	gameEntityStats toReturn;
	toReturn.name = gameEntityNode->Attribute( "name" );
	toReturn.ID = std::stoi(gameEntityNode->Attribute( "ID" ));

	int index = -1;
	if(gameEntityNode->Attribute( "position" ))
	{
		index = std::stoi(gameEntityNode->Attribute( "position" ));
	}
	toReturn.positionIndex = index;
	toReturn.position = map->getCellPositionFromIndex( index );
	return toReturn;
 }

//----------------------------------------------------------------------------------
void GameEntity::resolvePointers( std::map<int, GameEntity*>& oldIDsToEntityMap )
{

}