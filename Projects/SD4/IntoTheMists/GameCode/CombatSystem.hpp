#pragma once

#include "Engine/Math/IntVec2.hpp"
#include <string>
class GameEntity;

struct ResultOfAttack
{
	int damageDeltToAttacker;
	int damageDeltToDefender;
	int currentDefenderHP;
};

struct DefendData
{
	int armorClass;
	int bonusArmor;
	IntVec2 reflectiveDamageRange;
};

struct AttackData
{
	int bonusChanceToHit;
	IntVec2 baseHitChance;
	IntVec2 damageRange;
};

class CombatSystem
{
public:
	CombatSystem();
	~CombatSystem();

	static void resolveCombat( GameEntity* attacker, GameEntity* defender, const AttackData& dataFromAttacker, ResultOfAttack& outResultOfAttack );
	static void calculateDamage( const AttackData& attackData, const DefendData& defendData, ResultOfAttack& outResult);

	static void createAndShowMessage( GameEntity* attacker, GameEntity* defender, const ResultOfAttack& results );
};

