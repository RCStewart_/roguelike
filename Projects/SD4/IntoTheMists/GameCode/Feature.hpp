#pragma once
#include <string>
#include "Engine/Core/RGBAColors.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "GameEntity.hpp"

class Map;
class TiXmlElement;

enum FeatureType
{
	FEATURE_TYPE_DOOR
};

class Feature: public GameEntity
{
public:
	Feature( std::string& name, char glyphOn, char glyphOff, FeatureType featureType, RGBAColors& color, bool isActive, Map* map, IntVec2& position );
	~Feature(void);

	static FeatureType Feature::sGetFeatureTypeFromString( const std::string& featureTypeAsString );
	virtual void toggleActive();
	virtual bool isAWall();
	//----------------------------------------------------------------------------------
	virtual void saveToXML( TiXmlElement* parent );
	virtual void loadFromXML( TiXmlElement* parent );

	char m_glyphOn;
	char m_glyphOff;

	bool m_isActive;
	FeatureType m_featureType;
};

