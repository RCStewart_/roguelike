#pragma once

class Agent;
class FontGenerator;
class Vec2;

class AgentStatusBar
{
public:
	AgentStatusBar(void);
	~AgentStatusBar(void);

	static void displayAgentOnScreen( Agent* toRender, FontGenerator* font, const Vec2& startingLocationTopLeft );
};

