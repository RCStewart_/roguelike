#pragma once

#include "Engine/Core/RGBAColors.hpp"
#include <string>
#include <vector>
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"
#include "Engine\Math\IntVec2.hpp"
#include "Item.hpp"

class Cell;
class Map;

class ItemFactory
{
public:
	ItemFactory( const std::string& name, char glyph, const RGBAColors& color, ItemType itemType, int uses, const IntVec2& powerRange, int bonusChance );
	~ItemFactory();

	static int sGenerateItemFactoriesFromFileList( std::vector<std::string>& files );
	static int getItemFactoryIndexForName( const std::string& name );

	Item* createItem( Map* map, IntVec2& position );
	bool isValidItemForType( Cell& cellToCheck );

	std::string		m_name; 
	char			m_glyph;
	RGBAColors		m_color;
	ItemType		m_itemType;
	int				m_uses;
	IntVec2			m_powerRange;
	int				m_bonusChance;

	static std::vector<ItemFactory*> s_registeredFactories;
};

