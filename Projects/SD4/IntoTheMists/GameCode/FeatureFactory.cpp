#include "FeatureFactory.hpp"
#include "Map.hpp"
#include "Cell.hpp"

std::vector<FeatureFactory*> FeatureFactory::s_registeredFactories;

//----------------------------------------------------------------------------------
FeatureFactory::FeatureFactory( const std::string& name, char glyphOn, char glyphOff, const RGBAColors& color, FeatureType featureType )
	:m_name(name) 
	,m_glyphOn(glyphOn)
	,m_glyphOff(glyphOff)
	,m_color(color)
	,m_featureType(featureType)
{
}

//----------------------------------------------------------------------------------
FeatureFactory::~FeatureFactory(void)
{
}

//----------------------------------------------------------------------------------
int FeatureFactory::sGenerateFeatureFactoriesFromFileList( std::vector<std::string>& files )
{
	for( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		TiXmlDocument doc;

		if(!doc.LoadFile( files[fileIndex].c_str()))
		{
			return 0;
		}

		TiXmlElement* root = doc.FirstChildElement();

		if(root == NULL)
		{
			doc.Clear();
			return 0;
		}

		TiXmlElement* feature = root->FirstChildElement( "Feature" );

		std::string name = feature->Attribute( "name" );
		std::string glyphOn = feature->Attribute( "glyphOn" );
		std::string glyphOff = feature->Attribute( "glyphOff" );
		std::string r = feature->Attribute( "r" );
		std::string g = feature->Attribute( "g" );
		std::string b = feature->Attribute( "b" );
		std::string a = feature->Attribute( "a" );

		RGBAColors newColor( (unsigned char) std::stof(r), (unsigned char) std::stof(g),
			(unsigned char) std::stof(b), (unsigned char) std::stof(a));

		std::string typeName = feature->Attribute( "type" );

		FeatureFactory* newFactory = new FeatureFactory( name, glyphOn[0], glyphOff[0], newColor, Feature::sGetFeatureTypeFromString(typeName) );
		s_registeredFactories.push_back(newFactory);
	}
	return 0;
}

//----------------------------------------------------------------------------------
Feature* FeatureFactory::createFeature( Map* map, IntVec2& position )
{
	bool isActive = false;
	if(rand() % 2 == 1)
	{
		isActive = true;
	}
	Feature* toReturn = new Feature(
		m_name,
		m_glyphOn,
		m_glyphOff,
		m_featureType,
		m_color,
		isActive,
		map,
		position
		);
	return toReturn;
}

//----------------------------------------------------------------------------------
bool FeatureFactory::isValidFeatureForPosition( int location, Map* map )
{
	bool isValid = false;

	if( map->m_cells[location].isWalkable() )
	{
		if( m_featureType == FEATURE_TYPE_DOOR )
		{
			IntVec2 pos = map->getCellPositionFromIndex(location);

			IntVec2 west = IntVec2( pos.x - 1, pos.y);
			IntVec2 east = IntVec2( pos.x + 1, pos.y);
			IntVec2 north = IntVec2( pos.x, pos.y + 1);
			IntVec2 south = IntVec2( pos.x, pos.y - 1);

			int westIndex = map->getCellIndexFromPosition(west);
			int eastIndex = map->getCellIndexFromPosition(east);
			int northIndex = map->getCellIndexFromPosition(north);
			int southIndex = map->getCellIndexFromPosition(south);

			if( westIndex != -1 && eastIndex != -1 && northIndex != -1 && southIndex != -1 )
			{
				if( (!map->m_cells[westIndex].isWalkable() && !map->m_cells[eastIndex].isWalkable()) 
					&& (map->m_cells[southIndex].isWalkable() && map->m_cells[northIndex].isWalkable()))
				{
					isValid = true;
				}
				if( (map->m_cells[westIndex].isWalkable() && map->m_cells[eastIndex].isWalkable()) 
					&& (!map->m_cells[southIndex].isWalkable() && !map->m_cells[northIndex].isWalkable()))
				{
					isValid = true;
				}
			}
		}
	}

	return isValid;
}

//----------------------------------------------------------------------------------
int FeatureFactory::getFeatureFactoryIndexForName( const std::string& name )
{
	for( int factoryIndex = 0; factoryIndex < (int) s_registeredFactories.size(); ++factoryIndex )
	{
		if( name == s_registeredFactories[factoryIndex]->m_name )
		{
			return factoryIndex;
		}
	}
	return -1;
}