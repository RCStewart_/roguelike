#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"

class Follow : public BaseAIBehavior
{
public:
	Follow( const std::string name );
	~Follow(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	std::string m_leaderName;
	float		m_distanceThreshhold;
	Agent*		topTarget;

protected:
	static AIBehaviorRegistration s_followRegistration;
};

