#include "BaseMapGenerator.hpp"
#include "Map.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "NPCFactory.hpp"
#include "NPC.hpp"
#include <algorithm>
#include "Item.hpp"
#include "ItemFactory.hpp"
#include "Feature.hpp"
#include "FeatureFactory.hpp"

MapGeneratorRegistrationMap* MapGeneratorRegistration::s_mapRegistration = nullptr;
MapsGeneratedMap BaseMapGenerator::s_generatedMaps;

//----------------------------------------------------------------------------------
BaseMapGenerator* MapGeneratorRegistration::createMapGenerator()
{
	return (*s_mapRegistration)[m_name]->m_registrationFunc(m_name);
}

//----------------------------------------------------------------------------------
BaseMapGenerator::BaseMapGenerator(void)
{
}

//----------------------------------------------------------------------------------
BaseMapGenerator::BaseMapGenerator(const std::string& name):m_name(name)
{
}

//----------------------------------------------------------------------------------
BaseMapGenerator::~BaseMapGenerator(void)
{
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::generateMap( Map* map )
{
	initializeRandomData(map);
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::startup()
{

}

//----------------------------------------------------------------------------------
void BaseMapGenerator::shutdown()
{

}

//----------------------------------------------------------------------------------
const std::string BaseMapGenerator::getName() const
{
	return m_name;
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::processOneStep( Map* map )
{
	UNUSED(map);
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::initializeRandomData( Map* map )
{
	map->makeThisPercentOfCellsAir(.55f);
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::processSeveralSteps( Map* map )
{
	int numOfSteps = 30;
	for(int i = 0; i < numOfSteps; ++i )
	{
		processOneStep( map );
	}
}

//----------------------------------------------------------------------------------
int BaseMapGenerator::createGenerators()
{
	int numOfRegistrations = 0;
	MapGeneratorRegistrationMap* registrations = MapGeneratorRegistration::getMapRegistrations();
	if( registrations )
	{
		for(MapGeneratorRegistrationMap::iterator registerIterator = registrations->begin(); registerIterator != registrations->end(); registerIterator++ )
		{
			++numOfRegistrations;
			MapGeneratorRegistration* registration = registerIterator->second;
			BaseMapGenerator* generator = registration->createMapGenerator();
			std::string name = registration->getName();
			s_generatedMaps[name] = generator;
		}
	}
	return numOfRegistrations;
}

//----------------------------------------------------------------------------------
IntVec2 BaseMapGenerator::getPlayersPosition( Map* map )
{
	int numAttemptsToPlaceThePlayer = 5000;
	while(numAttemptsToPlaceThePlayer >= 0)
	{
		int x = rand() % map->m_numOfCellsWide;
		int y = rand() % map->m_numOfCellsHigh;

		IntVec2 location = IntVec2( x, y );
		int index = map->getCellIndexFromPosition( location );
		if( map->m_cells[index].isWalkable() && map->m_cells[index].m_occupant == nullptr )
		{
			return location;
		}
		--numAttemptsToPlaceThePlayer;
	}
	return IntVec2(0,0);
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::addNValidItemsToMap( Map* map, int numberOfItemsToAdd, std::vector<Item*>& items )
{
	for( int numOfItemsAdded = 0; numOfItemsAdded < numberOfItemsToAdd; ++numOfItemsAdded)
	{
		int i = 500;
		while(i >= 0)
		{
			int x = rand() % map->m_numOfCellsWide;
			int y = rand() % map->m_numOfCellsHigh;

			IntVec2 location = IntVec2( x, y );
			int index = map->getCellIndexFromPosition( location );

			if( map->m_cells[index].isWalkable() )
			{
				std::random_shuffle( ItemFactory::s_registeredFactories.begin(), ItemFactory::s_registeredFactories.end());
				for( size_t itemFactoryIndex = 0; itemFactoryIndex < ItemFactory::s_registeredFactories.size(); ++itemFactoryIndex )
				{
					{
						Item* currentItem = ItemFactory::s_registeredFactories[itemFactoryIndex]->createItem( map, location );
						map->m_cells[index].addItemToList( currentItem );
						items.push_back( currentItem );
						i = -1;
						break;
					}
				}
			}
			--i;
		}
	}
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::addNValidNPCSToMap( Map* map, int numberOfNPCsToAdd, std::vector<NPC*>& npcs )
{
	for( int numOfNPCsAdded = 0; numOfNPCsAdded < numberOfNPCsToAdd; ++numOfNPCsAdded)
	{
		int i = 500;
		while(i >= 0)
		{
			int x = rand() % map->m_numOfCellsWide;
			int y = rand() % map->m_numOfCellsHigh;

			IntVec2 location = IntVec2( x, y );
			int index = map->getCellIndexFromPosition( location );

			if( !map->m_cells[index].isWall() && map->m_cells[index].m_occupant == nullptr )
			{
				std::random_shuffle( NPCFactory::s_registeredFactories.begin(), NPCFactory::s_registeredFactories.end());
				for( size_t npcFactoryIndex = 0; npcFactoryIndex < NPCFactory::s_registeredFactories.size(); ++npcFactoryIndex )
				{
					if( NPCFactory::s_registeredFactories[npcFactoryIndex]->isValidNPCForType( map->m_cells[index] ) )
					{
						NPC* currentNPC = NPCFactory::s_registeredFactories[npcFactoryIndex]->createNPC( map, location );
						npcs.push_back( currentNPC );
						map->m_cells[index].m_occupant = (Agent*) currentNPC;
						i = -1;
						break;
					}
				}
			}
			--i;
		}
	}
}

//----------------------------------------------------------------------------------
void BaseMapGenerator::addNValidDoorsToMap( Map* map, int numberOfFeaturesToAdd, std::vector<Feature*>& features )
{

	std::vector<int> locations;
	for(size_t index = 0; index < map->m_cells.size(); ++index)
	{
		if( map->m_cells[index].m_feature == nullptr )
		{
			if( FeatureFactory::s_registeredFactories[0]->isValidFeatureForPosition( index, map ) )
			{
				locations.push_back(index);
			}
		}
	}

	std::random_shuffle( locations.begin(), locations.end());
	for( int locationIndex = 0; (locationIndex < numberOfFeaturesToAdd) && (locationIndex < (int) locations.size()); ++locationIndex)
	{
		IntVec2 pos = map->getCellPositionFromIndex(locations[locationIndex] );
		map->m_cells[locations[locationIndex]].m_feature = FeatureFactory::s_registeredFactories[0]->createFeature( map, pos );
		features.push_back(map->m_cells[locations[locationIndex]].m_feature);
	}
}