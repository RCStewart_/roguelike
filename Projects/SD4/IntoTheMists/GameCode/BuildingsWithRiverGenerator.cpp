
#include "BuildingsWithRiverGenerator.hpp"
#include "Map.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Room.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include "Engine/Core/GameCommon.hpp"

MapGeneratorRegistration BuildingsWithRiverGenerator::s_buildingsWithRiverRegistration( "Buildings with River", &BuildingsWithRiverGenerator::createGenerator );

//----------------------------------------------------------------------------------
BuildingsWithRiverGenerator::BuildingsWithRiverGenerator(const std::string& name) : CellularAutomataMapGenerator( "Buildings" )
{
	UNUSED( name );
}

//----------------------------------------------------------------------------------
BuildingsWithRiverGenerator::~BuildingsWithRiverGenerator(void)
{

}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::processSeveralSteps( Map* map )
{
	int numOfBuildings = 20;
	for(int i = 0; i < numOfBuildings; ++i)
	{
		processOneStep( map );
	}
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::generateMap( Map* map )
{
	initializeRandomData(map);

	int numOfStepsOfCellularAutomata = 30;
	for(int i = 0; i < numOfStepsOfCellularAutomata; ++i)
	{
		CellularAutomataMapGenerator::processOneStep( map );
	}

	addRiver( map );
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::placeABuilding( Map* map, Room& buildingToAdd)
{
	for(int x = (int) buildingToAdd.m_center.x - buildingToAdd.m_cellDisplacementFromCenterX - 1; x < (int) buildingToAdd.m_center.x + buildingToAdd.m_cellDisplacementFromCenterX + 1; ++x )
	{
		for(int y = (int) buildingToAdd.m_center.y - buildingToAdd.m_cellDisplacementFromCenterY - 1; y < (int) buildingToAdd.m_center.y + buildingToAdd.m_cellDisplacementFromCenterY + 1; ++y)
		{
			IntVec2 pos(x, y);
			int index = map->getCellIndexFromPosition(pos);
			if( index >= 0 && index < (int) map->m_cells.size() && x >= 0 && x < map->m_numOfCellsWide && y >= 0 && y < map->m_numOfCellsHigh )
			{
				CellType prevCellTypeOnCell = map->m_cells[index].m_currentCellType;
				map->m_cells[index].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
				if((x == buildingToAdd.m_center.x - buildingToAdd.m_cellDisplacementFromCenterX - 1)
					||(x == buildingToAdd.m_center.x + buildingToAdd.m_cellDisplacementFromCenterX)
					||(y == buildingToAdd.m_center.y - buildingToAdd.m_cellDisplacementFromCenterY - 1)
					||(y == buildingToAdd.m_center.y + buildingToAdd.m_cellDisplacementFromCenterY))
				{
					if(!(prevCellTypeOnCell == CELL_TYPE_BUILDING_FLOOR) && !(prevCellTypeOnCell == CELL_TYPE_BUILDING_WALL))
					{
						map->m_cells[index].m_currentCellType = CELL_TYPE_BUILDING_WALL;
					}
				}
				++map->m_cells[index].m_timesOverWritten;
			}
		}
	}
	addDoorWaysToBuilding(map, buildingToAdd);
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::addDoorWaysToBuilding( Map* map, Room& buildingToAdd )
{
	Vec2 min = Vec2(buildingToAdd.m_center.x - buildingToAdd.m_cellDisplacementFromCenterX - 1, buildingToAdd.m_center.y - buildingToAdd.m_cellDisplacementFromCenterY - 1); 
	Vec2 max = Vec2(buildingToAdd.m_center.x + buildingToAdd.m_cellDisplacementFromCenterX,  buildingToAdd.m_center.y + buildingToAdd.m_cellDisplacementFromCenterY);
	Vec2 yRange = Vec2(min.y, max.y);
	Vec2 xRange = Vec2(min.x, max.x);
	//North
	std::vector<Vec2> northRanges;
	getRangesForDoorHorizantol(map, (int) max.y + 1, xRange, northRanges);
	for(int i = 0; i < (int) northRanges.size(); ++i)
	{
		Vec2 range = northRanges[i];
		int xLocationForDoor = (rand() % (int) (range.y - range.x + 1.f)) + ( (int) range.x);
		IntVec2 locationForDoor(xLocationForDoor, (int) max.y);
		int indexForDoor = map->getCellIndexFromPosition(locationForDoor);
		if(indexForDoor != -1)
		{
			map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;

			if((xLocationForDoor == min.x) || (xLocationForDoor == max.x))
			{
				locationForDoor.y = (int) max.y - 1;
				indexForDoor = map->getCellIndexFromPosition(locationForDoor);
				if(map->isValidY((int) max.y - 1, indexForDoor))
				{
					map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
				}
			}
		}
	}
	//South
	std::vector<Vec2> southRanges;
	getRangesForDoorHorizantol(map, (int) min.y - 1, xRange, southRanges);
	for(int i = 0; i < (int) southRanges.size(); ++i)
	{
		Vec2 range = southRanges[i];
		int xLocationForDoor = (rand() % (int) (range.y - range.x + 1.f)) + ( (int) range.x);
		IntVec2 locationForDoor(xLocationForDoor, (int) min.y);
		int indexForDoor = map->getCellIndexFromPosition(locationForDoor); 

		if(indexForDoor != -1)
		{
			map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;

			if((xLocationForDoor == min.x) || (xLocationForDoor == max.x))
			{
				locationForDoor.y = (int) min.y + 1;
				indexForDoor = map->getCellIndexFromPosition(locationForDoor);
				if(map->isValidY((int) min.y + 1, indexForDoor))
				{
					map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
				}
			}
		}
	}

	//East
	std::vector<Vec2> eastRanges;
	getRangesForDoorVertical(map, (int) max.x + 1, yRange, eastRanges);
	for(int i = 0; i < (int) eastRanges.size(); ++i)
	{
		Vec2 range = eastRanges[i];
		int yLocationForDoor = (rand() % (int) (range.y - range.x + (float) 1)) + ((int) range.x);
		IntVec2 locationForDoor((int) max.x, yLocationForDoor);
		int indexForDoor = map->getCellIndexFromPosition(locationForDoor); 
		
		if(indexForDoor != -1)
		{
			map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;

			if((yLocationForDoor == min.y) || (yLocationForDoor == max.y))
			{
				locationForDoor.x = (int) max.x - 1;
				indexForDoor = map->getCellIndexFromPosition(locationForDoor);
				if(map->isValidX((int) max.x - 1, indexForDoor))
				{
					map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
				}
			}
		}
	}

	//West
	std::vector<Vec2> westRanges;
	getRangesForDoorVertical(map, (int) min.x - 1, yRange, westRanges);
	for(int i = 0; i < (int) westRanges.size(); ++i)
	{
		Vec2 range = westRanges[i];
		int yLocationForDoor = (rand() % (int) (range.y - range.x + 1.f)) + ((int) range.x);
		IntVec2 locationForDoor((int) min.x, yLocationForDoor);
		int indexForDoor = map->getCellIndexFromPosition(locationForDoor); 

		if(indexForDoor != -1)
		{
			map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;

			if((yLocationForDoor == min.y) || (yLocationForDoor == max.y))
			{
				locationForDoor.x = (int) min.x + 1;
				indexForDoor = map->getCellIndexFromPosition(locationForDoor);
				if(map->isValidX((int) min.x + 1, indexForDoor))
				{
					map->m_cells[indexForDoor].m_currentCellType = CELL_TYPE_BUILDING_FLOOR;
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::processOneStep( Map* map )
{
	int numberOfTries = 5000;
	for(int i = 0; i < numberOfTries; ++i)
	{
		int index = rand() % map->m_cells.size();

		bool isValidLocation = (map->m_cells[index].m_currentCellType == CELL_TYPE_AIR);
		if(!isValidLocation)
		{
			continue;
		}

		IntVec2 centerOfRoom = map->getCellPositionFromIndex(index);
		Vec2 roomLocation = Vec2((float) centerOfRoom.x, (float) centerOfRoom.y);
		int xDisplacement = (int) RandomNumber::getRandomFloatInRange( 1.f, 4.f );
		int yDisplacement = (int) RandomNumber::getRandomFloatInRange( 1.f, 4.f );
		Room centerRoom( xDisplacement, yDisplacement, roomLocation );
		placeABuilding( map, centerRoom );
		return;
	}

}

//----------------------------------------------------------------------------------
BaseMapGenerator* BuildingsWithRiverGenerator::createGenerator( const std::string& name )
{
	return new BuildingsWithRiverGenerator( name );
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::getRangesForDoorHorizantol( Map* map, int y, Vec2& xRange, std::vector<Vec2>& rangesToGather)
{
	Vec2 currentRange = Vec2(-1, -1);

	int x = (int) xRange.x;
	for(; x <= xRange.y; ++x)
	{
		IntVec2 cellPosition = IntVec2(x, y);
		int index = map->getCellIndexFromPosition(cellPosition);
		if( map->isValidX( x, index ))
		{
			if(currentRange.x == -1)
			{
				if( map->m_cells[index].isWalkable() )
				{
					currentRange.x = (float) x;
				}
			}
			else
			{
				if( !map->m_cells[index].isWalkable() )
				{
					currentRange.y = (float) x - 1.f;
					rangesToGather.push_back(currentRange);
					currentRange = Vec2(-1, -1);
				}
			}
		}
	}
	if((currentRange.y == -1) && (currentRange.x != -1))
	{
		currentRange.y = (float) x - 1.f;
		rangesToGather.push_back(currentRange);
	}
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::addRiver( Map* map )
{
	//Find a valid starting location
	int numOfTriesToSearchForStartLeft = 500;
	while( numOfTriesToSearchForStartLeft >= 0 )
	{
		int x = rand() % map->m_numOfCellsWide;
		int y = rand() % map->m_numOfCellsHigh;

		IntVec2 location = IntVec2( x, y );
		int index = map->getCellIndexFromPosition( location );

		if( !map->m_cells[index].isBuilding() )
		{
			map->m_cells[index].m_currentCellType = CELL_TYPE_SHALLOW_WATER;
			numOfTriesToSearchForStartLeft = -1;
		}
		--numOfTriesToSearchForStartLeft;
	}

	//Start generating river's shape
	int numOfRiverTilesToAdd = 100;
	for( int numOfTilesAddedSoFar = 0; numOfTilesAddedSoFar < numOfRiverTilesToAdd; ++numOfTilesAddedSoFar )
	{
		int numOfTriesToPlaceTile = 5000;
		while( numOfTriesToPlaceTile >= 0 )
		{
			int x = rand() % map->m_numOfCellsWide;
			int y = rand() % map->m_numOfCellsHigh;

			IntVec2 location = IntVec2( x, y );
			int index = map->getCellIndexFromPosition( location );

			if( isValidLocationForRiverTile(map, location, index) )
			{
				map->m_cells[index].m_currentCellType = CELL_TYPE_SHALLOW_WATER;
				numOfTriesToPlaceTile = -1;
			}
			--numOfTriesToPlaceTile;
		}
	}
}

//----------------------------------------------------------------------------------
bool BuildingsWithRiverGenerator::isValidLocationForRiverTile( Map* map, IntVec2& locationToCheck, int index )
{
	bool isValidLocation = false;
	if( map->m_cells[index].m_currentCellType == CELL_TYPE_SHALLOW_WATER || map->m_cells[index].isBuilding())
	{
		return false;
	}

	int northIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x, locationToCheck.y + 1 ) );
	if( northIndex >= 0 )
	{
		if( map->m_cells[northIndex].m_currentCellType == CELL_TYPE_SHALLOW_WATER )
		{
			return true;
		}
	}
	int southIndex = map->getCellIndexFromPosition( IntVec2(locationToCheck.x, locationToCheck.y - 1 ) );
	if( southIndex >= 0 )
	{
		if( map->m_cells[southIndex].m_currentCellType == CELL_TYPE_SHALLOW_WATER )
		{
			return true;
		}
	}

	if( map->getNumOfTypeAtLocationWithRadius( 1, locationToCheck, CELL_TYPE_SHALLOW_WATER ) >= 3)
	{
		return true;
	}

	return isValidLocation;
}

//----------------------------------------------------------------------------------
void BuildingsWithRiverGenerator::getRangesForDoorVertical( Map* map, int x, Vec2& yRange, std::vector<Vec2>& rangesToGather)
{
	Vec2 currentRange = Vec2(-1, -1);

	int y = (int) yRange.x;
	for(; y <= yRange.y; ++y)
	{
		IntVec2 coordPosition = IntVec2(x,y);
		int index = map->getCellIndexFromPosition(coordPosition);
		if( map->isValidY( y, index ))
		{
			if(currentRange.x == -1)
			{
				if( map->m_cells[index].isWalkable() )
				{
					currentRange.x = (float) y;
				}
			}
			else
			{
				if( !map->m_cells[index].isWalkable() )
				{
					currentRange.y = (float) y - 1.f;
					rangesToGather.push_back(currentRange);
					currentRange = Vec2(-1, -1);
				}
			}
		}
	}
	if((currentRange.y == -1) && (currentRange.x != -1))
	{
		currentRange.y = (float) y - 1.f;
		rangesToGather.push_back(currentRange);
	}
}