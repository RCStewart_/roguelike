#pragma once

#include "Cell.hpp"
#include <vector>
#include "Engine/Math/Vec3.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Font/FontGenerator.hpp"
#include "Engine\Core\GameCommon.hpp"

class Vec2;
class Agent;
class FontGenerator;
class Player;

struct MoveResults
{
	MoveResults()
		:canMoveToLocation(false)
		,agentOnCell( nullptr )
	{};
	Agent* agentOnCell;
	bool canMoveToLocation;
};

class Map
{
public:
	Map();
	~Map();

	//Drawing
	//----------------------------------------------------------------------------------
	void draw( FontGenerator* font, bool isLit = false);
	void drawCells( FontGenerator* font, bool isLit = false);

	//Update
	//----------------------------------------------------------------------------------
	void update();

	void initialize();

	int getCellIndexFromPosition( const IntVec2& tileCoords );
	IntVec2 getCellPositionFromIndex( int index );

	void makeThisPercentOfCellsAir( float percentToAir );
	void makeAllCellsWalls();
	void makeAllCellsAir();

	int getNumOfTypeAtLocationWithRadius( int radius, IntVec2& location, CellType type = CELL_TYPE_WALL );
	int getNumOfHatedEnemiesNearby( int radius, IntVec2& location, Agent* heWhoHates );
	Agent* getMostHatedEnemyNearby( int radius, IntVec2& location, Agent* heWhoHates );

	bool isValidX(int x, int index);
	bool isValidY(int y, int index);

	void makeCellsOnPathWater( std::vector<IntVec2>& path );

	MoveResults isValidLocation( IntVec2& newLocation, Agent* movingAgent );
	void		moveAgentToLocation( IntVec2& newLocation, Agent* movingAgent );
	void		setAllCellsLighting( float percentLit );
	void		setAllCellsToLowerLighting( float percentLit );

	float		getLightingPercentFromNeighboringCells( int cellIndex, std::vector<IntVec2>& cellsToCheck );

	void renderEntities( Map* map, FontGenerator* font );
	void clearMap();
	void toggleFeaturesInArea( int radius, IntVec2& location );
	Player* m_thePlayer;

	int m_numOfCellsWide;
	int m_numOfCellsHigh;
	std::vector<Cell> m_cells;
	Vec3 m_translation;
	bool m_hasBeenGenerated;

	IntVec2 NW;
	IntVec2 NE;
	IntVec2 SW;
	IntVec2 SE;
};

//----------------------------------------------------------------------------------
inline bool Map::isValidX(int x, int index)
{
	return ((x >= 0) && ( x <= m_numOfCellsWide) && (index >= 0) && ( index < (int) m_cells.size()));
}

//----------------------------------------------------------------------------------
inline bool Map::isValidY(int y, int index)
{
	return ((y >= 0) && ( y <= m_numOfCellsHigh) && (index >= 0) && ( index < (int) m_cells.size()));
}

//----------------------------------------------------------------------------------
inline MoveResults Map::isValidLocation( IntVec2& newLocation, Agent* movingAgent )
{
	UNUSED( movingAgent );
	MoveResults results;

	int index = getCellIndexFromPosition( newLocation );

	if( index != -1)
	{
		if( m_cells[index].howWalkableIsCellType( movingAgent ) > 0 && m_cells[index].m_occupant == nullptr )
		{
			results.canMoveToLocation = true;
		}

		results.agentOnCell = m_cells[index].m_occupant;
	}

	return results;
}

//Returns -1 if the index is invalid
//---------------------------------------------------------------------------
inline int Map::getCellIndexFromPosition( const IntVec2& tileCoords)
{
	int index = tileCoords.y * m_numOfCellsWide + tileCoords.x;
	if(index >= 0 && index < (int) m_cells.size() 
		&& tileCoords.x >= 0 && tileCoords.x < m_numOfCellsWide
		&& tileCoords.y >= 0 && tileCoords.y < m_numOfCellsHigh)
	{
		return index;
	}
	return -1;
}


//----------------------------------------------------------------------------------
inline IntVec2 Map::getCellPositionFromIndex(int index)
{
	int floorX = index % m_numOfCellsWide;
	float actualX = (float) floorX;

	int floorY = index / m_numOfCellsWide;
	float actualY = (float) floorY;

	return IntVec2( (int) actualX, (int) actualY);
}