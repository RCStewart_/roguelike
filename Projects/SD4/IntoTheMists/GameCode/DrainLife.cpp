#include "DrainLife.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Messages.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include "AStarPathing.hpp"
#include "TheGame.hpp"

AIBehaviorRegistration DrainLife::s_drainLifeRegistration( "DrainLife", &DrainLife::createAndGetAIBehavior );

DrainLife::DrainLife( std::string name ) 
	: BaseAIBehavior( name )
	, topTarget(nullptr)
	, minDrain(1)
	, maxDrain(5)
{

}

DrainLife::~DrainLife(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* DrainLife::createAndGetAIBehavior( const std::string& name )
{
	return new DrainLife( name );
}

//----------------------------------------------------------------------------------
bool DrainLife::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	thinker->m_currentPath.clear();
	bool thoughtResult = true;

	if(topTarget)
	{
		int amountOfDamageToDeal = RandomNumber::getRandomIntInRange( IntVec2( minDrain, maxDrain ) );
		topTarget->m_currentHealth = max( 0, topTarget->m_currentHealth - amountOfDamageToDeal );
		if( topTarget->m_currentHealth <= 0 )
		{
			g_theGame->addToTheDeadList(topTarget);
			topTarget = nullptr;
		}

		std::string messageOne = GetMessageLiteralf( "The %s touches you!", thinker->m_name.c_str() );
		std::string messageTwo = GetMessageLiteralf( "You feel drained");
		std::string messageThree = GetMessageLiteralf( "%s appeals stronger.", thinker->m_name.c_str() );

		g_messages->addMessage( messageOne, thinker->m_position );
		g_messages->addMessage( messageTwo, thinker->m_position );
		g_messages->addMessage( messageThree, thinker->m_position );
	}

	return thoughtResult;
}

//----------------------------------------------------------------------------------
float DrainLife::calculateImportance( Map* map, Agent* thinker)
{
	if( map->getNumOfHatedEnemiesNearby(1, thinker->m_position, thinker ) == 0 )
	{
		return 0.0f;	
	}
	topTarget = map->getMostHatedEnemyNearby( 1, thinker->m_position, thinker );

	return 4.0f * (float) (thinker->m_maxHealth - thinker->m_currentHealth );
}

//----------------------------------------------------------------------------------
void DrainLife::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	if(name == "minDrain")
	{
		minDrain = std::stof(attributeAsString);
	}
	if(name == "maxDrain")
	{
		maxDrain = std::stof(attributeAsString);
	}
}