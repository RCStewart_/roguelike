#include "NPCFactory.hpp"
#include "Map.hpp"
#include "BaseAIBehavior.hpp"

std::vector<NPCFactory*> NPCFactory::s_registeredFactories;

//----------------------------------------------------------------------------------
NPCFactory::NPCFactory(const std::string& name, const std::string& region, const std::string& faction, const std::string& family, char glyph, RGBAColors& color, int lineOfSight,
	int armorClass, const IntVec2& damageRange, const IntVec2& baseChanceToHit, int bonusChanceToHit, int health, unsigned char movement)
	: m_name(name)
	, m_region(region)
	, m_faction(faction)
	, m_family(family)
	, m_glyph(glyph)
	, m_color(color)
	, m_lineOfSight( lineOfSight )
	, m_armorClass(armorClass)
	, m_damageRange(damageRange)
	, m_baseChanceToHit(baseChanceToHit)
	, m_bonusChanceToHit(bonusChanceToHit)
	, m_health(health)
	, m_movement( movement )
{
}

//----------------------------------------------------------------------------------
NPCFactory::~NPCFactory(void)
{
}

//----------------------------------------------------------------------------------
int NPCFactory::sGenerateNPCFactoriesFromFileList( std::vector<std::string>& files )
{

	for( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		TiXmlDocument doc;

		if(!doc.LoadFile( files[fileIndex].c_str()))
		{
			return 0;
		}

		TiXmlElement* root = doc.FirstChildElement();

		if(root == NULL)
		{
			doc.Clear();
			return 0;
		}

		TiXmlElement* NPC = root->FirstChildElement( "NPC" );

		std::string name = NPC->Attribute( "name" );
		std::string family = NPC->Attribute( "family" );
		std::string region = NPC->Attribute( "region" );
		std::string factions = NPC->Attribute( "factions" );
		std::string glyph = NPC->Attribute( "glyph" );
		std::string r = NPC->Attribute( "r" );
		std::string g = NPC->Attribute( "g" );
		std::string b = NPC->Attribute( "b" );
		std::string a = NPC->Attribute( "a" );

		std::string los = NPC->Attribute( "lineOfSight" );
		int lineOfSight = stoi( los );

		RGBAColors newColor( (unsigned char) std::stof(r), (unsigned char) std::stof(g),
			(unsigned char) std::stof(b), (unsigned char) std::stof(a));

		IntVec2 baseAttack(
			stoi(NPC->Attribute( "minAttack" )),
			stoi(NPC->Attribute( "maxAttack" ))
			);

		int ac = stoi(NPC->Attribute("ac"));
		IntVec2 hitChance(
			stoi(NPC->Attribute( "minHitChance" )),
			stoi(NPC->Attribute( "maxHitChance" ))
			);

		int bonusChanceToHit = stoi(NPC->Attribute( "bonusChanceToHit" ));
		int health = stoi(NPC->Attribute( "health" ));

		unsigned char movement = stoi(NPC->Attribute( "move" ));

		std::vector<BaseAIBehavior*> personality;
		for( TiXmlElement* AI = NPC->FirstChildElement(); AI != NULL; AI = AI->NextSiblingElement() )
		{
			std::string nameOfBehavior = AI->Attribute("name");
			BaseAIBehavior* newBehavior = (*AIBehaviorRegistration::s_behaviorRegistrationMap)[nameOfBehavior]->createBehavior();

			for( TiXmlAttribute* attrib = AI->FirstAttribute(); attrib != NULL; attrib = attrib->Next() ) {
				newBehavior->addAttributeByName( attrib->Name(), attrib->Value());
			}

			personality.push_back(newBehavior);
		}

		NPCFactory* newFactory = new NPCFactory( name, region, factions, family, glyph[0], newColor, lineOfSight,
			ac, baseAttack, hitChance, bonusChanceToHit, health, movement);

		newFactory->m_personality = personality;
		s_registeredFactories.push_back(newFactory);

	}
	return 0;
}

//----------------------------------------------------------------------------------
NPC* NPCFactory::createNPC( Map* map, IntVec2& position )
{
	NPC* toReturn = new NPC( map, position, m_name, m_glyph, m_color );
	toReturn->m_lineOfSight = m_lineOfSight;
	toReturn->m_personality = m_personality;
	toReturn->setCombatStats( m_damageRange, m_baseChanceToHit, m_bonusChanceToHit, m_armorClass, m_health );
	toReturn->m_faction = m_faction;
	toReturn->m_movementBits = m_movement;
	return toReturn;
}

//----------------------------------------------------------------------------------
bool NPCFactory::isValidNPCForType( Cell& cellToCheck )
{
	bool isValid = false;

	if(m_region == "land" && cellToCheck.isWalkable() && cellToCheck.m_currentCellType != CELL_TYPE_SHALLOW_WATER && cellToCheck.m_currentCellType != CELL_TYPE_LAVA )
	{
		isValid = true;
	}
	if(m_region == "building" && cellToCheck.isBuilding())
	{
		isValid = true;
	}
	if(m_region == "water" && cellToCheck.m_currentCellType == CELL_TYPE_SHALLOW_WATER)
	{
		isValid = true;
	}
	if(m_region == "lava" && cellToCheck.m_currentCellType == CELL_TYPE_LAVA )
	{
		isValid = true;
	}
	if(m_region == "undead" && cellToCheck.isWalkable() && cellToCheck.m_currentCellType != CELL_TYPE_SHALLOW_WATER && cellToCheck.m_currentCellType != CELL_TYPE_LAVA )
	{
		isValid = true;
	}
	return isValid;
}

//----------------------------------------------------------------------------------
int NPCFactory::getNPCFactoryIndexForName( const std::string& name )
{
	for( int factoryIndex = 0; factoryIndex < (int) s_registeredFactories.size(); ++factoryIndex )
	{
		if( name == s_registeredFactories[factoryIndex]->m_name )
		{
			return factoryIndex;
		}
	}
	return -1;
}