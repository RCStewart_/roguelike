#include "ItemFactory.hpp"
#include "Map.hpp"

std::vector<ItemFactory*> ItemFactory::s_registeredFactories;

//----------------------------------------------------------------------------------
ItemFactory::ItemFactory( const std::string& name, char glyph, const RGBAColors& color, ItemType itemType, int uses, const IntVec2& powerRange, int bonusChance )
	:m_name(name) 
	,m_glyph(glyph)
	,m_color(color)
	,m_itemType(itemType)
	,m_uses(uses)
	,m_powerRange(powerRange)
	,m_bonusChance(bonusChance)
{
}

//----------------------------------------------------------------------------------
ItemFactory::~ItemFactory(void)
{
}

//----------------------------------------------------------------------------------
int ItemFactory::sGenerateItemFactoriesFromFileList( std::vector<std::string>& files )
{
	for( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		TiXmlDocument doc;

		if(!doc.LoadFile( files[fileIndex].c_str()))
		{
			return 0;
		}

		TiXmlElement* root = doc.FirstChildElement();

		if(root == NULL)
		{
			doc.Clear();
			return 0;
		}

		TiXmlElement* item = root->FirstChildElement( "Item" );

		std::string name = item->Attribute( "name" );
		std::string glyph = item->Attribute( "glyph" );
		std::string r = item->Attribute( "r" );
		std::string g = item->Attribute( "g" );
		std::string b = item->Attribute( "b" );
		std::string a = item->Attribute( "a" );

		RGBAColors newColor( (unsigned char) std::stof(r), (unsigned char) std::stof(g),
			(unsigned char) std::stof(b), (unsigned char) std::stof(a));

		std::string typeName = item->Attribute( "type" );
		int uses = std::stoi( item->Attribute( "uses" ) );
		IntVec2 itemStrength = IntVec2( std::stoi(item->Attribute( "minStrength" )), std::stoi(item->Attribute( "maxStrength" )));

		int bonusChance = std::stoi( item->Attribute("bonus_chance") );
		ItemType itemType = Item::sGetItemTypeFromString(typeName);
		ItemFactory* newFactory = new ItemFactory( 	name, glyph[0], newColor, itemType, uses, itemStrength, bonusChance );
		s_registeredFactories.push_back(newFactory);
	}
	return 0;
}

//----------------------------------------------------------------------------------
Item* ItemFactory::createItem( Map* map, IntVec2& position )
{
	Item* toReturn = new Item(
		m_name, 
		m_glyph,
		m_color,
		m_itemType,
		m_uses,
		m_powerRange,
		m_bonusChance,
		map,
		position
	);
	return toReturn;
}

//----------------------------------------------------------------------------------
bool ItemFactory::isValidItemForType( Cell& cellToCheck )
{
	bool isValid = false;

	if(cellToCheck.isWalkable())
	{
		isValid = true;
	}

	return isValid;
}

//----------------------------------------------------------------------------------
int ItemFactory::getItemFactoryIndexForName( const std::string& name )
{
	for( int factoryIndex = 0; factoryIndex < (int) s_registeredFactories.size(); ++factoryIndex )
	{
		if( name == s_registeredFactories[factoryIndex]->m_name )
		{
			return factoryIndex;
		}
	}
	return -1;
}