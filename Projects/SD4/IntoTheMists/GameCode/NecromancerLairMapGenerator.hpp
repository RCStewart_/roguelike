#pragma once
#include "BaseMapGenerator.hpp"

class Vec2;
class Room;
class Map;

class NecromancerLairMapGenerator : public BaseMapGenerator
{
public:
	NecromancerLairMapGenerator( const std::string& name );
	~NecromancerLairMapGenerator(void);

	static BaseMapGenerator* createGenerator( const std::string& name );

	virtual void generateMap( Map* map );
	virtual void processOneStep( Map* map );
	virtual void processSeveralSteps( Map* map );

	virtual void addNValidNPCSToMap( Map* map, int numberOfNPCsToAdd, std::vector<NPC*>& npcs );

	void makeAir( const Room& room, Map* map );

private:
	Vec2 checkIfIValidLocationForNewHall( int index, Map* map );
	Vec2 placeHallway( Vec2& direction, int index, Map* map );
	Vec2 placeRoom( Vec2& direction, int index, Map* map, const Room& room );
	void placeRandomRoom( Vec2& direction, int index, Map* map, const Room& room );

	void placeStartRoom( Map* map, const IntVec2& location, const IntVec2& dimensions );
	void placeNecromancerRoom( Map* map, const IntVec2& location, const IntVec2& dimensions );
	void placeCatacombs( Map* map );
	void addLava( Map* map );
	bool isValidLocationForLavaTile( Map* map, IntVec2& locationToCheck, int index );
protected:
	static MapGeneratorRegistration s_necromancerLairRegistration;
};

