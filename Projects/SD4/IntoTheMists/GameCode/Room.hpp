#pragma once
#include "Engine/Math/Vec2.hpp"

class Room
{
public:
	Room(int fromCenterX, int fromCenterY, const Vec2& centerPosition);

	//May switch to this for more control
	//Room(int downDist, int upDist, int leftDist, int rightDist, const Vec2& centerPosition);
	~Room(void);

	int m_cellDisplacementFromCenterX;
	int m_cellDisplacementFromCenterY;

	//int m_distLeft;
	//int m_distRight;
	//int m_distUp;
	//int m_distDown;
	Vec2 m_center;
};

