#pragma once

#include <vector>
#include <set>
#include "CellularAutomataMapGenerator.hpp"

class Player;
class NPC;
class GameEntity;
class Item;
class Feature;

enum GameState
{
	GAME_STATE_STARTING_UP,
	GAME_STATE_LOADING,
	GAME_STATE_PLAYING,
	GAME_STATE_SHOWING_MAIN_MENU,
	GAME_STATE_QUITTING,
	GAME_STATE_SHOWING_QUEST_MENU,
	GAME_STATE_SHUTTING_DOWN,
	GAME_STATE_GENERATING
};

class FontGenerator;
class Map;

class TheGame
{
public:
	TheGame(void);
	~TheGame(void);

	//Main Loop
	//----------------------------------------------------------------------------------
	void run();
	void runFrame();
	void runMessagePump();
	bool processInput();
	void update();
	void render();

	//State STARTING_UP
	//----------------------------------------------------------------------------------
	void initializeGame();

	//State LOADING
	//----------------------------------------------------------------------------------
	void load();

	//State PLAYING
	//----------------------------------------------------------------------------------
	void playingInput();
	void drawPlaying();
	void updatePlaying( double deltaSeconds );
	void startPlaying();

	//State SHOWING_MAIN_MENU
	//----------------------------------------------------------------------------------
	void mainMenuInput();
	void drawMainMenu();

	//State QUITTING
	//----------------------------------------------------------------------------------

	//State SHOWING_QUEST_MENU
	//----------------------------------------------------------------------------------
	void questMenuInput();
	void drawQuestMenu();

	//State SHUTTING_DOWN
	//----------------------------------------------------------------------------------

	//State GENERATING
	//----------------------------------------------------------------------------------
	void generatingMenuInput();
	void drawGenerating();

	void setGameState( GameState gameState );
	void addToTheDeadList( GameEntity* heWhoHasDied );
	GameState getGameState();

	void saveGame();
	bool loadGame();

	CellularAutomataMapGenerator* m_cellularAutomataMapGenerator;

	FontGenerator* m_font;
	bool m_isQuitting;
	Map* m_map;
	std::string m_currentGeneratorName;
	bool m_isInAutoGenerateMode;

	std::vector<NPC*> m_NPCs;
	std::vector<Item*> m_items;
	std::vector<Feature*> m_features;
	Player* m_player;
	std::vector< std::string > m_savedFiles;

private:
	GameState m_currentGameState;
	std::vector<GameEntity*> m_listOfTheDead;
	void cleanUpTheDead();
	void deletePointers();
};

extern TheGame* g_theGame;