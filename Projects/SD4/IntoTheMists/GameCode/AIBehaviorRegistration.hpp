#pragma once
#include <string>
#include "BaseAIBehavior.hpp"
#include <map>

class AIBehaviorRegistration;

typedef BaseAIBehavior* (AICreateFunc)( const std::string& name );
typedef std::map<std::string, AIBehaviorRegistration*> AIBehaviorRegistrationMap; 

class AIBehaviorRegistration
{
public:
	AIBehaviorRegistration( const std::string& name, AICreateFunc* creationFunc )
		:m_name(name)
		,m_aiCreateFunc(creationFunc)
	{
		if(s_behaviorRegistrationMap == nullptr)
		{
			s_behaviorRegistrationMap = new AIBehaviorRegistrationMap;
		}
		(*s_behaviorRegistrationMap)[name] = this; 
	};

	~AIBehaviorRegistration(void);

	std::string getName()
	{
		return m_name;
	};

	BaseAIBehavior* createBehavior();
	static AIBehaviorRegistrationMap* s_behaviorRegistrationMap;

protected:
	std::string m_name;
	AICreateFunc* m_aiCreateFunc;
};

