#include "SpawnNPC.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "AStarPathing.hpp"
#include "TheGame.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Messages.hpp"
#include "NPCFactory.hpp"
#include "FactionManager.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Math/RandomNumber.hpp"

AIBehaviorRegistration SpawnNPC::s_spawnNPCRegistration( "SpawnNPC", &SpawnNPC::createAndGetAIBehavior );

SpawnNPC::SpawnNPC( std::string name ) 
	: BaseAIBehavior( name )
	, npcName( "nullptr" )
	, spawnThreshhold( 3 )
	, chanceToSpawn( 100 )
{

}

SpawnNPC::~SpawnNPC(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* SpawnNPC::createAndGetAIBehavior( const std::string& name )
{
	return new SpawnNPC( name );
}

//----------------------------------------------------------------------------------
bool SpawnNPC::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	thinker->m_currentPath.clear();
	bool thoughtResult = true;

	int checkIfFailed = RandomNumber::getRandomIntInRange( IntVec2( 0, 100 ));
	if( checkIfFailed <= chanceToSpawn )
	{
		std::string message = GetMessageLiteralf( "The %s attempts to summon a %s but failed.", thinker->m_name.c_str(), npcName.c_str() );
		g_messages->addMessage( message, thinker->m_position );
		return false;
	}

	//TODO: Randomize
	////random_shuffle( thinker->m_cellsCanSee.begin(), thinker->m_cellsCanSee.end() );
	//std::vector<int> tempInts;
	bool hasPlacedNpc = false;
	//std::set<int>::iterator iteratorOfSet;
	//for(iteratorOfSet = thinker->m_cellsCanSee.begin(); iteratorOfSet != thinker->m_cellsCanSee.end(); ++iteratorOfSet)
	//{
	//	if( !map->m_cells[*it].m_occupant && !map->m_cells[*it].isWall())
	//	{
	//		hasPlacedNpc = true;
	//		int factoryIndex = NPCFactory::getNPCFactoryIndexForName( npcName );
	//		IntVec2 position = map->getCellPositionFromIndex( *it );
	//		map->m_cells[*it].m_occupant = NPCFactory::s_registeredFactories[ factoryIndex ]->createNPC( map, position );
	//		g_theGame->m_NPCs.push_back( (NPC*) map->m_cells[*it].m_occupant );

	//		break;
	//	}
	//}

	std::set<int>::iterator it;
	for(it = thinker->m_cellsCanSee.begin(); it != thinker->m_cellsCanSee.end(); ++it)
	{
		if( !map->m_cells[*it].m_occupant && !map->m_cells[*it].isWall())
		{
			hasPlacedNpc = true;
			int factoryIndex = NPCFactory::getNPCFactoryIndexForName( npcName );
			IntVec2 position = map->getCellPositionFromIndex( *it );
			map->m_cells[*it].m_occupant = NPCFactory::s_registeredFactories[ factoryIndex ]->createNPC( map, position );
			g_theGame->m_NPCs.push_back( (NPC*) map->m_cells[*it].m_occupant );

			break;
		}
	}
	
	if(hasPlacedNpc)
	{
		std::string message = GetMessageLiteralf( "The %s summons a %s to fight for him!", thinker->m_name.c_str(), npcName.c_str() );
		g_messages->addMessage( message, thinker->m_position );
	}

	return thoughtResult;
}

//----------------------------------------------------------------------------------
float SpawnNPC::calculateImportance( Map* map, Agent* thinker)
{
	std::set<int>::iterator it;
	int enemyCount = 0;
	int countOfNPCWithName = 0;
	for(it = thinker->m_cellsCanSee.begin(); it != thinker->m_cellsCanSee.end(); ++it)
	{
		if(map->m_cells[*it].m_occupant)
		{
			if( FactionManager::isEnemy(map->m_cells[*it].m_occupant, thinker))
			{
				++enemyCount;
			}
			if( map->m_cells[*it].m_occupant->m_name == npcName )
			{
				++countOfNPCWithName;
			}
		}
	}

	//if(enemyCount == 0)
	//{
	//	return 0.0f;
	//}
	if( (int) g_theGame->m_NPCs.size() >= 30 )
	{
		return 0.f;
	}

	return (spawnThreshhold * 5.0f) - ((float) countOfNPCWithName * 5.0f);
}

//----------------------------------------------------------------------------------
void SpawnNPC::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	if(name == "npcName")
	{
		npcName = attributeAsString;
	}
	if(name == "spawnThreshhold")
	{
		spawnThreshhold = std::stoi(attributeAsString);
	}
	if(name == "chanceToSpawn")
	{
		chanceToSpawn = std::stoi(attributeAsString);
	}
}