#pragma once
#include "Engine/Math/IntVec2.hpp"

class AStarNode
{
public:
	AStarNode( const IntVec2& position, const float& currentCost, const float& hueristic, AStarNode* parent );
	~AStarNode(void);

	float getFullCost();

	static bool howToSortAStarNodeInInverseOrder( AStarNode* left, AStarNode* right );

	float m_f; //added for debugging
	float m_currentCost;
	float m_hueristic;
	AStarNode* m_parent;
	IntVec2 m_position;

};

