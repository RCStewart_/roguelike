#include "Feature.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"
#include "Map.hpp"
//----------------------------------------------------------------------------------
Feature::Feature( std::string& name, char glyphOn, char glyphOff, FeatureType featureType, RGBAColors& color, bool isActive, Map* map, IntVec2& position )
	:GameEntity( map, position, name, glyphOn, color)
	,m_glyphOn(glyphOn)
	,m_glyphOff(glyphOff)
	,m_featureType(featureType)
	,m_isActive(isActive)
{
}

//----------------------------------------------------------------------------------
Feature::~Feature(void)
{
}

//----------------------------------------------------------------------------------
FeatureType Feature::sGetFeatureTypeFromString( const std::string& featureTypeAsString )
{
	UNUSED(featureTypeAsString);
	FeatureType toReturn = FEATURE_TYPE_DOOR;
	return toReturn;
}

//----------------------------------------------------------------------------------
void Feature::toggleActive()
{
	m_isActive = !m_isActive;
}

//----------------------------------------------------------------------------------
bool Feature::isAWall()
{
	if(m_featureType == FEATURE_TYPE_DOOR)
	{
		if(m_isActive)
		{
			return true;
		}
	}
	return false;
}

//----------------------------------------------------------------------------------
void Feature::saveToXML( TiXmlElement* parent )
{

	TiXmlElement* feature = new TiXmlElement( "feature" );  
	GameEntity::saveToXML( feature );

	if( !m_isActive )
	{
		appendAttributeToXMLElement( "isActive", "false", "true", feature);
	}

	parent->LinkEndChild( feature );
}

//----------------------------------------------------------------------------------
void Feature::loadFromXML( TiXmlElement* parent )
{
	GameEntity::loadFromXML( parent );
	m_isActive = true;

	if(parent->Attribute("isActive"))
	{
		m_isActive = false;
	}
}