#include "Regenerate.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/EngineCommon.hpp"
#include "Item.hpp"
#include "Messages.hpp"
#include <string>

AIBehaviorRegistration Regenerate::s_regenerateRegistration( "Regenerate", &Regenerate::createAndGetAIBehavior );

Regenerate::Regenerate( std::string name ) 
	: BaseAIBehavior( name )
	, m_chanceToMoveStrait(0.f)
	, m_chanceToRest(0.f)
{

}

Regenerate::~Regenerate(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Regenerate::createAndGetAIBehavior( const std::string& name )
{
	return new Regenerate( name );
}

//----------------------------------------------------------------------------------
bool Regenerate::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	bool thoughtResult = true;

	thinker->m_currentPath.clear();

	int healedFor = (int)((float) thinker->m_maxHealth * .25f);
	thinker->m_currentHealth += healedFor;
	std::string message = GetMessageLiteralf( "%s has regenerated %d health", thinker->m_name.c_str(), healedFor );
	g_messages->addMessage( message, thinker->m_position );

	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Regenerate::calculateImportance( Map* map, Agent* toThink)
{
	int importance = 0;
	
	if( (float) toThink->m_currentHealth / (float) toThink->m_maxHealth < .5f)
	{
		importance += 2;
	}
	if( (float) toThink->m_currentHealth / (float) toThink->m_maxHealth < .25f)
	{
		importance += 4;
	}
	if( (float) toThink->m_currentHealth / (float) toThink->m_maxHealth < .1f)
	{
		importance += 6;
	}
	if(importance > 0)
	{
		importance = rand() % importance;
	}

	return importance;
}

//----------------------------------------------------------------------------------
void Regenerate::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{

}