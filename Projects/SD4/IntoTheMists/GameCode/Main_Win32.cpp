#pragma once
#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Renderer\GLWrapper.hpp"

#include <math.h>

#include <cassert>
#include <crtdbg.h>

#include "Engine\Math\Time.hpp"
#include "TheGame.hpp"
#include "Engine\Core\GameCommon.hpp"


//-----------------------------------------------------------------------------------------------
int WINAPI WinMain( HINSTANCE applicationInstanceHandle, HINSTANCE, LPSTR commandLineString, int )
{
	WindowWrapperSetProcessDPIAware();
	UNUSED( commandLineString );

	g_numOfTileUnitsX = 60.f;
	g_numOfTileUnitsY = 35.f;
	WindowWrapperCreateOpenGLOrthoWindow( applicationInstanceHandle );
	WindowWrapperInitialization();
	InitializeAdvancedOpenGLFunctions();

	g_theGame = new TheGame();

	g_theGame->run();
	delete g_theGame;

	WindowsWrapperFindMemoryLeaks();
	return 0;
}