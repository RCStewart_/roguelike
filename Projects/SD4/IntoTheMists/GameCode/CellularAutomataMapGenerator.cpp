#include "CellularAutomataMapGenerator.hpp"
#include "Map.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "Engine/Core/GameCommon.hpp"

MapGeneratorRegistration CellularAutomataMapGenerator::s_cellularAutomataRegistration( "Cellular Automata", &CellularAutomataMapGenerator::createGenerator );

CellularAutomataMapGenerator::CellularAutomataMapGenerator(const std::string& name) : BaseMapGenerator( "Cellular Automata" )
{
	UNUSED(name);
}


CellularAutomataMapGenerator::~CellularAutomataMapGenerator(void)
{

}

void CellularAutomataMapGenerator::generateMap( Map* map )
{
	initializeRandomData(map);
}

void CellularAutomataMapGenerator::processOneStep( Map* map )
{
	Map* tempMap = new Map();

	for( int i = 0; i < (int) map->m_cells.size(); ++i )
	{
		IntVec2 currentCell = map->getCellPositionFromIndex( i );
		int numOfWalls = map->getNumOfTypeAtLocationWithRadius( 1, currentCell );
		int numOfWallsRadius2 = map->getNumOfTypeAtLocationWithRadius( 2, currentCell );
		if( numOfWalls > 5 || numOfWallsRadius2 < 5 )
		{
			tempMap->m_cells[i].m_currentCellType = CELL_TYPE_WALL;
		}
		else
		{
			tempMap->m_cells[i].m_currentCellType = CELL_TYPE_AIR;
		}
	}

	for( int currentCell = 0; currentCell < (int) map->m_cells.size(); ++currentCell )
	{
		map->m_cells[currentCell].m_currentCellType = tempMap->m_cells[currentCell].m_currentCellType;
	}

	delete tempMap;
}


BaseMapGenerator* CellularAutomataMapGenerator::createGenerator( const std::string& name )
{
	return new CellularAutomataMapGenerator( name );
}