#include "Follow.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "AStarPathing.hpp"

AIBehaviorRegistration Follow::s_followRegistration( "Follow", &Follow::createAndGetAIBehavior );

Follow::Follow( std::string name ) 
	: BaseAIBehavior( name )
	, m_leaderName("nullptr")
	, m_distanceThreshhold(10.f)
	, topTarget(nullptr)
{

}

Follow::~Follow(void)
{
}

//----------------------------------------------------------------------------------
BaseAIBehavior* Follow::createAndGetAIBehavior( const std::string& name )
{
	return new Follow( name );
}

//----------------------------------------------------------------------------------
bool Follow::Think( Map* map, Agent* thinker )
{
	UNUSED( map );
	thinker->m_currentPath.clear();
	bool thoughtResult = true;

	if(topTarget)
	{
		AStarPathing pathFinder;
		pathFinder.generatePath(thinker->m_currentPath, thinker->m_position, topTarget->m_position, map, thinker);
	}
	return thoughtResult;
}

//----------------------------------------------------------------------------------
float Follow::calculateImportance( Map* map, Agent* thinker)
{
	float maxImportance = m_distanceThreshhold;

	std::set<int>::iterator it;
	int highestCalculatedLoyalty = -10;
	float utility = 0.f;
	topTarget = nullptr;
	for (it = thinker->m_cellsCanSee.begin(); it != thinker->m_cellsCanSee.end(); ++it)
	{
		if(map->m_cells[*it].m_occupant)
		{
			if( map->m_cells[*it].m_occupant->m_name == m_leaderName )
			{
				int howLoyal = thinker->getLoyaltyForAgentBasedOnID( map->m_cells[*it].m_occupant );
				float tempUtility = 0.f;

				{
					IntVec2 cellPos = map->m_cells[*it].m_occupant->m_position;
					float	xDisplacement = (float) ( cellPos.x - thinker->m_position.x );
					float	yDisplacement = (float) ( cellPos.y - thinker->m_position.y );
					Vec2	lengthVector = Vec2(xDisplacement, yDisplacement);
					float	length = lengthVector.getMagnetude();
					if( length < m_distanceThreshhold )
					{
						tempUtility = 2.f * length;
					}
				}

				if( tempUtility > utility)
				{
					highestCalculatedLoyalty = howLoyal;
					utility = tempUtility;					
					topTarget = map->m_cells[*it].m_occupant;
				}
			}
		}
	}

	if(topTarget)
	{
		IntVec2 cellPos = topTarget->m_position;
		float xDisplacement = (float) ( cellPos.x - thinker->m_position.x );
		float yDisplacement = (float) ( cellPos.y - thinker->m_position.y );
		Vec2 lengthVector = Vec2(xDisplacement, yDisplacement);
		float length = lengthVector.getMagnetude();
		return (2.0f * length);
	}

	return 0.0f;
}

//----------------------------------------------------------------------------------
void Follow::addAttributeByName( const std::string& name, const std::string& attributeAsString )
{
	if(name == "leaderName")
	{
		m_leaderName = attributeAsString;
	}
	if(name == "distanceThreshhold")
	{
		m_distanceThreshhold = std::stof(attributeAsString);
	}
}