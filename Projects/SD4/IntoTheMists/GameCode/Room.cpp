#include "Room.hpp"

Room::Room(int fromCenterX, int fromCenterY, const Vec2& centerPosition)
	:m_cellDisplacementFromCenterX(fromCenterX)
	,m_cellDisplacementFromCenterY(fromCenterY)
	,m_center(centerPosition)
{
}

//Room::Room(int downDist, int upDist, int leftDist, int rightDist, const Vec2& centerPosition)
//	:m_distDown(downDist)
//	,m_distLeft(leftDist)
//	,m_distRight(rightDist)
//	,m_distUp(upDist)
//{
//
//}

Room::~Room(void)
{
}
