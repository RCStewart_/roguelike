#pragma once
#include "BaseMapGenerator.hpp"
#include <string>

class CellularAutomataMapGenerator: public BaseMapGenerator
{
public:
	CellularAutomataMapGenerator( const std::string& name );
	~CellularAutomataMapGenerator(void);

	static BaseMapGenerator* createGenerator( const std::string& name );

	virtual void generateMap( Map* map );
	virtual void processOneStep( Map* map );

protected:
	static MapGeneratorRegistration s_cellularAutomataRegistration;
};

