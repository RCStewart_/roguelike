#pragma once
#include <string>
#include <map>
#include "Engine/Math/IntVec2.hpp"
#include <vector>

class Map;
class MapGeneratorRegistration;
class BaseMapGenerator;
class NPC;
class Item;
class Feature;

typedef BaseMapGenerator* (RegistrationFunc)( const std::string& name );
typedef std::map<std::string, MapGeneratorRegistration*> MapGeneratorRegistrationMap;
typedef std::map<std::string, BaseMapGenerator*> MapsGeneratedMap;

class MapGeneratorRegistration
{
public:
	MapGeneratorRegistration( const std::string& name, RegistrationFunc* registrationFunc)
		:m_name(name)
		,m_registrationFunc(registrationFunc)
	{
		if(!s_mapRegistration)
		{
			s_mapRegistration = new MapGeneratorRegistrationMap();
		}
		(*s_mapRegistration)[name] = this;
	};

	static MapGeneratorRegistrationMap* getMapRegistrations()
	{
		return s_mapRegistration;
	};

	std::string getName()
	{
		return m_name;
	};

	BaseMapGenerator* createMapGenerator();

protected:
	std::string m_name;
	RegistrationFunc* m_registrationFunc;
	static MapGeneratorRegistrationMap* s_mapRegistration;
};

class BaseMapGenerator
{
public:
	BaseMapGenerator(void);
	BaseMapGenerator(const std::string& name);
	virtual ~BaseMapGenerator(void);

	virtual void generateMap( Map* map );
	virtual void startup();
	virtual void shutdown();
	virtual const std::string getName() const;
	virtual void processSeveralSteps( Map* map );
	virtual void processOneStep( Map* map );

	//Set initial 45 - 60% air
	virtual void initializeRandomData( Map* map );
	virtual IntVec2 getPlayersPosition( Map* map );

	virtual void addNValidNPCSToMap( Map* map, int numberOfNPCsToAdd, std::vector<NPC*>& npcs );
	virtual void addNValidItemsToMap( Map* map, int numberOfItemsToAdd, std::vector<Item*>& items );
	virtual void addNValidDoorsToMap( Map* map, int numberOfFeaturesToAdd, std::vector<Feature*>& features );
	static int createGenerators();

	std::string m_name;

	static MapsGeneratedMap s_generatedMaps; 
};

