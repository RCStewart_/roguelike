#pragma once

#include "AIBehaviorRegistration.hpp"
#include "NPC.hpp"
#include "Engine/Core/RGBAColors.hpp"
#include <string>
#include <vector>
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"

class Cell;
class Map;
class BaseAIBehavior;

class NPCFactory
{
public:
	NPCFactory( const std::string& name, const std::string& region, const std::string& faction, const std::string& family, char glyph, RGBAColors& color, int los,
		int armorClass, const IntVec2& damageRange, const IntVec2& baseChanceToHit, int bonusChanceToHit, int health, unsigned char movement );
	~NPCFactory();

	static int sGenerateNPCFactoriesFromFileList( std::vector<std::string>& files );

	NPC* createNPC( Map* map, IntVec2& position );
	static int getNPCFactoryIndexForName( const std::string& name );
	bool isValidNPCForType( Cell& cellToCheck );

	std::string m_name; 
	char m_glyph;
	RGBAColors m_color;
	std::string m_region;
	std::string m_faction;
	std::string m_family;

	int				m_armorClass			;
	IntVec2			m_damageRange			;
	IntVec2			m_baseChanceToHit		;
	int				m_bonusChanceToHit		;
	int				m_health				;
	unsigned char	m_movement				;

	int m_lineOfSight;

	//Know npc's personality
	std::vector<BaseAIBehavior*> m_personality;

	static std::vector<NPCFactory*> s_registeredFactories;
};

