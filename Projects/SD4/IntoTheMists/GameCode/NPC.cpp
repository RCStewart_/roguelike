#include "NPC.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "CombatSystem.hpp"

//----------------------------------------------------------------------------------
NPC::NPC(Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color )
	:Agent( map, position, name, glyph, color )
{

}

//----------------------------------------------------------------------------------
NPC::~NPC(void)
{

}

//----------------------------------------------------------------------------------
void NPC::update( Map* map, double deltaSeconds )
{
	UNUSED( deltaSeconds );
	m_cellsCanSee.clear();

	m_isRunning = false;

	TraceResult cellsVisible;
	for(int i = 0; i < 360; ++i )
	{
		rayTrace(cellsVisible, (float) m_lineOfSight, m_position, (float) cos( (float) i), (float) sin( (float) i), map);
	}

	m_cellsCanSee = cellsVisible.visibleCells;

	BaseAIBehavior* mostImportantBehavior = nullptr;
	float highestAIScore = 0.0f;
	for( size_t behaviorIndex = 0; behaviorIndex < m_personality.size(); ++behaviorIndex)
	{
		float tempScore = m_personality[behaviorIndex]->calculateImportance( map, this );
		if( tempScore > highestAIScore )
		{
			highestAIScore = tempScore;
			mostImportantBehavior = m_personality[behaviorIndex];
		}
	}

	mostImportantBehavior->Think( map, this );

	if(m_currentPath.size() > 0)
	{
		IntVec2 locationToCheck = IntVec2(m_currentPath[0].x, m_currentPath[0].y);
		MoveResults canMove = map->isValidLocation(locationToCheck, this);
		if(canMove.canMoveToLocation)
		{
			map->moveAgentToLocation( locationToCheck, this );
			m_isRunning = true;
		}
		else if( canMove.agentOnCell && !(locationToCheck == m_position) )
		{
			AttackData dataToSend;
			getOffense( dataToSend );
			ResultOfAttack results;
			if( !FactionManager::isAlly( canMove.agentOnCell, this ))
			{
				CombatSystem::resolveCombat( this, canMove.agentOnCell, dataToSend, results );
			}
		}
	}
}
