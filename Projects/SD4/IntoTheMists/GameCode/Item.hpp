#pragma once
#include <string>
#include "Engine/Core/RGBAColors.hpp"
#include "Engine/Math/IntVec2.hpp"
#include "GameEntity.hpp"

class TiXmlElement;
class Map;

enum ItemType
{
	ITEM_TYPE_SHIELD,
	ITEM_TYPE_WEAPON,
	ITEM_TYPE_HELM,
	ITEM_TYPE_CHEST,
	ITEM_TYPE_PANTS,
	ITEM_TYPE_BOOTS,
	ITEM_TYPE_LIGHT
};

class Item: public GameEntity
{
public:
	Item( std::string& name, char glyph, RGBAColors& color, ItemType itemType, int uses, const IntVec2& itemStrength, int bonusChance, Map* map, IntVec2& position );
	~Item(void);

	static ItemType sGetItemTypeFromString( const std::string& itemTypeAsString );
	virtual void saveToXML( TiXmlElement* parentNode );

	ItemType m_itemType;

	int m_bonusChance;
	int m_uses;
	IntVec2 m_itemStrengthRange;
};

