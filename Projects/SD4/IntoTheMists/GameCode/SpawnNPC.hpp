#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"

class SpawnNPC : public BaseAIBehavior
{
public:
	SpawnNPC( const std::string name );
	~SpawnNPC(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	std::string npcName;
	int			spawnThreshhold;
	int			chanceToSpawn;

protected:
	static AIBehaviorRegistration s_spawnNPCRegistration;
};

