#include "FactionManager.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Engine/Files/FileSystem.hpp"
#include "Engine/Libraries/TinyXML/tinyxml.h"
#include "Agent.hpp"
#include "NPC.hpp"
#include "TheGame.hpp"
#include "Map.hpp"
#include "Engine/Math/IntVec2.hpp"

std::map< PersonalFaction, std::map< OtherFaction, HowMuchPersonalFactionLikesOtherFaction>> FactionManager::s_factionLoyaltyMap;
//----------------------------------------------------------------------------------
FactionManager::FactionManager(void)
{
}

//----------------------------------------------------------------------------------
FactionManager::~FactionManager(void)
{
}

//----------------------------------------------------------------------------------
void FactionManager::setBaseFactionLoyalty()
{
	std::vector<std::string> files;
	g_theFileSystem->findAllFilesInDirectory( files, "Data/Factions/*.faction.xml");

	for( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		files[fileIndex] = "Data/Factions/" + files[fileIndex];
	}

	for( size_t fileIndex = 0; fileIndex < files.size(); ++fileIndex)
	{
		TiXmlDocument doc;

		if(!doc.LoadFile( files[fileIndex].c_str()))
		{
			return;
		}

		TiXmlElement* root = doc.FirstChildElement();

		if(root == NULL)
		{
			doc.Clear();
			return;
		}


		TiXmlElement* faction = root->FirstChildElement( "Faction" );

		std::string name = faction->Attribute( "name" );
		int factionID = StringID( name );

		std::map<OtherFaction, HowMuchPersonalFactionLikesOtherFaction> loyaltyMap;
		for( TiXmlElement* otherFaction = faction->FirstChildElement(); otherFaction != NULL; otherFaction = otherFaction->NextSiblingElement() )
		{
			std::string otherFactionName = otherFaction->Attribute("name");
			OtherFaction otherFactionID = StringID( otherFactionName );
			HowMuchPersonalFactionLikesOtherFaction loyalty = std::stoi(otherFaction->Attribute("BaseLoyalty"));
			loyaltyMap[otherFactionID] = loyalty;
		}

		s_factionLoyaltyMap[factionID] = loyaltyMap;
	}
}

//----------------------------------------------------------------------------------
void FactionManager::whatDoesThisActionMeanToMe(  Agent* actor, Agent* recieving, Agent* Me, ActionPerformed action )
{
	IntVec2 location = actor->m_position;
	int locationIndex = g_theGame->m_map->getCellIndexFromPosition(location);

	std::set<int>::iterator it;
	for (it = Me->m_cellsCanSee.begin(); it != Me->m_cellsCanSee.end(); ++it)
	{
		if( *it == locationIndex)
		{
			if( recieving == Me )
			{
				if( action == ACTION_PERFORMED_HURT)
				{
					Me->updateLoyaltyForAgent( actor, FA_HURT_SELF );
				}
			}
			else if( isAlly(recieving, Me ))
			{
				if( action == ACTION_PERFORMED_KILLED )
				{
					Me->updateLoyaltyForAgent( actor, FA_KILLED_FRIEND );
				}
				if( action == ACTION_PERFORMED_HURT)
				{
					Me->updateLoyaltyForAgent( actor, FA_HURT_FRIEND );
				}
			}
			else if( isEnemy(recieving, Me ))
			{
				if( action == ACTION_PERFORMED_KILLED )
				{
					Me->updateLoyaltyForAgent( actor, FA_KILLED_ENEMY );
				}
				if( action == ACTION_PERFORMED_HURT)
				{
					Me->updateLoyaltyForAgent( actor, FA_HURT_ENEMY );
				}
			}
		}
	}
}

//----------------------------------------------------------------------------------
void FactionManager::updateEveryNPCsLoyalty( Agent* actor, Agent* recieving, ActionPerformed action )
{
	for( size_t npcIndex = 0; npcIndex < g_theGame->m_NPCs.size(); ++npcIndex )
	{
		whatDoesThisActionMeanToMe(  actor, recieving, g_theGame->m_NPCs[npcIndex], action );
	}
}

//----------------------------------------------------------------------------------
bool FactionManager::isAlly( Agent* otherActor, Agent* Me )
{
	if( otherActor->m_ID == Me->m_ID)
	{
		return true;
	}
	return Me->getLoyaltyForAgentBasedOnID(otherActor) >= ALLY;
}

//----------------------------------------------------------------------------------
bool FactionManager::isEnemy( Agent* otherActor, Agent* Me )
{
	if( otherActor->m_ID == Me->m_ID)
	{
		return false;
	}
	int loyalty = Me->getLoyaltyForAgentBasedOnID(otherActor);
	return loyalty <= ENEMY;
}