#include "Agent.hpp"
#include "Engine/Font/FontGenerator.hpp"
#include "Map.hpp"
#include "Item.hpp"
#include "Messages.hpp"
#include "CombatSystem.hpp"
#include "Engine/Core/GameCommon.hpp"
#include "Engine/Core/StringTable.hpp"
#include "Cell.hpp"
#include "Engine/Math/RandomNumber.hpp"

const unsigned char MOVES_IN_AIR			= 1;
const unsigned char MOVES_SLOW_IN_AIR		= 2;
const unsigned char MOVES_IN_WATER			= 4;
const unsigned char MOVES_SLOW_IN_WATER		= 8;
const unsigned char MOVES_IN_LAVA			= 16;
const unsigned char MOVES_SLOW_IN_LAVA		= 32;

//----------------------------------------------------------------------------------
Agent::Agent(Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color)
	: GameEntity(map, position, name, glyph, color)
	, m_isPlayer(false)
	, m_numOfTurnsTaken(0)
	, m_weapon(nullptr)
	, m_shield(nullptr)
	, m_helm  (nullptr)
	, m_pants (nullptr)
	, m_boots (nullptr)
	, m_chest (nullptr)
	, m_lineOfSight(0)
	, m_isRunning(false)	
	, m_armorClassRunBonus(5)
	, m_movementBits(37)
	, m_lastRecordedExtraLight(0)
{
	m_armorClass = 5;
}

//----------------------------------------------------------------------------------
Agent::~Agent(void)
{

}

//----------------------------------------------------------------------------------
void Agent::render( Map* map, FontGenerator* font )
{
	Vec2 position = Vec2(m_map->m_translation.x + m_position.x, m_map->m_translation.y + m_position.y);
	string glyph;
	glyph += m_glyph;
	int indexInMap = map->getCellIndexFromPosition(m_position);
	if( (indexInMap != -1) && map->m_cells[indexInMap].isOccupantVisible())
	{
		font->drawText( glyph, GENERATED_FONTS_BIT, 1.f, position, m_color, false);
	}
}

//----------------------------------------------------------------------------------
void Agent::rayTrace( TraceResult& result, float distanceToTravel, IntVec2& position, float xStepSize, float yStepSize, Map* map  )
{
	float distanceSquared = distanceToTravel * distanceToTravel;
	const float invalidNum = numeric_limits<float>::infinity();

	Vec2 basePosition = Vec2( (float) position.x, (float) position.y);
	Vec2 currentPosition = basePosition;

	float stepSize = .5f;

	Vec2 step = Vec2(stepSize, stepSize);
	if(xStepSize < 0)
	{
		step.x = -stepSize;
	}
	if( yStepSize < 0)
	{
		step.y = -stepSize;
	}

	Vec2 tMax = Vec2(0.0f, 0.0f);

	Vec2 normalizedDirection = Vec2(xStepSize, yStepSize);
	normalizedDirection.normalize();

	tMax.x = abs(step.x / normalizedDirection.x); 
	tMax.y = abs(step.y / normalizedDirection.y);

	Vec2 tDelta = Vec2(0.0f, 0.0f);

	tDelta.x = abs(step.x / normalizedDirection.x); 
	tDelta.y = abs(step.y / normalizedDirection.y);

	if(xStepSize == 0)
	{
		tMax.x = invalidNum;
		tDelta.x = invalidNum;
	}
	if(yStepSize == 0)
	{
		tMax.y = invalidNum;
		tDelta.y = invalidNum;
	}

	Vec2 lengthVector = Vec2( (float) position.x,  (float) position.y);
	do {

		lengthVector.x = (float) abs(basePosition.x - currentPosition.x);
		lengthVector.y = (float) abs(basePosition.y - currentPosition.y);

		if(tMax.x < tMax.y) 
		{
			currentPosition.x += step.x;
			tMax.x = tMax.x  + tDelta.x;
			IntVec2 coords = IntVec2( (int) currentPosition.x, (int) currentPosition.y);
			int index = map->getCellIndexFromPosition( coords );
			if(!map->isValidX( (int) currentPosition.x, index))
			{
				return;
			}
		} 
		else 
		{
			tMax.y= tMax.y + tDelta.y;
			currentPosition.y = currentPosition.y + step.y;
			IntVec2 coords = IntVec2( (int) currentPosition.x, (int) currentPosition.y);
			int index = map->getCellIndexFromPosition( coords );
			if(!map->isValidY((int) currentPosition.y, index))
			{
				return;
			}
		}

		IntVec2 currentPositionAsIntVec2 = IntVec2( (int) currentPosition.x, (int) currentPosition.y);
		int mapIndex = map->getCellIndexFromPosition(currentPositionAsIntVec2);
		if(mapIndex != -1)
		{
			result.visibleCells.insert(mapIndex);
		}
		if( (mapIndex == -1) || map->m_cells[mapIndex].howWalkableIsCellType( this ) <= 0)
		{
			return;
		}

	}while(lengthVector.x * lengthVector.x + lengthVector.y * lengthVector.y <= distanceSquared);
}

//----------------------------------------------------------------------------------
Item* Agent::getAndDisEquipEquippedItem()
{
	Item* toReturn = nullptr;
	if(m_shield)
	{
		toReturn = m_shield;
		m_shield = nullptr;
		return toReturn;
	}
	if(m_helm)
	{
		toReturn = m_helm;
		m_helm = nullptr;
		return toReturn;
	}
	if(m_pants)
	{
		toReturn = m_pants;
		m_pants = nullptr;
		return toReturn;
	}
	if(m_boots)
	{
		toReturn = m_boots;
		m_boots = nullptr;
		return toReturn;
	}
	if(m_chest)
	{
		toReturn = m_chest;
		m_chest = nullptr;
		return toReturn;
	}
	return toReturn;
}

//----------------------------------------------------------------------------------
void Agent::addItemToInventoryOrEquip( Item* toAdd )
{
	if(!tryToEquipItem( toAdd ))
	{
		m_inventory.push_back( toAdd );
		std::string hasPickedUpAnItemMessage = m_name;
		hasPickedUpAnItemMessage += " picked up ";
		hasPickedUpAnItemMessage += toAdd->m_name;
		g_messages->addMessage(hasPickedUpAnItemMessage, m_position);
	}
}

//----------------------------------------------------------------------------------
bool Agent::tryToEquipItem( Item* toAdd )
{
	ItemType type = toAdd->m_itemType;
	bool wasEquipped = false;

	std::string hasEquippedAnItemMessage = m_name;
	hasEquippedAnItemMessage += " equipped ";
	hasEquippedAnItemMessage += toAdd->m_name;

	if( type == ITEM_TYPE_SHIELD )
	{
		if(!m_shield)
		{
			m_shield = toAdd;
			wasEquipped = true;
		}
	}
	if( type == ITEM_TYPE_WEAPON )
	{
		if(!m_weapon)
		{
			m_weapon = toAdd;
			wasEquipped = true;
		}
	}
	if( type == ITEM_TYPE_HELM	 )
	{
		if(!m_helm)
		{
			m_helm = toAdd;
			wasEquipped = true;
		}
	}
	if( type == ITEM_TYPE_CHEST	 )
	{
		if(!m_chest)
		{
			m_chest = toAdd;
			wasEquipped = true;
		}
	}
	if( type == ITEM_TYPE_PANTS	 )
	{
		if(!m_pants)
		{
			m_pants = toAdd;
			wasEquipped = true;
		}
	}
	if( type == ITEM_TYPE_BOOTS	 )
	{
		if(!m_boots)
		{
			m_boots = toAdd;
			wasEquipped = true;
		}
	}
	if(wasEquipped)
	{
		g_messages->addMessage(hasEquippedAnItemMessage, m_position );
	}
	return wasEquipped;
}

//----------------------------------------------------------------------------------
void Agent::getDefense( const AttackData& inAttackData, DefendData& outDefendData )
{
	UNUSED( inAttackData );

	int bonusArmor = 0;
	int bonusToMax = 0;

	if(m_shield)
	{
		bonusArmor += m_shield->m_itemStrengthRange.x;
		bonusToMax += m_shield->m_itemStrengthRange.y;
	}
	if(m_helm)
	{
		bonusArmor += m_helm->m_itemStrengthRange.x;
		bonusToMax += m_helm->m_itemStrengthRange.y;
	}
	if(m_pants)
	{
		bonusArmor += m_pants->m_itemStrengthRange.x;
		bonusToMax += m_pants->m_itemStrengthRange.y;
	}
	if(m_boots)
	{
		bonusArmor += m_boots->m_itemStrengthRange.x;
		bonusToMax += m_boots->m_itemStrengthRange.y;
	}
	if(m_chest)
	{
		bonusArmor += m_chest->m_itemStrengthRange.x;
		bonusToMax += m_chest->m_itemStrengthRange.y;
	}

	outDefendData.armorClass = m_armorClass + bonusToMax;
	outDefendData.reflectiveDamageRange = m_reflectiveDamageRange;
	outDefendData.bonusArmor = bonusArmor;
	if( m_isRunning )
	{
		outDefendData.armorClass += m_armorClassRunBonus;
	}
}

//----------------------------------------------------------------------------------
void Agent::getOffense( AttackData& outAttackData )
{
	IntVec2 bonusDamageRange = IntVec2(0, 0);
	int bonusChanceToHit = 0;
	if(m_weapon)
	{
		bonusChanceToHit = m_weapon->m_bonusChance;
		bonusDamageRange = m_weapon->m_itemStrengthRange;
	}
	outAttackData.bonusChanceToHit = m_bonusChanceToHit + bonusChanceToHit;
	outAttackData.baseHitChance = m_baseChanceToHit;
	outAttackData.damageRange = m_damageRange;
	outAttackData.damageRange.x += bonusDamageRange.x;
	outAttackData.damageRange.y += bonusDamageRange.y;
}

//----------------------------------------------------------------------------------
void Agent::updateLoyaltyForAgent( Agent* otherAgent, FactionAction whatHeDid )
{
	std::map<NameID, LoyaltyDisplacement>::iterator it = memory.find( otherAgent->m_ID );
	if( it == memory.end() )
	{
		memory[otherAgent->m_ID] = 0;	
	}
	memory[otherAgent->m_ID] += (int) whatHeDid;
}

//----------------------------------------------------------------------------------
int Agent::getLoyaltyForAgentBasedOnID( Agent* otherAgent )
{
	std::map<NameID, LoyaltyDisplacement>::iterator it = memory.find( otherAgent->m_ID );
	if( it == memory.end() )
	{
		memory[otherAgent->m_ID] = 0;	
	}
	return memory[otherAgent->m_ID] + FactionManager::s_factionLoyaltyMap[StringID(m_faction)][StringID(otherAgent->m_faction)];
}

//----------------------------------------------------------------------------------
void Agent::saveToXML( TiXmlElement* parent )
{
	TiXmlElement* agent = new TiXmlElement( "agent" );
	GameEntity::saveToXML( agent );
	appendAttributeToXMLElement( "turnsTaken", std::to_string((long long) m_numOfTurnsTaken), "0", agent );

	TiXmlElement* equipment = new TiXmlElement( "equipment" );
	{
		if(m_weapon)
		{
			TiXmlElement* toEquip = new TiXmlElement( "weapon" );
			appendAttributeToXMLElement( "name", m_weapon->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_weapon->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
		if(m_shield)
		{
			TiXmlElement* toEquip = new TiXmlElement( "shield" );
			appendAttributeToXMLElement( "name", m_shield->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_shield->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
		if(m_helm)
		{
			TiXmlElement* toEquip = new TiXmlElement( "helm" );
			appendAttributeToXMLElement( "name", m_helm->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_helm->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
		if(m_pants)
		{
			TiXmlElement* toEquip = new TiXmlElement( "pants" );
			appendAttributeToXMLElement( "name", m_pants->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_pants->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
		if(m_boots)
		{
			TiXmlElement* toEquip = new TiXmlElement( "boots" );
			appendAttributeToXMLElement( "name", m_boots->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_boots->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
		if(m_chest)
		{
			TiXmlElement* toEquip = new TiXmlElement( "chest" );
			appendAttributeToXMLElement( "name", m_chest->m_name, "", toEquip );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_chest->m_ID), to_string( (long long) 0 ), toEquip );
			equipment->LinkEndChild( toEquip );
		}
	}
	agent->LinkEndChild( equipment );

	TiXmlElement* inventory = new TiXmlElement( "inventory" );
	{
		for( int inventoryIndex = 0; inventoryIndex < (int) m_inventory.size(); ++inventoryIndex )
		{
			TiXmlElement* item = new TiXmlElement( "item" );
			appendAttributeToXMLElement( "name", m_inventory[inventoryIndex]->m_name, "", item );
			appendAttributeToXMLElement( "ID", to_string( (long long) m_inventory[inventoryIndex]->m_ID), to_string( (long long) 0 ), item );
			inventory->LinkEndChild( item );
		}
	}
	agent->LinkEndChild( inventory );

	TiXmlElement* viewOfOthers = new TiXmlElement( "viewOfOthers" );
	{
		for ( std::map<int, int>::iterator it = memory.begin(); it != memory.end(); it++)
		{
			TiXmlElement* otherAgent = new TiXmlElement( "otherAgent" );
			otherAgent->SetAttribute( "ID", std::to_string((long long) it->first).c_str());
			otherAgent->SetAttribute( "viewOfOther", std::to_string( (long long) it->second ).c_str());
			viewOfOthers->LinkEndChild( otherAgent );
		}
	}
	agent->LinkEndChild( viewOfOthers );

	parent->LinkEndChild( agent );
}

//----------------------------------------------------------------------------------
void Agent::loadFromXML( TiXmlElement* gameEntityNode )
{
	GameEntity::loadFromXML( gameEntityNode );

	if(gameEntityNode->Attribute("turnsTaken"))
	{
		m_numOfTurnsTaken = std::stoi(gameEntityNode->Attribute("turnsTaken"));
	}

	//Load equipment pointers
	m_weapon		= nullptr;
	m_shield		= nullptr;
	m_helm			= nullptr;
	m_pants			= nullptr;
	m_boots			= nullptr;
	m_chest			= nullptr;

	//Add equipment IDs
	TiXmlElement* equipment = gameEntityNode->FirstChildElement( "equipment" );
	{
		if(equipment->FirstChildElement("weapon"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("weapon");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_weapon );
		}
		if(equipment->FirstChildElement("shield"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("shield");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_shield );
		}
		if(equipment->FirstChildElement("helm"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("helm");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_helm );
		}
		if(equipment->FirstChildElement("pants"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("pants");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_pants );
		}
		if(equipment->FirstChildElement("boots"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("boots");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_boots );
		}
		if(equipment->FirstChildElement("chest"))
		{
			TiXmlElement* toEquip = equipment->FirstChildElement("chest");
			applyPointerIDFromXMLToGameEntity( "ID", toEquip, m_chest );
		}
	}

	//Add inventory IDs
	TiXmlElement* inventory = gameEntityNode->FirstChildElement( "inventory" );
	for( TiXmlElement* item = inventory->FirstChildElement(); item != NULL; item = item->NextSiblingElement() )
	{
		Item* toAddToInventory;
		applyPointerIDFromXMLToGameEntity( "ID", item, toAddToInventory );
		m_inventory.push_back( toAddToInventory );
	}

	//Add viewOfOther IDs
	TiXmlElement* otherAgents = gameEntityNode->FirstChildElement( "viewOfOthers" );
	for( TiXmlElement* otherAgent = otherAgents->FirstChildElement(); otherAgent != NULL; otherAgent = otherAgent->NextSiblingElement() )
	{
		int ID = std::stoi( otherAgent->Attribute("ID") );
		int displacement = std::stoi( otherAgent->Attribute("viewOfOther") );
		tempMemoryForLoading[ID] = displacement;
	}
}

//----------------------------------------------------------------------------------
void Agent::resolvePointers( std::map<int, GameEntity*>& oldIDsToEntityMap )
{
	//----------------------------------------------------------------------------------
	//Set item pointers
	//----------------------------------------------------------------------------------

	m_weapon	= (Item*) oldIDsToEntityMap[(int) (m_weapon	)];
	m_shield	= (Item*) oldIDsToEntityMap[(int) (m_shield	)];
	m_helm		= (Item*) oldIDsToEntityMap[(int) (m_helm	)];
	m_pants		= (Item*) oldIDsToEntityMap[(int) (m_pants	)];
	m_boots		= (Item*) oldIDsToEntityMap[(int) (m_boots	)];
	m_chest		= (Item*) oldIDsToEntityMap[(int) (m_chest	)];

	for( int itemIndex = 0; itemIndex < (int) m_inventory.size(); ++itemIndex )
	{
		m_inventory[itemIndex]	= (Item*) oldIDsToEntityMap[(int) (m_inventory[itemIndex])];
	}


	//----------------------------------------------------------------------------------
	//Set memory 
	//----------------------------------------------------------------------------------
	std::map<int, int>::iterator it = tempMemoryForLoading.begin();
	for( ; it != tempMemoryForLoading.end(); ++it)
	{
		if(oldIDsToEntityMap[it->first])
		{
			int oldID = it->first;
			int otherAgentID = oldIDsToEntityMap[oldID]->m_ID;
			memory[otherAgentID] = it->second;
		}
	}
}

//----------------------------------------------------------------------------------
int Agent::movementSpeedOnType( CellType toCheck )
{
	int moveSpeed = 0;

	switch(toCheck)
	{
	case CELL_TYPE_AIR:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_START:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_END:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_WALL:
		break;
	case CELL_TYPE_HALL:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_BUILDING_WALL:
		break;
	case CELL_TYPE_BUILDING_FLOOR:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_SHALLOW_WATER:
		if( hasBit( MOVES_SLOW_IN_WATER, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_WATER, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	case CELL_TYPE_LAVA:
		if( hasBit( MOVES_SLOW_IN_LAVA, m_movementBits ) )
		{
			moveSpeed = 1;
		}
		if( hasBit( MOVES_IN_LAVA, m_movementBits ) )
		{
			moveSpeed = 2;
		}
		break;
	}
	return moveSpeed;
}

//----------------------------------------------------------------------------------
int Agent::gValueOnType( CellType toCheck )
{
	int gValue = 0;

	switch(toCheck)
	{
	case CELL_TYPE_AIR:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			gValue = 5;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_START:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			gValue = 5;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_END:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			gValue = 5;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_WALL:
		break;
	case CELL_TYPE_HALL:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			gValue = 5;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_BUILDING_WALL:
		break;
	case CELL_TYPE_BUILDING_FLOOR:
		if( hasBit( MOVES_SLOW_IN_AIR, m_movementBits ) )
		{
			gValue = 5;
		}
		if( hasBit(  MOVES_IN_AIR, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_SHALLOW_WATER:
		if( hasBit( MOVES_SLOW_IN_WATER, m_movementBits ) )
		{
			gValue = 20;
		}
		if( hasBit(  MOVES_IN_WATER, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	case CELL_TYPE_LAVA:
		if( hasBit( MOVES_SLOW_IN_LAVA, m_movementBits ) )
		{
			gValue = 50;
		}
		if( hasBit(  MOVES_IN_LAVA, m_movementBits ) )
		{
			gValue = 1;
		}
		break;
	}
	return gValue;
}

//----------------------------------------------------------------------------------
bool Agent::hasBit( unsigned char bitToCheck, unsigned char numThatMayContainBit )
{
	return (( bitToCheck & numThatMayContainBit ) == bitToCheck );
}

//----------------------------------------------------------------------------------
void Agent::updateLastRecordedExtraLighting()
{
	m_lastRecordedExtraLight = 0;
	for(int itemIndex = 0; itemIndex < (int) m_inventory.size(); ++itemIndex )
	{
		if(m_inventory[itemIndex]->m_name == "torch")
		{
			m_lastRecordedExtraLight += RandomNumber::getRandomIntInRange( m_inventory[itemIndex]->m_itemStrengthRange );
		}
	}
}