#pragma once

#include "Agent.hpp"
#include "Map.hpp"
#include <string>
#include "Engine/Math/Vec2.hpp"
#include "Engine\Core\RGBAColors.hpp"

class FontGenerator;
class Player: public Agent
{
public:
	Player( Map* map, IntVec2& position, std::string& name, char glyph, RGBAColors& color  );
	~Player( void);

	virtual void update( Map* map, double deltaSeconds );
	virtual void render(  Map* map, FontGenerator* font );

	void updateLightOnly( Map* map, double deltaSeconds );
	bool processInput( Map* map );

	std::string m_nickName;
	IntVec2 m_movementVector;
	int m_lineOfSight;
};

