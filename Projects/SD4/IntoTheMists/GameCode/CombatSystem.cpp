#include "CombatSystem.hpp"
#include "GameEntity.hpp"
#include "TheGame.hpp"
#include "Engine/Math/RandomNumber.hpp"
#include <algorithm>
#include "Engine/Core/GameCommon.hpp"
#include "TheGame.hpp"
#include "Messages.hpp"
#include "FactionManager.hpp"

//----------------------------------------------------------------------------------
CombatSystem::CombatSystem(void)
{
}

//----------------------------------------------------------------------------------
CombatSystem::~CombatSystem(void)
{
}

//----------------------------------------------------------------------------------
void CombatSystem::resolveCombat( GameEntity* attacker, GameEntity* defender, const AttackData& dataFromAttacker, ResultOfAttack& outResultOfAttack )
{
	UNUSED( outResultOfAttack );
	DefendData dataForDefense;
	defender->getDefense( dataFromAttacker, dataForDefense );

	ResultOfAttack result;
	
	calculateDamage( dataFromAttacker, dataForDefense, result );
	createAndShowMessage( attacker, defender, result );

	defender->m_currentHealth = max( 0, defender->m_currentHealth - result.damageDeltToDefender );
	if(defender->m_currentHealth == 0)
	{
		defender->m_isDead = true;
	}

	attacker->m_currentHealth = max( 0, attacker->m_currentHealth - result.damageDeltToAttacker );

	if(attacker->m_currentHealth == 0)
	{
		attacker->m_isDead = true;
	}

	if( defender->m_isDead )
	{
		g_theGame->addToTheDeadList(defender);
	}
	if( attacker->m_isDead )
	{
		g_theGame->addToTheDeadList(attacker);
	}

	if( defender->m_isDead )
	{
		FactionManager::updateEveryNPCsLoyalty( (Agent*) attacker, (Agent*) defender, ACTION_PERFORMED_KILLED );
	}
	else
	{
		FactionManager::updateEveryNPCsLoyalty( (Agent*) attacker, (Agent*) defender, ACTION_PERFORMED_HURT );
	}
	result.currentDefenderHP = defender->m_currentHealth;
}


void CombatSystem::calculateDamage( const AttackData& attackData, const DefendData& defendData, ResultOfAttack& outResult)
{
	int hitChance = RandomNumber::getRandomIntInRange( attackData.baseHitChance ) + attackData.bonusChanceToHit;
	int dogdeRoll = defendData.armorClass + 1;
	int chanceToDogde = rand() % dogdeRoll + defendData.bonusArmor;

	int damageToDefender = 0;
	int damageToAttacker = 0;
	if( hitChance > chanceToDogde)
	{
		damageToDefender = RandomNumber::getRandomIntInRange( attackData.damageRange );
	}
	else
	{
		damageToAttacker = RandomNumber::getRandomIntInRange( defendData.reflectiveDamageRange );
	}

	outResult.damageDeltToAttacker = damageToAttacker;
	outResult.damageDeltToDefender = damageToDefender;
}

void CombatSystem::createAndShowMessage( GameEntity* attacker, GameEntity* defender, const ResultOfAttack& results )
{
	std::string attackerDamageMessage = attacker->m_name;
	if(results.damageDeltToDefender > 0)
	{
		attackerDamageMessage += " dealt ";
		attackerDamageMessage += to_string(static_cast<long long>(results.damageDeltToDefender));
		attackerDamageMessage += " damage";
		attackerDamageMessage += " to ";
		attackerDamageMessage += defender->m_name;
	}
	else
	{
		attackerDamageMessage += " missed ";
		attackerDamageMessage += defender->m_name;
	}
	g_messages->addMessage( attackerDamageMessage, defender->m_position );
}