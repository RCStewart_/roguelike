#pragma once
#include "BaseAIBehavior.hpp"
#include "AIBehaviorRegistration.hpp"

class Regenerate : public BaseAIBehavior
{
public:
	Regenerate( const std::string name );
	~Regenerate(void);

	virtual bool Think( Map* map, Agent* toThink );
	virtual float calculateImportance( Map* map, Agent* toThink);
	virtual void addAttributeByName( const std::string& name, const std::string& attributeAsString );
	static BaseAIBehavior* createAndGetAIBehavior( const std::string& name );

	float m_chanceToRest;
	float m_chanceToMoveStrait;

protected:
	static AIBehaviorRegistration s_regenerateRegistration;
};
