#include "TheGame.hpp"
#include "Engine/Renderer/Renderer.hpp"
#include "Engine/Renderer/GLWrapper.hpp"

#include "Engine\Core\WindowsWrapper.hpp"
#include "Engine\Renderer\GLWrapper.hpp"

#include <time.h>
#include <math.h>
#include <cassert>
#include <crtdbg.h>
#include "Engine\Math\Time.hpp"
#include "Engine\Core\GameCommon.hpp"
#include "Engine\Math\IntVec2.hpp"
#include "BaseMapGenerator.hpp"
#include "Map.hpp"

#include "Engine\Font\FontGenerator.hpp"

#include "Player.hpp"
#include "NPC.hpp"
#include "GameEntity.hpp"
#include "AgentStatusBar.hpp"

#include "AStarPathing.hpp"

#include "Engine/Files/FileSystem.hpp"
#include "NPCFactory.hpp"
#include <set>

#include "AIBehaviorRegistration.hpp"
#include "Messages.hpp"

#include "ItemFactory.hpp"
#include "Item.hpp"
#include "FactionManager.hpp"

#include "FeatureFactory.hpp"
#include "Engine/Core/UtilityFunctions.hpp"

#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"

TheGame* g_theGame = nullptr;

//---------------------------------------------------------------------------
TheGame::TheGame(void)
{
	g_theGame = this;
}

//----------------------------------------------------------------------------------
void PrintToConsoleVariable( const char* message, ... )
{
	UNUSED( message );
}

//----------------------------------------------------------------------------------
void ConsolePrintf( const char *, ... )
{

}

//----------------------------------------------------------------------------------
void TheGame::run()
{
	initializeGame();
	while(!m_isQuitting)
	{
		runFrame();
	}
}

//----------------------------------------------------------------------------------
void TheGame::runFrame()
{
	runMessagePump();
	processInput();
	update();
	render();
}

//----------------------------------------------------------------------------------
void TheGame::runMessagePump()
{
	WindowWrapperRunMessagePump();
}

//----------------------------------------------------------------------------------
bool TheGame::processInput()
{
	if(WindowWrapperIsInputActive('Q'))
	{
		m_isQuitting = true;
		setGameState( GAME_STATE_QUITTING );
	}

	switch(getGameState())
	{
	case GAME_STATE_STARTING_UP:
		break;
	case GAME_STATE_LOADING:
		break;
	case GAME_STATE_PLAYING:
		playingInput();
		break;
	case GAME_STATE_SHOWING_MAIN_MENU:
		mainMenuInput();
		break;
	case GAME_STATE_QUITTING:
		break;
	case GAME_STATE_SHOWING_QUEST_MENU:
		questMenuInput();
		break;
	case GAME_STATE_SHUTTING_DOWN:
		break;
	case GAME_STATE_GENERATING:
		generatingMenuInput();
		break;
	}
	return true;
}

//---------------------------------------------------------------------------
void TheGame::update()
{
	const float deltaSeconds = 1.f/60.f; //Use actual wait time later
	UNUSED(deltaSeconds);
	switch(getGameState())
	{
		case GAME_STATE_STARTING_UP:
			break;
		case GAME_STATE_LOADING:
			load();
			break;
		case GAME_STATE_PLAYING:
			updatePlaying( deltaSeconds );
			break;
		case GAME_STATE_SHOWING_MAIN_MENU:
			break;
		case GAME_STATE_QUITTING:
			break;
		case GAME_STATE_SHOWING_QUEST_MENU:
			break;
		case GAME_STATE_SHUTTING_DOWN:
			break;
		case GAME_STATE_GENERATING:
			break;
	}

	//ConsoleLog::getInputs();
	//m_mainCamera.updateCameraFromMouseAndKeyboard( deltaTime );
	//updatePointLightPos();
	//getDebugInputs();
}


//---------------------------------------------------------------------------
void TheGame::render()
{
	//GLWrapperSetUpInitialPerspectiveProjection();
	glClearColor( 0.f, 0.f, 0.f, 1.f );
	glClear( GL_COLOR_BUFFER_BIT );

	switch(getGameState())
	{
	case GAME_STATE_STARTING_UP:
		break;
	case GAME_STATE_LOADING:
		break;
	case GAME_STATE_PLAYING:
		drawPlaying();
		break;
	case GAME_STATE_SHOWING_MAIN_MENU:
		drawMainMenu();
		break;
	case GAME_STATE_QUITTING:
		break;
	case GAME_STATE_SHOWING_QUEST_MENU:
		drawQuestMenu();
		break;
	case GAME_STATE_SHUTTING_DOWN:
		break;
	case GAME_STATE_GENERATING:
		drawGenerating();
		break;
	}

	double timeNow = WindowWrapperGetCurrentTimeSeconds();
	static double previousTime = timeNow;
	double timeSinceLastFrame = timeNow - previousTime;
	while( timeSinceLastFrame < (1.0/ 60.0) )
	{
		timeNow = WindowWrapperGetCurrentTimeSeconds();
		timeSinceLastFrame = timeNow - previousTime;
	}

	previousTime = timeNow;

	GLWrapperSwapBuffers( g_displayDeviceContext );
}

//----------------------------------------------------------------------------------------------
void TheGame::drawMainMenu()
{
	std::string text = "Into The Mists";
	Vec2 pos = Vec2(5.f, 28.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 5.f, pos, RGBAColors(255,255,255,255), false );

	text = "(Q)uit to Quit at any time";
	pos = Vec2(5.f, 25.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 3.f, pos, RGBAColors(255,255,255,255), false );
	text = "(N)ew Quest";
	pos = Vec2(5.f, 22.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 3.f, pos, RGBAColors(255,255,255,255), false );

	if(m_savedFiles.size() > 0)
	{
		text = "(C)ontinue";
		pos = Vec2(5.f, 19.f);
		m_font->drawText( text, GENERATED_FONTS_BIT, 3.f, pos, RGBAColors(255,255,255,255), false );
	}
}

//----------------------------------------------------------------------------------------------
void TheGame::drawQuestMenu()
{
	float startingLocation = 28.f;
	std::string text = "Select a Quest";
	Vec2 optionPos = Vec2(5.f, startingLocation);
	m_font->drawText( text, GENERATED_FONTS_BIT, 5.f, optionPos, RGBAColors(255,255,255,255), false );

	char currentIndex = 1;
	for(MapsGeneratedMap::iterator mapGeneratorIterator = BaseMapGenerator::s_generatedMaps.begin(); mapGeneratorIterator != BaseMapGenerator::s_generatedMaps.end(); ++mapGeneratorIterator)
	{
		text = "(";
		text += std::to_string(static_cast<long long>(currentIndex));
		text += ") ";
		text += mapGeneratorIterator->first;
		optionPos = Vec2(10.f, startingLocation - 3.f * currentIndex);
		m_font->drawText( text, GENERATED_FONTS_BIT, 3.f, optionPos, RGBAColors(255,255,255,255), false );
		++currentIndex;
	}

	text = "(a) AutoGenerateMode: Is Active";
	if(!m_isInAutoGenerateMode)
	{
		text = "(a) AutoGenerateMode: Inactive";
	}
	
	optionPos = Vec2(10.f, startingLocation - 3.f * currentIndex);
	m_font->drawText( text, GENERATED_FONTS_BIT, 3.f, optionPos, RGBAColors(255,255,255,255), false );
}

//----------------------------------------------------------------------------------------------
void TheGame::drawGenerating()
{
	m_map->draw( m_font, true );

	Vec2 optionPos = Vec2(1.f, 8.f);

	std::string text = "(Space) Process One Step";
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.f, optionPos, RGBAColors(255,255,255,255), false );

	optionPos = Vec2(20.f, 8.f);
	text = "(Enter) Process Several Steps";
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.f, optionPos, RGBAColors(255,255,255,255), false );

	optionPos = Vec2(40.f, 8.f);
	text = "(F) Finish";
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.f, optionPos, RGBAColors(255,255,255,255), false );
}

//----------------------------------------------------------------------------------------------
void TheGame::drawPlaying()
{
	m_map->draw( m_font );
	if(m_player != nullptr)
	{
		m_player->render( m_map, m_font );
		Vec2 statusBarPosition = Vec2(5.f, 5.f);
		AgentStatusBar::displayAgentOnScreen( m_player, m_font, statusBarPosition );
	}

	m_map->renderEntities( m_map, m_font );

	for( int NPCindex = 0; (size_t) NPCindex < m_NPCs.size(); ++NPCindex)
	{
		m_NPCs[NPCindex]->render( m_map, m_font );
	}

	std::string text = "(ESC) Go to Quest Menu";
	Vec2 optionPos = Vec2(5.f, 8.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.5f, optionPos, RGBAColors(255,255,255,255), false );

	text = "(M) toggle map lighting";
	optionPos = Vec2(23.f, 8.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.5f, optionPos, RGBAColors(255,255,255,255), false );

	text = "(S) Save & Quit";
	optionPos = Vec2(42.f, 8.f);
	m_font->drawText( text, GENERATED_FONTS_BIT, 1.5f, optionPos, RGBAColors(255,255,255,255), false );

	Vec2 messageLogPos = Vec2( 20.f, 33.f);
	g_messages->displayMessage( m_font, messageLogPos );
}

//---------------------------------------------------------------------------
void TheGame::initializeGame()
{
	g_messages = new Messages();
	g_theFileSystem = new FileSystem( nullptr );

	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------
	std::vector<std::string> NPCFiles;

	g_theFileSystem->findAllFilesInDirectory( NPCFiles, "Data/NPCs/*.blueprint.xml");

	for( size_t fileIndex = 0; fileIndex < NPCFiles.size(); ++fileIndex)
	{
		NPCFiles[fileIndex] = "Data/NPCs/" + NPCFiles[fileIndex];
	}

	NPCFactory::sGenerateNPCFactoriesFromFileList( NPCFiles );

	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------

	std::vector<std::string> itemFiles;
	g_theFileSystem->findAllFilesInDirectory( itemFiles, "Data/Items/*.item.xml");

	for( size_t fileIndex = 0; fileIndex < itemFiles.size(); ++fileIndex)
	{
		itemFiles[fileIndex] = "Data/Items/" + itemFiles[fileIndex];
	}
	ItemFactory::sGenerateItemFactoriesFromFileList(itemFiles);

	//----------------------------------------------------------------------------------
	//----------------------------------------------------------------------------------
	std::vector<std::string> featureFiles;
	g_theFileSystem->findAllFilesInDirectory( featureFiles, "Data/Features/*.feature.xml");

	for( size_t fileIndex = 0; fileIndex < featureFiles.size(); ++fileIndex)
	{
		featureFiles[fileIndex] = "Data/Features/" + featureFiles[fileIndex];
	}

	FeatureFactory::sGenerateFeatureFactoriesFromFileList(featureFiles);

	m_player = nullptr;
	g_messages->m_referenceToPlayer = m_player;

	m_isInAutoGenerateMode = true;

	srand((unsigned int) time(NULL));
	m_isQuitting = false;
	m_map = new Map();

	m_font = new FontGenerator();
	m_font->initialize();

	std::string firstDungeon = "";

	setGameState( GAME_STATE_LOADING );
	m_currentGeneratorName = ' ';
	BaseMapGenerator::createGenerators();

	FactionManager::setBaseFactionLoyalty();

	g_theFileSystem->findAllFilesInDirectory( m_savedFiles, "Data/Maps/saveFile.xml");
}

//---------------------------------------------------------------------------
TheGame::~TheGame(void)
{
	
}

//----------------------------------------------------------------------------------
void TheGame::setGameState( GameState gameState )
{
	//TODO: IMPLEMENT AFTER MAKING SURE LOG IS WORKING
	//PRINT TO LOG
	if( m_currentGameState != gameState )
	{
		//Print to console
	}
	m_currentGameState = gameState;
}

//----------------------------------------------------------------------------------
GameState TheGame::getGameState()
{
	return m_currentGameState;
}

//----------------------------------------------------------------------------------------------
void TheGame::load()
{
	setGameState( GAME_STATE_SHOWING_MAIN_MENU );
}

//----------------------------------------------------------------------------------------------
void TheGame::mainMenuInput()
{
	if(WindowWrapperIsInputActive('Q'))
	{
		m_isQuitting = true;
		setGameState( GAME_STATE_QUITTING );
	}
	if(WindowWrapperIsInputActive('N'))
	{
		setGameState( GAME_STATE_SHOWING_QUEST_MENU );
	}
	if(m_savedFiles.size() > 0)
	{
		if(WindowWrapperIsInputActive('C'))
		{
			setGameState( GAME_STATE_PLAYING );
			loadGame();
		}
	}
}

//----------------------------------------------------------------------------------
void TheGame::questMenuInput()
{
	char currentIndex = '1';
	for( MapsGeneratedMap::iterator generatorIterator = BaseMapGenerator::s_generatedMaps.begin(); generatorIterator != BaseMapGenerator::s_generatedMaps.end(); generatorIterator++ )
	{
		if(WindowWrapperIsInputActive(currentIndex))
		{
			m_currentGeneratorName = generatorIterator->first;
			setGameState(GAME_STATE_GENERATING);
			BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->generateMap( m_map );
		}
		++currentIndex;
	}
	if(WindowWrapperGetIsInputActiveAndConsume('A'))
	{
		m_isInAutoGenerateMode = !m_isInAutoGenerateMode;
	}

}

//----------------------------------------------------------------------------------
void TheGame::generatingMenuInput()
{
	if(m_isInAutoGenerateMode)
	{
		BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->processSeveralSteps( m_map );
		startPlaying();
	}
	if(WindowWrapperGetIsInputActiveAndConsume(WINDOW_WRAPPER_INPUT_ENTER))
	{
		BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->processSeveralSteps( m_map );
	}
	if(WindowWrapperGetIsInputActiveAndConsume(WINDOW_WRAPPER_INPUT_SPACE))
	{
		BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->processOneStep( m_map );
	}
	if(WindowWrapperGetIsInputActiveAndConsume('F'))
	{
		startPlaying();
	}
}

//----------------------------------------------------------------------------------
void TheGame::startPlaying()
{
	int numOfNPCsToAdd = 20;
	int numOfItemsToAdd = 20;
	int numOfDoorsToAdd = 10;
	setGameState( GAME_STATE_PLAYING );
	IntVec2 playersPos = BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->getPlayersPosition(m_map);

	if(m_player == nullptr)
	{
		string playerName = "Player";
		RGBAColors colorOfPlayer = RGBAColors(255, 255, 255, 255);
		m_player = new Player(m_map, playersPos, playerName, '@', colorOfPlayer );
		g_messages->m_referenceToPlayer = m_player;
	}
	else
	{
		m_player->m_position = playersPos;
	}

	m_player->m_currentHealth = m_player->m_maxHealth;
	m_map->m_thePlayer = m_player;
	int mapIndex = m_map->getCellIndexFromPosition(playersPos);
	m_map->m_cells[mapIndex].m_occupant = m_player;

	m_map->setAllCellsLighting(0.f);

	BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->addNValidNPCSToMap( m_map, numOfNPCsToAdd, m_NPCs );
	BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->addNValidItemsToMap( m_map, numOfItemsToAdd, m_items );
	BaseMapGenerator::s_generatedMaps[m_currentGeneratorName]->addNValidDoorsToMap( m_map, numOfDoorsToAdd, m_features );
}

//----------------------------------------------------------------------------------
void TheGame::playingInput()
{
	if(WindowWrapperIsInputActive(WINDOW_WRAPPER_INPUT_ESCAPE))
	{
		deletePointers();
		m_map->clearMap();
		m_map->makeAllCellsWalls();
		setGameState( GAME_STATE_SHOWING_MAIN_MENU );
	}
	if(WindowWrapperGetIsInputActiveAndConsume('S'))
	{
		saveGame();
		deletePointers();
		m_map->clearMap();
		m_map->makeAllCellsWalls();
		setGameState( GAME_STATE_SHOWING_MAIN_MENU );
	}
	static bool makeAllLight = false;
	if(WindowWrapperGetIsInputActiveAndConsume('M'))
	{
		if(makeAllLight)
		{
			m_map->setAllCellsLighting(1.0f);
		}
		else
		{
			m_map->setAllCellsLighting(0.f);
		}

		makeAllLight = !makeAllLight;
	}
	m_map->setAllCellsToLowerLighting(Cell::s_basePercentLight);
}

//----------------------------------------------------------------------------------
void TheGame::updatePlaying( double deltaSeconds )
{
	if( !g_messages->hasMoreMessages())
	{
		if( m_player != nullptr )
		{
			bool playerHasMoved = m_player->processInput( m_map );

			m_player->update( m_map, deltaSeconds );

			if(playerHasMoved)
			{
				m_player->updateLastRecordedExtraLighting();
				for(size_t npcIndex = 0; npcIndex < m_NPCs.size(); ++npcIndex )
				{
					m_NPCs[npcIndex]->update( m_map, deltaSeconds );
				}
				m_map->update();
			}
		}
	}
	else
	{
		if( m_player != nullptr )
		{
			m_player->updateLightOnly( m_map, deltaSeconds );
		}
	}
	if(WindowWrapperGetIsInputActiveAndConsume( WINDOW_WRAPPER_INPUT_SPACE ))
	{
		g_messages->incrementMessage();
	}
	cleanUpTheDead();
}

//----------------------------------------------------------------------------------
void TheGame::cleanUpTheDead()
{
	for( size_t cellIndex = 0; cellIndex < m_map->m_cells.size(); ++cellIndex )
	{
		for( size_t deadIndex = 0; deadIndex < m_listOfTheDead.size(); ++deadIndex )
		{
			if( m_map->m_cells[cellIndex].m_occupant == m_listOfTheDead[deadIndex])
			{
				m_map->m_cells[cellIndex].m_occupant = nullptr;
			}
		}
	}

	for( int NPCIndex = (int) (m_NPCs.size() - 1); NPCIndex >= 0; --NPCIndex )
	{
		for( size_t deadIndex = 0; deadIndex < m_listOfTheDead.size(); ++deadIndex )
		{
			if( NPCIndex < (int) m_NPCs.size())
			{
				if( m_NPCs[NPCIndex] == m_listOfTheDead[deadIndex])
				{
					m_NPCs[NPCIndex] = nullptr;
					m_NPCs.erase( m_NPCs.begin() + NPCIndex );
				}
			}
		}
	}

	bool hasPlayerDied = false;
	for( size_t deadIndex = 0; deadIndex < m_listOfTheDead.size(); ++deadIndex )
	{
		if( m_player == m_listOfTheDead[deadIndex])
		{
			hasPlayerDied = true;
			m_player = nullptr;
			g_messages->m_referenceToPlayer = nullptr;
		}
	}

	for( size_t deadIndex = 0; deadIndex < m_listOfTheDead.size(); ++deadIndex )
	{
		std::string hasDiedMessage;
		hasDiedMessage = m_listOfTheDead[ deadIndex ]->m_name;
		hasDiedMessage += " has died";
		g_messages->addMessage(hasDiedMessage, m_listOfTheDead[ deadIndex ]->m_position );
		delete m_listOfTheDead[deadIndex];
	}
	m_listOfTheDead.clear();

	if(hasPlayerDied)
	{
		std::string gameOver = "Game Over:	 (Q) Quit	 (Esc) to Main menu";
		IntVec2 location(0, 0);
		g_messages->addMessage( gameOver, location );
	}
}

//----------------------------------------------------------------------------------
void TheGame::addToTheDeadList( GameEntity* heWhoHasDied )
{
	bool foundDead = false;
	for( size_t indexOfTheDead = 0; indexOfTheDead < m_listOfTheDead.size(); ++indexOfTheDead )
	{
		if( heWhoHasDied == m_listOfTheDead[indexOfTheDead])
		{
			foundDead = true;
		}
	}
	if(!foundDead)
	{
		m_listOfTheDead.push_back( heWhoHasDied );
	}
}

//----------------------------------------------------------------------------------
void TheGame::deletePointers()
{
	UtilityFunctions::deleteVectorOfPointers( m_NPCs );
	UtilityFunctions::deleteVectorOfPointers( m_items );
	UtilityFunctions::deleteVectorOfPointers( m_features );
}

//----------------------------------------------------------------------------------
void TheGame::saveGame()
{
	TiXmlDocument doc;  
	TiXmlElement* data;
	TiXmlDeclaration* declaration = new TiXmlDeclaration( "1.0", "utf-8", "" );  

	std::string cellLightingBuffer = "";
	char* cellBuffer = new char[m_map->m_cells.size() + 1];

	int cellIndex = 0;
	for( ; cellIndex < (int) m_map->m_cells.size(); ++cellIndex )
	{
		cellBuffer[cellIndex] = m_map->m_cells[cellIndex].getGlyph();

		if( m_map->m_cells[cellIndex].m_percentLit > 0 )
		{
			cellLightingBuffer += "1";
		}
		else
		{
			cellLightingBuffer += "0";
		}
	}
	
	cellBuffer[cellIndex] = '\0';
	doc.LinkEndChild( declaration );  

	TiXmlElement* root = new TiXmlElement( "Map" );  
	doc.LinkEndChild( root );  

	TiXmlElement* cellData = new TiXmlElement( "cellData" );  
	root->LinkEndChild( cellData );
	TiXmlText* cellBufferAsXML = new TiXmlText( cellBuffer );
	cellData->LinkEndChild( cellBufferAsXML );

	TiXmlElement* cellLighting = new TiXmlElement( "cellLighting" );  
	root->LinkEndChild( cellLighting );
	TiXmlText* cellLightingAsXML = new TiXmlText( cellLightingBuffer.c_str() );
	cellLighting->LinkEndChild( cellLightingAsXML );

	TiXmlElement* agents = new TiXmlElement( "agents" );
	m_player->saveToXML( agents );
	for( int NPCIndex = 0; NPCIndex < (int) m_NPCs.size(); ++NPCIndex )
	{
		m_NPCs[NPCIndex]->saveToXML( agents );
	}
	root->LinkEndChild( agents );

	TiXmlElement* items = new TiXmlElement( "items" );
	for( int itemIndex = 0; itemIndex < (int) m_items.size(); ++itemIndex )
	{
		m_items[itemIndex]->saveToXML( items );
	}
	root->LinkEndChild( items );

	TiXmlElement* features = new TiXmlElement( "features" );
	for( int featureIndex = 0; featureIndex < (int) m_features.size(); ++featureIndex )
	{
		m_features[featureIndex]->saveToXML( features );
	}
	root->LinkEndChild(features);

	doc.SaveFile( "Data/Maps/saveFile.xml" );

	delete[] cellBuffer;
}

//----------------------------------------------------------------------------------
bool TheGame::loadGame()
{
	bool wasLoaded = true;
	std::map< int, GameEntity* > savedIDToEntityMap;
	savedIDToEntityMap[0] = nullptr;

	deletePointers();
	TiXmlDocument doc;

	std::string filePath = "Data/Maps/";

	filePath += m_savedFiles[0];

	if(!doc.LoadFile( filePath.c_str() ))
	{
		return false;
	}

	TiXmlElement* root = doc.FirstChildElement();

	if(root == NULL)
	{
		doc.Clear();
		return false;
	}

	TiXmlElement* cellData = root->FirstChildElement( "cellData" );

	const char* cellTypeBuffer = new char[m_map->m_cells.size()];
	cellTypeBuffer = cellData->GetText();
	std::string bufferContainer = cellTypeBuffer;
	int sizeOfMap = m_map->m_cells.size();

	TiXmlElement* cellLighting = root->FirstChildElement( "cellLighting" );
	const char* cellLightingBuffer = new char[m_map->m_cells.size()];
	cellLightingBuffer = cellLighting->GetText();
	std::string lightingBufferContainer = cellTypeBuffer;

	for( int cellIndex = 0; cellIndex < (int) m_map->m_cells.size(); ++cellIndex )
	{
		m_map->m_cells[cellIndex].setTypeWithCharGlyph( bufferContainer[cellIndex] );

		if(cellLightingBuffer[cellIndex] == '1')
		{
			m_map->m_cells[cellIndex].setPercentLit(1);
		}
	}

	//----------------------------------------------------------------------------------
	//Agents
	//----------------------------------------------------------------------------------
	TiXmlElement* agents = root->FirstChildElement("agents");

	std::vector<BaseAIBehavior*> personality;
	for( TiXmlElement* agent = agents->FirstChildElement(); agent != NULL; agent = agent->NextSiblingElement() )
	{
		std::string agentName = agent->Attribute("name");
		gameEntityStats agentData = GameEntity::getGeneralGameEntityStatsFromXMLNode( agent, m_map );
		Agent* toAdd;
		if( agentName == "Player")
		{
			RGBAColors colorOfPlayer = RGBAColors(255, 255, 255, 255);
			m_player = new Player( m_map, agentData.position, agentData.name, '@', colorOfPlayer );\
			m_player->loadFromXML( agent );
			toAdd = m_player;
		}
		else
		{
			int NPCFactoryIndex = NPCFactory::getNPCFactoryIndexForName( agentName );
			NPC* createdNPC = NPCFactory::s_registeredFactories[NPCFactoryIndex]->createNPC(m_map, agentData.position);
			createdNPC->loadFromXML( agent );
			toAdd = createdNPC;
			m_NPCs.push_back( createdNPC );
		}
		if(agentData.positionIndex != -1)
		{
			m_map->m_cells[agentData.positionIndex].m_occupant = toAdd; 
		}
		savedIDToEntityMap[agentData.ID] = toAdd;
	}

	//----------------------------------------------------------------------------------
	//items
	//----------------------------------------------------------------------------------
	TiXmlElement* items = root->FirstChildElement("items");

	for( TiXmlElement* item = items->FirstChildElement(); item != NULL; item = item->NextSiblingElement() )
	{
		std::string itemName = item->Attribute("name");
		gameEntityStats itemData = GameEntity::getGeneralGameEntityStatsFromXMLNode( item, m_map );
		Item* toAdd;
		{
			int ItemFactoryIndex = ItemFactory::getItemFactoryIndexForName( itemName );
			Item* createdItem = ItemFactory::s_registeredFactories[ItemFactoryIndex]->createItem(m_map, itemData.position);
			createdItem->loadFromXML( item );
			toAdd = createdItem;
			m_items.push_back( createdItem );
		}
		if(itemData.positionIndex != -1)
		{
			m_map->m_cells[ itemData.positionIndex].m_items.push_back(toAdd); 
		}
		savedIDToEntityMap[ itemData.ID] = toAdd;
	}

	//----------------------------------------------------------------------------------
	//features
	//----------------------------------------------------------------------------------
	TiXmlElement* features = root->FirstChildElement("features");

	for( TiXmlElement* feature = features->FirstChildElement(); feature != NULL; feature = feature->NextSiblingElement() )
	{
		std::string featureName = feature->Attribute("name");
		gameEntityStats featureData = GameEntity::getGeneralGameEntityStatsFromXMLNode( feature, m_map );
		Feature* toAdd;
		{
			int featureFactoryIndex = FeatureFactory::getFeatureFactoryIndexForName( featureName );
			Feature* createdFeature = FeatureFactory::s_registeredFactories[featureFactoryIndex]->createFeature(m_map, featureData.position);
			createdFeature->loadFromXML( feature );
			toAdd = createdFeature;
			m_features.push_back( createdFeature );
		}
		if(featureData.positionIndex != -1)
		{
			m_map->m_cells[ featureData.positionIndex].m_feature = toAdd; 
		}
		savedIDToEntityMap[ featureData.ID] = toAdd;
	}

	//Link pointers back from IDs
	std::map<int, GameEntity*>::iterator it = savedIDToEntityMap.begin();
	for( ; it != savedIDToEntityMap.end(); ++it)
	{
		if(it->second)
		{
			it->second->resolvePointers( savedIDToEntityMap );
		}
	}
	return wasLoaded;
}