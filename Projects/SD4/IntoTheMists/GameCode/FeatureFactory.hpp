#pragma once

#include "Engine/Core/RGBAColors.hpp"
#include <string>
#include <vector>
#include "Engine\Libraries\TinyXML\tinystr.h"
#include "Engine\Libraries\TinyXML\tinyxml.h"
#include "Engine\Math\IntVec2.hpp"
#include "Feature.hpp"

class Map;

class FeatureFactory
{
public:
	FeatureFactory( const std::string& name, char glyphOn, char glyphOff, const RGBAColors& color, FeatureType featureType );
	~FeatureFactory();

	static int sGenerateFeatureFactoriesFromFileList( std::vector<std::string>& files );
	static int getFeatureFactoryIndexForName( const std::string& name );

	Feature* createFeature( Map* map, IntVec2& position );
	bool isValidFeatureForPosition( int location, Map* map );

	std::string		m_name; 
	char			m_glyphOn;
	char			m_glyphOff;
	RGBAColors		m_color;
	FeatureType		m_featureType;

	static std::vector<FeatureFactory*> s_registeredFactories;
};
