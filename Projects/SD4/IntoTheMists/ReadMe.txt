Controls:
	
	H:	Move W
	J:	Move S
	K:	Move N
	L:	Move E
	Y:	Move NW
	U:	Move NE
	B:	Move SW
	N:	Move SE

	Space: 	Progress message bar
	,:	Pick up item
	O:	Operate nearby feature or door