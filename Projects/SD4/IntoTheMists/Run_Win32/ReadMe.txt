Known issues:
 I have encountered at least one crash, needs more testing to isolate the problem

Controls:
	H: move left
	J: move down
	K: move up
	L: move right
	Y: move north west
	V: move north east
	B: move south west
	N: move south east
	O: toggle doors